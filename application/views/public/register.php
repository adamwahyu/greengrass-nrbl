
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="<?php echo(base_url("asset/img/green_grass.png")); ?>" type="image/png" sizes="16x16">
  <?php
          $perumahan = $this->db->query("select * from ref_perumahan where sts_active=1
          ")->row_array();
          $result = $perumahan['nama_perumahan'];

          ?>

  <title><?=$result?> | Halaman Daftar Akun</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-name" content="<?php echo($this->security->get_csrf_token_name()); ?>" charset="utf-8">
  <meta name="csrf-token" content="<?php echo($this->security->get_csrf_hash()); ?>" charset="utf-8">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/plugins/")); ?>fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/plugins/")); ?>icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/adminlte/")); ?>css/adminlte.min.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/plugins/")); ?>sweetalert2-theme-bootstrap-4/bootstrap-4.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/plugins/")); ?>toastr/toastr.min.css">
  <link rel="stylesheet" href="<?php echo(base_url("asset/")); ?>plugins/pace-progress/themes/blue/pace-theme-flat-top.css">
  <link rel="stylesheet" type="text/css" href="<?php echo(base_url("asset/css/init.css")); ?>">

  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
  <style type="text/css">
    body {
      /*font-family:muli,sans-serif;*/
      font-size: 0.9rem !important;
    }
    .login-page, .register-page {
        -ms-flex-align: center;
        align-items: center;
        padding-top: 0px !important;
        background-size: 100% 100%;
        background-repeat: no-repeat;
        background-image: url('<?php echo(base_url("asset/img/bg2.jpg")); ?>') !important;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        flex-direction: column;
        height: 100vh;
        -ms-flex-pack: center;
        justify-content: center;
      }

    .register-box {
      width: 460px !important;
    }
    /*
      ##Device = Desktops
      ##Screen = 1281px to higher resolution desktops
    */

    @media (min-width: 1281px) {

      //CSS

    }

    /*
      ##Device = Laptops, Desktops
      ##Screen = B/w 1025px to 1280px
    */

    @media (min-width: 1025px) and (max-width: 1280px) {

      //CSS

    }

    /*
      ##Device = Tablets, Ipads (portrait)
      ##Screen = B/w 768px to 1024px
    */

    @media (min-width: 768px) and (max-width: 1024px) {

      //CSS

    }

    /*
      ##Device = Tablets, Ipads (landscape)
      ##Screen = B/w 768px to 1024px
    */

    @media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {

      .register-box {
        width: 100% !important;
      }

      //CSS

    }

    /*
      ##Device = Low Resolution Tablets, Mobiles (Landscape)
      ##Screen = B/w 481px to 767px
    */

    @media (min-width: 481px) and (max-width: 767px) {

      .register-box {
        width: 100% !important;
      }
      //CSS

    }

    /*
      ##Device = Most of the Smartphones Mobiles (Portrait)
      ##Screen = B/w 320px to 479px
    */

    @media (min-width: 320px) and (max-width: 480px) {

      .register-box {
        width: 100% !important;
      }
      //CSS

    }

    @media (max-width: 576px) {
    .login-box, .register-box {
         margin-top: 0px;
        width: 90%;
    }
  }

  .card {
       margin-bottom: 0px;
  }
  .login-box-msg, .register-box-msg {
    margin: 0;
    padding: 0px;
    text-align: center;
}

  </style>
  <script type="text/javascript">
    const base_url = (url="") => {
      return `<?php echo(base_url("")); ?>${url}`
    }
  </script>
</head>
<body class="hold-transition register-page">
<div class="register-box">


  <div class="card shadow">

    <div class="card-body register-card-body">
      <!-- <div class="ribbon-wrapper ribbon-xl">
        <div class="ribbon bg-secondary">
          Register Form
        </div>
      </div> -->
      <div class="register-logo">
        <!-- <a href="javascript:void(0)"><b>Forwards</b></a> -->
        <img style="width: 170px;" src="<?php echo(base_url("asset/img/green_grass.png")); ?>" />
      </div>
      <p class="login-box-msg">
        <b>Register Form</b>
      </p>

      <!-- <form action="../../index.html" method="post"> -->
      <?php echo(form_open("", [
        "id" => "form_validation"
      ])); ?>
        <div class="row">

          <div class="col-md-12">
            <div id="id-alertdanger" class="alert alert-danger alert-dismissible" style="display: none;">
              <h5><i class="icon fas fa-ban"></i> Informasi !</h5>
              <span id="id-alertdanger-txt">
                Danger alert preview. This alert is dismissable. A wonderful serenity has taken possession of my
              entire
              </span>
            </div>
            <div id="id-alertsuccess" class="alert alert-success alert-dismissible" style="display: none;">
              <h5><i class="icon fas fa-ban"></i> Informasi !</h5>
              <span id="id-alertsuccess-txt">
                Danger alert preview. This alert is dismissable. A wonderful serenity has taken possession of my
              entire
              </span>
            </div>
          </div>

          <div class="col-md-9">
            <div class="form-group">
              <label for="">
                Nomer VA Narobil
              </label>
              <div class="input-group mb-1">
                <input
                  type="text"
                  name="nomerva_narobil"
                  onkeyup="form_input_numberonly(event)" id="id-txt-nomerva_narobil" class="form-control" placeholder="Ketik disini . . .">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-credit-card"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="">
                Cek VA
              </label>
              <button type="button" id="id-btn-ceknomerva" class="btn bg-gradient-primary btn-block">
                <!-- <i class="fas fa-check"> </i> -->
                Cek
              </button>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="">
                Kode Unit
              </label>
              <div class="input-group mb-1">
                <input type="text" name="kodeunit" id="id-txt-kodeunit" class="form-control" placeholder="-" readonly="">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-map-pin"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="">
                Nama Pemilik
              </label>
              <div class="input-group mb-1">
                <input type="text" name="namapemilik" id="id-txt-namapemilik" class="form-control" placeholder="-" readonly="">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-user"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="">
                Email
              </label>
              <div class="input-group mb-1">
                <input type="email" name="email" id="id-txt-email" class="form-control" placeholder="Ketik disini . . .">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="">
                Msisdn
              </label>
              <div class="input-group mb-1">
                <input type="text" name="msisdn" id="id-txt-msisdn" class="form-control" placeholder="Ketik disini . . .">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-phone"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="">
                Kata Sandi
              </label>
              <div class="input-group mb-1">
                <input type="password" name="password" id="id-txt-password" class="form-control" placeholder="Ketik disini . . .">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-key"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="">
                Ulangi Kata Sandi
              </label>
              <div class="input-group mb-1">
                <input type="password" name="retypepassword" id="id-txt-retypepassword" class="form-control" placeholder="Ketik disini . . .">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-key"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 mt-2">

            <button type="submit" id="id-btn-register" class="btn bg-gradient-primary btn-block" disabled="">
              <i class="fas fa-plus"> </i>
              Daftar
            </button>
          </div>
          <div class="col-md-6 mt-2">
            <button type="button" id="id-btn-reset" class="btn bg-gradient-danger btn-block" >
              <i class="fas fa-window-close"> </i>
              Reset Form
            </button>
          </div>
          <!-- <div class="col-md-4 mt-2">
            <a href="<?php echo(base_url("auth/login")); ?>" class="btn bg-gradient-success btn-block" >
              <i class="fas fa-user-circle"> </i>
              Login Akun
            </a>
          </div> -->

          <!-- /.col -->

          <!-- /.col -->

        </div>

      <!-- </form> -->
      <?php echo(form_close()); ?>

      <div class="social-auth-links text-center">
        <p>- LAINNYA -</p>
        <a href="<?php echo(base_url("auth/login")); ?>" class="btn btn-block bg-gradient-success">
          <i class="fas fa-user-circle mr-2"></i>
          Login Akun
        </a>
      </div>

    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="<?php echo(base_url("asset/plugins/")); ?>jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo(base_url("asset/plugins/")); ?>bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo(base_url("asset/adminlte/")); ?>js/adminlte.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url("asset/plugins/jquery-validation/jquery.validate.min.js")); ?>"></script>
<!-- SweetAlert2 -->
<script src="<?php echo(base_url("asset/plugins/")); ?>sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo(base_url("asset/plugins/")); ?>toastr/toastr.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url("asset/plugins/")); ?>jquery-loading-overlay/dist/loadingoverlay.min.js"></script>
<script src="<?php echo(base_url("asset/")); ?>plugins/pace-progress/pace.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url("asset/script/init.js")); ?>"></script>

<script type="text/javascript">
  window.addEventListener("load", () => {

    const Toast = Swal.mixin({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      timer: 3000
    });

    $("#id-btn-reset").on("click", async () => {

      $.LoadingOverlay("show");

      $("#id-txt-kodeunit").val("");
      $("#id-txt-nomerva_bca").val("");
      $("#id-txt-namapemilik").val("");
      $("#id-txt-email").val("");
      $("#id-txt-msisdn").val("");
      $("#id-txt-nomerva_narobil").val("");
      $("#id-txt-nomerva_narobil").removeAttr("readonly");
      $("#id-btn-register").attr("disabled", "");

      $.LoadingOverlay("hide");
      await timeout(500);
      Toast.fire(
        'Pesan Sukses',
        `Berhasil direset`,
        'success'
      );
    });

    $("#id-alertsuccess").hide();
    $("#id-alertdanger").hide();

    <?php if($this->session->flashdata("error")) : ?>
      setTimeout(() => {
        Swal.fire(
          'Pesan Error',
          `<?php echo($this->session->flashdata("error")); ?>`,
          'error'
        );
      }, 500);
      <?php unset($_SESSION["error"]); ?>
    <?php endif; ?>

    <?php if($this->session->flashdata("success")) : ?>

      setTimeout(() => {
        Swal.fire(
          'Pesan Sukses',
          `<?php echo($this->session->flashdata("success")); ?>`,
          'success'
        );
      }, 500)

    <?php unset($_SESSION["success"]); ?>
    <?php endif; ?>

    $("#form_validation").validate({
      rules: {
        password: {
          required: true
        },
        retypepassword: {
          required: true,
          equalTo: "#id-txt-password"
        },
        msisdn: {
          required: true,
          number: true
        },
        email: {
          required: true
        }
      },
      errorPlacement: function(error, element) {
        $(element).addClass("is-invalid");
      },
      success: function(label, element) {
        $(element).removeClass("is-invalid");
      }
    });

    $("#id-btn-ceknomerva").on("click", () => {

      $.LoadingOverlay("show");
      let nomerva_narobil = $("#id-txt-nomerva_narobil").val();
      $.post(base_url("auth/check_nomerva_narobil"), {
        nomerva_narobil: nomerva_narobil
      })
      .done(async (r) => {
        $.LoadingOverlay("hide")
        await timeout(500);
        if(r.error) {

          Toast.fire(
            'Pesan Error',
            r.message,
            'error'
          );

        } else {

          $("#id-txt-nomerva_narobil").attr("readonly", true);
          $("#id-btn-register").removeAttr("disabled");
          let data = r.data;

          let kode_unit = data.kode_unit;
          let nomerva_bca = data.nomor_va_bca;
          let namapemilik = data.nama_pemilik;
          let email = data.email;
          let msisdn = data.msisdn;

          $("#id-txt-kodeunit").val(kode_unit);
          $("#id-txt-nomerva_bca").val(nomerva_bca);
          $("#id-txt-namapemilik").val(namapemilik);
          $("#id-txt-email").val(email);
          $("#id-txt-msisdn").val(msisdn);
          // $("#form_validation").submit();
          Toast.fire(
            'Pesan Sukses',
            r.message,
            'success'
          );

        }
      })
      .fail(e => {
        $.LoadingOverlay("hide")
        alert(e.toString());
      })
    });

  });

</script>
</body>
</html>
