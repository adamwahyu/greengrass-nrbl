
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="<?php echo(base_url("asset/img/green_grass.png")); ?>" type="image/png" sizes="16x16">
  <?php
          $perumahan = $this->db->query("select * from ref_perumahan where sts_active=1
          ")->row_array();
          $result = $perumahan['nama_perumahan'];

          ?>

  <title><?=$result?> | Halaman Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-name" content="<?php echo($this->security->get_csrf_token_name()); ?>" charset="utf-8">
  <meta name="csrf-token" content="<?php echo($this->security->get_csrf_hash()); ?>" charset="utf-8">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/plugins/")); ?>fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/plugins/")); ?>icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/adminlte/")); ?>css/adminlte.min.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/plugins/")); ?>sweetalert2-theme-bootstrap-4/bootstrap-4.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/plugins/")); ?>toastr/toastr.min.css">
  <link rel="stylesheet" href="<?php echo(base_url("asset/")); ?>plugins/pace-progress/themes/blue/pace-theme-flat-top.css">
  <link rel="stylesheet" type="text/css" href="<?php echo(base_url("asset/css/init.css")); ?>">
    <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
  <style media="screen">
      /* background-image: url("../../img/bcg_residenceone.jpg"); */
      body {
      /*font-family:muli,sans-serif;*/
      font-size: 0.9rem !important;
    }
      .login-page, .register-page {
        -ms-flex-align: center;
        align-items: center;
        padding-top: 0px !important;
        background-size: 100% 100%;
        background-repeat: no-repeat;
        background-image: url('<?php echo(base_url("asset/img/bg2.jpg")); ?>') !important;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        flex-direction: column;
        height: 100vh;
        -ms-flex-pack: center;
        justify-content: center;
      }
      .login-box-msg, .register-box-msg {
    margin: 0;
    padding: 0px;
    text-align: center;
}
  </style>

</head>
<body class="hold-transition login-page">

<div class="login-box">

  <div class="login-logo">
    <a href="<?php echo(base_url("auth/login")); ?>"></a>
  </div>

  <!-- /.login-logo -->
  <div class="card shadow">
    <div class="card-body login-card-body">
      <!-- <div class="ribbon-wrapper ribbon-xl">
        <div class="ribbon bg-secondary">
          Login Form
        </div>
      </div> -->
      <div class="register-logo">
        <a href="javascript:void(0)">
          <!-- <b>Forwards</b> -->
          <img style="width: 170px;" src="<?php echo(base_url("asset/img/green_grass.png")); ?>" />
        </a>
      </div>
      <p class="login-box-msg">
        <b>Login Form</b>
      </p>
      <?php echo(form_open("", array(
        "id" => "form_validation"
      ))); ?>

        <div class="form-group">
          <label for="">
            Username
          </label>
          <div class="input-group mb-1">
            <input type="text" name="kodeunit" class="form-control" placeholder="Ketik disini . . .">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="">
            Kata Sandi
          </label>
          <div class="input-group mb-1">
            <input type="password" name="password" class="form-control" placeholder="Ketik disini . . .">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 mt-2">
            <button type="submit" class="btn bg-gradient-primary btn-block">
              <i class="fas fa-sign-in-alt"></i>
              Masuk
            </button>
          </div>
        </div>
      <?php echo(form_close()); ?>

      <div class="social-auth-links text-center mb-3">
        <p>- LAINNYA -</p>
        <a href="<?php echo(base_url("auth/register")); ?>" class="btn btn-block bg-gradient-success">
          <i class="fas fa-users mr-2"></i>
          Daftar Akun
        </a>
        <a href="<?php echo(base_url("check_va_unit")); ?>" class="btn btn-block bg-gradient-secondary">
          <i class="fas fa-search mr-2"></i>
          Cek VA Unit
        </a>
      </div>
      <!-- /.social-auth-links -->

      <!-- <p class="mb-1">
        <a href="forgot-password.html">I forgot my password</a>
      </p> -->
      <!-- <p class="mb-0">
        <a href="<?php echo(base_url("auth/register")); ?>" class="text-center">Register a new membership</a>
      </p> -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?php echo(base_url("asset/plugins/")); ?>jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo(base_url("asset/plugins/")); ?>bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo(base_url("asset/adminlte/")); ?>js/adminlte.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?php echo(base_url("asset/plugins/")); ?>sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script type="text/javascript" src="<?php echo(base_url("asset/plugins/jquery-validation/jquery.validate.min.js")); ?>"></script>
<script src="<?php echo(base_url("asset/plugins/")); ?>toastr/toastr.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url("asset/plugins/")); ?>jquery-loading-overlay/dist/loadingoverlay.min.js"></script>
<script src="<?php echo(base_url("asset/")); ?>plugins/pace-progress/pace.min.js"></script>
<script type="text/javascript">

  window.addEventListener("load", () => {

    $("#form_validation").validate({
      rules: {
        kodeunit: {
          required: true
        },
        password: {
          required: true
        }
      },
      errorPlacement: function(error, element) {
        $(element).addClass("is-invalid");
      },
      success: function(label, element) {
        $(element).removeClass("is-invalid");
      }
    });

    <?php if($this->session->flashdata("error")) : ?>

      setTimeout(() => {

        Swal.fire(
          'Pesan Error',
          `<?php echo($this->session->flashdata("error")); ?>`,
          'error'
        );

      }, 500);


      <?php unset($_SESSION["error"]); ?>
    <?php endif; ?>

    <?php if($this->session->flashdata("success")) : ?>

      setTimeout(() => {

        Swal.fire(
          'Pesan Sukses',
          `<?php echo($this->session->flashdata("success")); ?>`,
          'success'
        );

      }, 500);


      <?php unset($_SESSION["success"]); ?>
    <?php endif; ?>
  });
</script>
</body>
</html>
