<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	  <title>AdminLTE 2 | Simple Tables</title>
	  <!-- Tell the browser to be responsive to screen width -->
	  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="<?php echo(base_url("asset/adminlte/")); ?>css/adminlte.min.css">
</head>
<body>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th rowspan="2">Kode Unit</th>
				<th rowspan="2">Nama Pemilik</th>
			<?php foreach($mst_tahun as $tahun) : ?>
				<th colspan="12">
					<?php echo($tahun->tahun); ?>
				</th>
			<?php endforeach; ?>
			</tr>
			<tr>
			<?php foreach($mst_tahun as $tahun) : ?>
				<?php foreach($mst_bulan as $bulan) : ?>
					<th colspan="1">
						<?php echo($bulan->id_bulan); ?>
					</th>
				<?php endforeach; ?>
			<?php endforeach; ?>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>asds</td>
				<td>asds</td>
				<td>asds</td>
			</tr>
		</tbody>
	</table>
</body>
</html>