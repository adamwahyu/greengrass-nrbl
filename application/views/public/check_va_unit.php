
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="icon" href="<?php echo(base_url("asset/img/green_grass.png")); ?>" type="image/png" sizes="16x16">
  <?php
          $perumahan = $this->db->query("select * from ref_perumahan where sts_active=1
          ")->row_array();
          $result = $perumahan['nama_perumahan'];

          ?>

  <title><?=$result?> | Pencarian Nomor VA</title>
  <meta name="csrf-name" content="<?php echo($this->security->get_csrf_token_name()); ?>" charset="utf-8">
  <meta name="csrf-token" content="<?php echo($this->security->get_csrf_hash()); ?>" charset="utf-8">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/")); ?>plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/")); ?>adminlte/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/plugins/")); ?>datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="<?php echo(base_url("asset/")); ?>plugins/pace-progress/themes/blue/pace-theme-flat-top.css">
  <link rel="stylesheet" type="text/css" href="<?php echo(base_url("asset/")); ?>plugins/jquery-ui/jquery-ui.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo(base_url("asset/css/init.css")); ?>">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

  <script type="text/javascript">
    const base_url = (url = "") => {
      return `<?php echo(base_url("")); ?>${url}`
    }
  </script>
  <style type="text/css">
    body {
      /*font-family:muli,sans-serif;*/
      font-size: 0.9rem !important;
    }
    .row > .col-md-6 > .dataTables_filter {
      visibility: hidden;
      display: none;
    }
    .content-wrapper {
        -ms-flex-align: center;
        align-items: center;
        background-size: 100% 100%;
        background-repeat: no-repeat;
        background-image: url('<?php echo(base_url("asset/img/bg2.jpg")); ?>') !important;
        -ms-flex-direction: column;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
      }

      div > #id-datatable_info {
        display: none;
      }

  </style>
</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white shadow" style="border-bottom: 1px solid black;">
    <div class="container">
      <a href="<?php echo(base_url("/")); ?>" class="navbar-brand">
        <i class="fa fa-arrow-left"></i>
        <span class="brand-text font-weight-light">
          Kembali
        </span>
      </a>
      <a href="<?php echo(base_url("/")); ?>" class="navbar-brand">
        <!-- <img src="<?php echo(base_url("asset/img/green_grass.png")); ?>" alt="AdminLTE Logo" class="brand-image"
             style="opacity: .8"> -->
      </a>
     <!--  <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
      </div> -->

    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="card shadow">
              <div class="card-header">
                <!-- <h3 class="m-0 text-dark"> Pencarian <small>Nomer VA</small></h3> -->
                <div class="register-logo">
                  <a href="javascript:void(0)">
                    <img style="width: 170px;" src="<?php echo(base_url("asset/img/green_grass.png")); ?>" />
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-2 mx-auto">
                    <div class="form-group mb-0 text-center">
                      <label>
                        UNIT
                      </label>
                      <input
                        id="id_search"
                        type="text"
                        name="search"
                        class="form-control text-center" id="id-txt-kodeunit" placeholder="Ketik disini . . ." value="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="id_card_search" class="card shadow" style="display: none;">
              <div class="card-body">
                <div class="table-responsive">
                  <table id="id-datatable" class="table table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center">Kode Unit</th>
                        <th class="text-center">Nama</th>
                        <th class="text-center">Cluster</th>
                        <th class="text-center">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer shadow" style="border-top: 1px solid black;">
    <!-- To the right -->
    <!-- <div class="float-right d-none d-sm-inline">
      Anything you want
    </div> -->
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2019 <a href="<?php echo(base_url("/")); ?>">Forwards</a>.</strong> All rights reserved.
  </footer>
</div>

<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Detil Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">

            <div class="col-md-6">
              <input type="hidden" name="id_unit" id="id_unit">
              <div class="form-group">
                <label for="id_kodeunit">
                  Kode Unit
                </label>
                <input type="text" class="form-control" id="id_kodeunit" placeholder="-" readonly>
              </div>
              <div class="form-group">
                <label for="id_luasunit">
                  Nominal IPL
                </label>
                <input type="text" class="form-control" id="id_luasunit" placeholder="-" readonly>
              </div>
              <div class="form-group">
                <label for="id_nomerva_bca">
                  Nomer VA BCA
                </label>
                <input type="text" class="form-control" id="id_nomerva_bca" placeholder="-" readonly>
              </div>
              <div class="form-group">
                <label for="id_nomerva_narobil">
                  Nomer VA Narobil
                </label>
                <input type="text" class="form-control" id="id_nomerva_narobil" placeholder="-" readonly>
              </div>
              <div class="form-group">
                <label for="id_tarif">
                  Tarif
                </label>
                <input type="text" class="form-control" id="id_tarif" placeholder="-" readonly>
              </div>
          </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="id_namapemilik">
                  Nama Pemilik
                </label>
                <input type="text" class="form-control" id="id_namapemilik" placeholder="-" readonly>
              </div>

              <div class="form-group">
                <label for="id_msisdn">
                  MSISDN
                </label>
                <input type="text" class="form-control" id="id_msisdn" placeholder="-" readonly="">
              </div>
              <div class="form-group">
                <label for="id_email">
                  Email
                </label>
                <input type="text" class="form-control" id="id_email" placeholder="-" readonly="">
              </div>
              <div class="form-group">
                <label for="status-huni">
                  Status Hunian
                </label>
                <input type="text" class="form-control" readonly="" id="id_sts_huni">
              </div>
              <!-- <div class="form-group">
                <label for="id_nilai_tunggakan">
                  Jumlah Tunggakan
                </label>
                <input type="text" class="form-control" id="id_nilai_tunggakan" placeholder="-" readonly>
              </div> -->
            </div>

            <div class="col-md-12 col-xs-12">
              <div class="form-group">
                <label for="id_notes">
                  Notes
                </label>
                <textarea class="form-control" id="id_notes" name="notes" readonly=""></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>

<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?php echo(base_url("asset/")); ?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo(base_url("asset/")); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo(base_url("asset/plugins/")); ?>datatables/jquery.dataTables.js"></script>
<script src="<?php echo(base_url("asset/plugins/")); ?>datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo(base_url("asset/")); ?>adminlte/js/adminlte.min.js"></script>
<script src="<?php echo(base_url("asset/plugins/")); ?>inputmask/min/jquery.inputmask.bundle.min.js"></script>
<script src="<?php echo(base_url("asset/")); ?>plugins/pace-progress/pace.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url("asset/plugins/")); ?>jquery-loading-overlay/dist/loadingoverlay.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url("asset/plugins/jquery-ui/jquery-ui.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo(base_url("asset/script/init.js")); ?>"></script>
<script type="text/javascript">
  var table;

  const onDetail = (id_unit, kode_unit) => {

    $.LoadingOverlay("show");

    $.post(base_url("check_va_unit/ajax_get_unit_by_kodeunit"), {
      id_unit: id_unit
    })
    .done(async (r) => {

      let data = r.data;
      $("#id_kodeunit").val(data.kode_unit);
      $("#id_namapemilik").val(data.nama_pemilik);
      $("#id_luasunit").val(data.luas_unit);
      $("#id_nomerva_narobil").val(data.nomor_va_narobil);
      $("#id_nomerva_bca").val(data.nomor_va_bca);
      $("#id_sts_huni").val(data.sts_huni);
      $("#id_msisdn").val(data.msisdn);
      $("#id_email").val(data.email);
      $("#id_tarif").val(rupiah(data.tarif));
      $("textarea#id_notes").val(data.notes);
      // $("#id_nilai_tunggakan").val("-");

      //
      $("#btn-create-tunggakan-invoice-pdf").attr("href", base_url(`unit/create_tunggakan_invoice_pdf/${data.id_unit}`));

      $("#modal-lg").modal({
        show: true
      });

      $('.modal-dialog').draggable({
        handle: ".modal-header"
      });
      $('.modal-content').resizable();

      // table_trans_kewajiban.columns.adjust().draw();

      $.LoadingOverlay("hide");

    })
    .fail(e => {
      alert(e.toString());
    })
  }

  window.addEventListener("load", () => {

    $("#id_search").on("keyup", function() {
      this.value = this.value.toUpperCase();
      if(this.value == "") {
        $("#id_card_search").hide();
        return;
      }
      table.search(this.value).draw();
      $("#id_card_search").show();
    });

    table = $("#id-datatable").DataTable({
      'language': {
        "emptyTable": "Data tidak ditemukan"
      },
      'autoWidth': false,
      'lengthChange': false,
      'searching': true,
      'ordering': false,
      'processing': true,
      'serverSide': true,
      'paging': false,
      'pageLength': 1,
      'ajax': {
          'url': base_url("check_va_unit/get_datatables"),
          'type': 'GET'
      },
      drawCallback: () => {
        // $('[data-toggle="tooltip"]').tooltip()
      },
      columnDefs: [
        // {
        //     targets: [0],
        //     className: 'text-right',
        //     width: "5%"
        // },
        {
          targets: [-1],
          className: "text-center"
        }
      ]
    });

  });
</script>
</body>
</html>
