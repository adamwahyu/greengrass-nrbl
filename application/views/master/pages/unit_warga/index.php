

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Tabel Unit
            </h1>
          </div>
          <div class="col-sm-6">

          </div>
        </div>
      </div> --><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Daftar Unit
          </h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="id-datatable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">Kode Unit</th>
                  <th class="text-center">Nama Pemilik</th>
                  <th class="text-center">Nominal IPL</th>
                  <th class="text-center">Nomer VA Narobil</th>
                  <th class="text-center">Status Hunian</th>
                  <th class="text-center">Tarif</th>
                  <th class="text-center">Indikator</th>
                  <th class="text-center">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Detil Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">

            <div class="col-md-6">
              <input type="hidden" name="id_unit" id="id_unit">
              <div class="form-group">
                <label for="id_kodeunit">
                  Kode Unit
                </label>
                <input type="text" class="form-control" id="id_kodeunit" placeholder="-" readonly>
              </div>
              <div class="form-group">
                <label for="id_luasunit">
                  Nominal IPL
                </label>
                <input type="text" class="form-control" id="id_luasunit" placeholder="-" readonly>
              </div>
              <div class="form-group">
                <label for="id_nomerva_bca">
                  Nomer VA BCA
                </label>
                <input type="text" class="form-control" id="id_nomerva_bca" placeholder="-" readonly>
              </div>
              <div class="form-group">
                <label for="id_nomerva_narobil">
                  Nomer VA Narobil
                </label>
                <input type="text" class="form-control" id="id_nomerva_narobil" placeholder="-" readonly>
              </div>
              <div class="form-group">
                <label for="id_tarif">
                  Tarif
                </label>
                <input type="text" class="form-control" id="id_tarif" placeholder="-" readonly>
              </div>
          </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="id_namapemilik">
                  Nama Pemilik
                </label>
                <input type="text" class="form-control" id="id_namapemilik" placeholder="-" readonly>
              </div>

              <div class="form-group">
                <label for="id_msisdn">
                  MSISDN
                </label>
                <input type="text" class="form-control" id="id_msisdn" placeholder="-" readonly="">
              </div>
              <div class="form-group">
                <label for="id_email">
                  Email
                </label>
                <input type="text" class="form-control" id="id_email" placeholder="-" readonly="">
              </div>
              <div class="form-group">
                <label for="status-huni">
                  Status Hunian
                </label>
                <input type="text" class="form-control" readonly="" id="id_sts_huni">
              </div>
              <div class="form-group">
                <label for="id_nilai_tunggakan">
                  Jumlah Tunggakan
                </label>
                <input type="text" class="form-control" id="id_nilai_tunggakan" placeholder="-" readonly>
              </div>
            </div>

            <div class="col-md-12 col-xs-12">
              <div class="form-group">
                <label for="id_notes">
                  Notes
                </label>
                <textarea class="form-control" id="id_notes" name="notes" readonly=""></textarea>
              </div>
            </div>
          </div>
        <!-- </div> -->
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="modal-title" style="text-align: center;">
                    Tagihan
                  </h4>
                </div>
                <div class="col-md-12 mb-2"></div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="table-responsive">
                <table id="id-datatable-trans-kewajiban" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th class="text-center">No</th>
                      <th class="text-center">Tahun</th>
                      <th class="text-center">Bulan</th>
                      <th class="text-center">Nominal IPL</th>
                      <th class="text-center">Tarif Permeter</th>
                      <th class="text-center">Kewajiban</th>
                      <th class="text-center">Pemenuhan</th>
                      <th class="text-center">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    var table, table_trans_kewajiban;

    const onCreateInvoice = (ctx) => {

      Swal.fire({
        title: 'Tanggal Jatuh Tempo',
        html: `<div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control float-right" id="id_tgl_jatuh_tempo" autocomplete="off">
                  </div>
                  <!-- /.input group -->
                </div>`,
        focusConfirm: false,
        showCancelButton: true,
        preConfirm: () => {
          let tgl_jatuh_tempo = document.getElementById('id_tgl_jatuh_tempo').value;
          return tgl_jatuh_tempo;
        },
        onOpen: () => {
          $("#id_tgl_jatuh_tempo").daterangepicker({
            singleDatePicker: true,
            locale: {
              "format": "DD-MM-YYYY"
            }
          });
        }
      }).then(r => {

        if(r.value) {

          let action = $(ctx).data("href");
          let form = $("<form />", {
            action: action,
            method: "POST"
          });

          let txtCsrf = $("<input />", {
            type: "hidden",
            name: $("meta[name='csrf-name']").attr("content"),
            value: $("meta[name='csrf-token']").attr("content")
          });
          let txtTempo = $("<input />", {
            type: "hidden",
            name: "tanggal_jatuh_tempo",
            value: r.value
          });
          form.append(txtCsrf);
          form.append(txtTempo);
          form.appendTo("body");
          form.submit();

        }
        

      }).catch(e => {
        alert(e.toString());
      });

    }

    const onDetail = (id_unit, kode_unit) => {

      $.LoadingOverlay("show");

      $.post(base_url("unit_warga/ajax_get_unit_by_kodeunit"), {
        id_unit: id_unit
      })
      .done(async (r) => {

        let data = r.data;
        $("#id_kodeunit").val(data.kode_unit);
        $("#id_namapemilik").val(data.nama_pemilik);
        $("#id_luasunit").val(data.luas_unit);
        $("#id_nomerva_narobil").val(data.nomor_va_narobil);
        $("#id_nomerva_bca").val(data.nomor_va_bca);
        $("#id_sts_huni").val(data.sts_huni);
        $("#id_msisdn").val(data.msisdn);
        $("#id_email").val(data.email);
        $("#id_tarif").val(rupiah(data.tarif));
        $("textarea#id_notes").val(data.notes);
        $("#id_nilai_tunggakan").val(data.nilai_tunggakan);
        
        await onTagihan(kode_unit);

        $("#modal-lg").modal({
          show: true
        });

        $('.modal-dialog').draggable({
          handle: ".modal-header"
        });
        $('.modal-content').resizable();

        // table_trans_kewajiban.columns.adjust().draw();

        $.LoadingOverlay("hide");

      })
      .fail(e => {
        alert(e.toString());
      })
    }

    const onTagihan = (kode_unit) => {

      return new Promise((resolve, reject) => {

        if(typeof table_trans_kewajiban != 'undefined') {
          table_trans_kewajiban.destroy();
        }
        table_trans_kewajiban = $("#id-datatable-trans-kewajiban").DataTable({
          'language': {
            "emptyTable": "Data tidak ditemukan"
          },
          // scrollX:        true,
          // scrollCollapse: true,
          'autoWidth': false,
          'lengthChange': false,
          'searching': true,
          'ordering': false,
          'processing': true,
          'serverSide': true,
          'ajax': {
              'url': base_url("unit_warga/get_datatables_trans_kewajiban"),
              'type': 'GET',
              'data': {
                kode_unit: kode_unit
              }
          },
          drawCallback: () => {
            $('[data-toggle="tooltip"]').tooltip()
          },
          initComplete: () => {
            resolve()
          },
          columnDefs: [
            {
                targets: [0, 1, 2, 3, 4,5,6, 7],
                className: 'text-right'
            },
            {
                targets: [-1],
                className: 'text-left'
            }
          ]
        });

      })

    }

    window.addEventListener("load", () => {

      table = $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("unit_warga/get_datatables"),
            'type': 'GET'
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs: [
          {
              targets: [0, 3, 4,5,6],
              className: 'text-right'
          },
          {
            targets: -1,
            className: 'text-center'
          }
        ]
      });

    });
  </script>
