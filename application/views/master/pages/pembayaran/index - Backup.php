

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Daftar Pembayaran
          </h3>

          <div class="card-tools">

            <a href="<?php echo(base_url("pembayaran/create")); ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Tambah Data">
              <i class="fas fa-plus"></i></a>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="id-datatable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th class="text-center" style="">No</th>
                  <th class="text-center" style="">Kode SPP</th>
                  <th class="text-center" style="">Kode Unit</th>
                  <th class="text-center" style="">Nama Pemilik</th>
                  <th class="text-center" style="">Tgl SPP</th>
                  <th class="text-center" style="">Periode Bulan</th>
                  <th class="text-center" style="">Nilai SPP</th>
                  <th class="text-center" style="">Metode Pembayaran</th>
                  <th class="text-center" style="">Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <!-- Footer -->
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Detil Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            
            <div class="col-md-12 mt-2 mb-2">
              <a href="#" class="btn btn-danger" id="id_btn_cetak_invoice_tanda_terima_sementara">
                <i class="fas fa-file-pdf"></i>
                Cetak TTS Invoice PDF
              </a>
            </div>
            <div class="col-xs-12 col-md-12">
              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_kodespp">
                      Kode SPP
                    </label>
                    <input type="text" class="form-control" id="id_kodespp" placeholder="-" readonly>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_kodeunit">
                      Kode Unit
                    </label>
                    <input type="text" class="form-control" id="id_kodeunit" placeholder="-" readonly>
                  </div>
                </div>
              </div>

              <div class="row">
              <div class="col-md-12">
              <div class="form-group">
                <label for="id_namapemilik">
                  Nama Pemilik
                </label>
                <input type="text" class="form-control" id="id_namapemilik" placeholder="-" readonly>
              </div>
            </div>
            </div>

              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_nomervanarobil">
                      No Va Narobil
                    </label>
                    <input type="text" class="form-control" id="id_nomervanarobil" placeholder="-" readonly>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_nomervabca">
                      No Va BCA
                    </label>
                    <input type="text" class="form-control" id="id_nomervabca" placeholder="-" readonly>
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_tglspp">
                      Tgl SPP
                    </label>
                    <input type="text" class="form-control" id="id_tglspp" placeholder="-" readonly>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_bulanawal">
                      Bulan Awal
                    </label>
                    <input type="text" class="form-control" id="id_bulanawal" placeholder="-" readonly>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_status">
                      Tahun Awal
                    </label>
                    <input type="text" class="form-control" id="id_tahunawal" placeholder="-" readonly>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_periodespp">
                      Periode Bulan
                    </label>
                    <input type="text" class="form-control" id="id_periodespp" placeholder="-" readonly>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_status">
                      Luas Unit
                    </label>
                    <input type="text" class="form-control" id="id_luasunit" placeholder="-" readonly>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_periodespp">
                      Tarif Permeter
                    </label>
                    <input type="text" class="form-control" id="id_tarifpermeter" placeholder="-" readonly>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_status">
                      Nilai SPP
                    </label>
                    <input type="text" class="form-control" id="id_nilaispp" placeholder="-" readonly>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_periodespp">
                      Metode Pembayaran
                    </label>
                    <input type="text" class="form-control" id="id_metodepembayaran" placeholder="-" readonly>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_status">
                      Tanggal Bayar
                    </label>
                    <input type="text" class="form-control" id="id_tanggalbayar" placeholder="-" readonly>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_periodespp">
                      Nilai Kewajiban
                    </label>
                    <input type="text" class="form-control" id="id_nilaikewajiban" placeholder="-" readonly>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_status">
                      Nilai Bayar
                    </label>
                    <input type="text" class="form-control" id="id_nilaibayar" placeholder="-" readonly>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_periodespp">
                      Nilai Selisih
                    </label>
                    <input type="text" class="form-control" id="id_nilaiselisih" placeholder="-" readonly>
                  </div>
                </div>
              </div>

            </div>

            <div class="col-md-12">
              <div class="form-group">
                <label for="">
                  Bukti Pembayaran
                </label>
                <img id="id_bukti" src="" width="100%" height="auto">
              </div>
            </div>

          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">

    const onView = (id) => {
      


      $.LoadingOverlay("show");

      $.post(base_url("pembayaran/ajax_get_pembayaran_by_id"), {
        id: id
      })
      .done(r => {

        let data = r.data;

        $("#id_kodespp").val(data.id_spp);
        $("#id_kodeunit").val(data.kode_unit);
        $("#id_namapemilik").val(data.nama_pemilik);
        $("#id_nomervanarobil").val(data.nomor_va_narobil);
        $("#id_nomervabca").val(data.nomor_va_bca);
        $("#id_tglspp").val(data.tgl_spp);
        $("#id_bulanawal").val(data.bulan_awal);
        $("#id_tahunawal").val(data.tahun_awal);
        $("#id_periodespp").val(data.periode_spp);
        $("#id_luasunit").val(data.luas_unit);
        $("#id_tarifpermeter").val(rupiah(data.tarif_permeter));
        $("#id_nilaispp").val(rupiah(data.nilai_spp));
        $("#id_metodepembayaran").val(data.desc_metode_pembayaran);
        $("#id_tanggalbayar").val(data.tgl_bayar);
        $("#id_nilaikewajiban").val(rupiah(data.nilai_kewajiban));
        $("#id_nilaibayar").val(rupiah(data.nilai_bayar));
        $("#id_nilaiselisih").val(data.selisih);
        $('#id_bukti').attr('src', base_url() + '/upload/pembayaran/'+ data.file_bukti_bayar);

        $("#modal-xl").modal({
          show: true
        });

        $('.modal-dialog').draggable({
          handle: ".modal-header"
        });
        $('.modal-content').resizable();

        $.LoadingOverlay("hide");

      })
      .fail(e => {
        alert(e.toString());
        $.LoadingOverlay("hide");
      });

    }

    window.addEventListener("load", () => {

      $("#id_btn_cetak_invoice_tanda_terima_sementara").on("click", function() {
        console.log(this);
      });

      table = $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        "autoWidth": false,
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("pembayaran/get_datatables"),
            'type': 'GET'
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs: [
          {
              targets: [1,5,6],
              className: 'text-right'
          },
          {
              targets: [-1],
              className: 'text-center'
          },
          { "width": "5%", "targets": [5] },
          { "width": "5%", "targets": [1] },
          { "width": "3%", "targets": [0] },
        ]
      });

    });
  </script>
