

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detil Form Pembayaran</h1>
          </div>
          <div class="col-sm-6">
            <!-- <ol class="breadcrumb float-sm-right"> -->
              <!-- <li class="breadcrumb-item"><a href="#">Home</a></li> -->
              <!-- <li class="breadcrumb-item active">Blank Page</li> -->
            <!-- </ol> -->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Form Pembayaran
          </h3>
        </div>
        <div class="card-body">
          <?php echo(form_open_multipart("", [
            "id" => "form_validation"
          ])); ?>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode_unit">
                    Kode Unit
                  </label>
                  <select id="id_kode_unit" name="kode_unit" class="form-control" required="">
                    <option value="">
                      -- PILIH --
                    </option>
                    <?php foreach($v_data->unit as $unit) : ?>
                      <option value="<?php echo($unit->kode_unit); ?>">
                        <?php echo($unit->kode_unit); ?>
                      </option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="id_namapemilik">
                    Nama Pemilik
                  </label>
                  <input class="form-control" type="text" id="id_namapemilik" name="namapemilik" value="" readonly="" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="id_email">
                    Email
                  </label>
                  <input class="form-control" type="text" id="id_email" name="email" value="" readonly="" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="id_msisdn">
                    MSISDN
                  </label>
                  <input class="form-control" type="text" id="id_msisdn" name="msisdn" value="" readonly="" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="bulan_awal">
                    Bulan Awal
                  </label>
                  <input class="form-control" type="text" id="id_bulan_awal" name="bulan_awal" value="" readonly="" />
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="tahun_awal">
                    Tahun Awal
                  </label>
                  <input class="form-control" type="text" id="id_tahun_awal" name="tahun_awal" value="" readonly="" />
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label for="periode_spp">
                    Jumlah Periode Pembayaran
                  </label>
                  <input onkeyup="form_input_numberonly(event)" id="id_periode_spp" name="periode_spp" maxlength="2" value="" class="form-control" readonly="" />
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="periode_spp">
                    Jumlah Tunggakan
                  </label>
                  <input onkeyup="form_input_numberonly(event)" id="id_periode_spp_tunggakan" name="periode_spp_tunggakan" maxlength="2" value="" class="form-control" readonly="" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="id_jml_tagihan_pembayaran">
                    Jumlah Tunggakan Pembayaran
                  </label>
                  <input class="form-control" type="text" id="id_jml_tagihan_pembayaran" name="jml_tagihan_pembayaran" value="" readonly="" />
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="id_jml_pembayaran">
                    Jumlah Pembayaran
                  </label>
                  <input class="form-control" type="text" id="id_jml_pembayaran" name="jml_pembayaran" value="" readonly="" />
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="metode_pembayaran">
                    Metode Pembayaran
                  </label>
                  <select id="id_metode_pembayaran" name="metode_pembayaran" class="form-control" required="">
                    <option value="">
                      -- PILIH --
                    </option>
                    <?php foreach($v_data->metode_pembayaran as $metode) : ?>
                      <option value="<?php echo($metode->id_metode_pembayaran); ?>">
                        <?php echo($metode->desc_metode_pembayaran); ?>
                      </option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="id_tgl_pembayaran">
                    Tanggal Pembayaran
                  </label>
                  <input type="text" class="form-control" id="id_tgl_pembayaran" name="tgl_pembayaran" />
                </div>
              </div>

              <div class="col-md-6" id="id_file_bukti_layout">
                <div class="form-group">
                  <label for="id_file_bukti">
                    Berkas Bukti Pembayaran
                    <label class="badge-warning" style="font-size: 14px;">File yang dapat diupload berextensi: .jpg, .jpeg, .png,  .pdf</label>
                  </label>
                  <input accept="image/png, image/jpeg, application/pdf" class="form-control" type="file" id="id_file_bukti" name="file_bukti" required="" />
                </div>
              </div>

              <div class="col-md-12 mb-2"></div>

              <div class="col-md-6">
                <button type="submit" class="btn btn-primary">
                  <i class="fas fa-save"> </i>
                  Simpan
                </button>
                <a href="<?php echo(base_url("pembayaran")); ?>" class="btn btn-danger">
                  <i class="fas fa-arrow-left"> </i>
                  Kembali
                </a>
              </div>
            </div>
          <?php echo(form_close()); ?>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">

    const clearSelect = function(selector) {

      if($(`#${selector} option`).length > 0) {
            
        $(`#${selector} option`).filter((i, node) => {
          
          if(i == 0) {
            return false;
          }
          return node;

        }).remove();
        
      };
    }

    window.addEventListener("load", () => {

      $('#id_tgl_pembayaran').daterangepicker({
        "singleDatePicker": true,
        "locale": {
          "format": "DD/MM/YYYY"
        },
      });

      $("#id_metode_pembayaran").on("change", function(){
        if($(this).val() == "7") {
          $("#id_file_bukti").removeAttr("required");
          $("#id_file_bukti_layout").hide();
        } else {
          $("#id_file_bukti").attr("required", "");
          $("#id_file_bukti_layout").show();
        }
      });
      $("#id_kode_unit").select2({
        theme: "bootstrap4"
      });

      $("#id_periode_spp").on("keyup", function() {
        $.LoadingOverlay("show");

        let periode_spp = $(this).val();

        if(periode_spp.trim() == "" || periode_spp.match(/[a-z]/i)) {
          $.LoadingOverlay("hide");
          return;
        }
        
        let kode_unit = $("#id_kode_unit").val();
        $.post(base_url("pembayaran/get_nominal_pembayaran"), {
          kode_unit: kode_unit,
          jml_periode: periode_spp
        }).done(r => {
          if(r.error) {

          } else {
            $("#id_jml_tagihan_pembayaran").val(rupiah(r.data.nominal_pembayaran));
            $("#id_jml_pembayaran").val(rupiah(r.data.nominal_pembayaran));
          }
          $.LoadingOverlay("hide");
        })
        .fail(e => {
          $.LoadingOverlay("hide");
        });

      });

      $("#id_kode_unit").on("change", function() {

        $("#id_periode_spp").val(0);
        $.LoadingOverlay("show");

        let kode_unit = $(this).val();

        if(kode_unit == "") {

          $("#id_namapemilik").val("");
          $("#id_email").val("");
          $("#id_msisdn").val("");
          $("#id_bulan_awal").val("");
          $("#id_tahun_awal").val("");

          $("#id_jml_tagihan_pembayaran").val(0);
          $("#id_jml_pembayaran").val(0);


          $("#id_periode_spp").attr("readonly", true);
          $.LoadingOverlay("hide");
          return;
        }
        $.post(base_url("pembayaran/get_unit_by_kode_unit"), {
          kode_unit: kode_unit
        })
        .done((r) => {
          let data = r.data;
          
          $("#id_namapemilik").val(data.nama_pemilik);
          $("#id_email").val(data.email);
          $("#id_msisdn").val(data.msisdn);
          $("#id_bulan_awal").val(data.bulan_awal);
          $("#id_tahun_awal").val(data.tahun_awal);

          $("#id_jml_tagihan_pembayaran").val(0);
          $("#id_jml_pembayaran").val(0);
          $("#id_periode_spp_tunggakan").val(data.jml_tagihan);
          $("#id_periode_spp").val(0).trigger("keyup");
          
          $("#id_periode_spp").removeAttr("readonly");

          $.LoadingOverlay("hide");
        })
        .fail(e => {
          $.LoadingOverlay("hide");
        });

      });

      $("#form_validation").validate({
        rules: {
          periode_spp: {
            range: [1, 99]
          },
          tgl_pembayaran: {
            required: true
          }
        },
        errorPlacement: function(error, element) {
          $(element).addClass("is-invalid");
        },
        success: function(label, element) {
          $(element).removeClass("is-invalid");
        }
      });
    });
  </script>
  