

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detil Form Pembayaran</h1>
          </div>
          <div class="col-sm-6">
            <!-- <ol class="breadcrumb float-sm-right"> -->
              <!-- <li class="breadcrumb-item"><a href="#">Home</a></li> -->
              <!-- <li class="breadcrumb-item active">Blank Page</li> -->
            <!-- </ol> -->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Form Pembayaran
          </h3>
        </div>
        <div class="card-body">
          <?php echo(form_open_multipart("", [
            "id" => "form_validation"
          ])); ?>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode_unit">
                    Kode Unit
                  </label>
                  <select id="id_kode_unit" name="kode_unit" class="form-control">
                    <option value="">
                      -- PILIH --
                    </option>
                    <?php foreach($v_data->unit as $unit) : ?>
                      <option value="<?php echo($unit->kode_unit); ?>">
                        <?php echo($unit->kode_unit); ?>
                      </option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="id_namapemilik">
                    Nama Pemilik
                  </label>
                  <input class="form-control" type="text" id="id_namapemilik" name="namapemilik" value="" readonly="" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="id_email">
                    Email
                  </label>
                  <input class="form-control" type="text" id="id_email" name="email" value="" readonly="" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="id_msisdn">
                    MSISDN
                  </label>
                  <input class="form-control" type="text" id="id_msisdn" name="msisdn" value="" readonly="" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="bulan_awal">
                    Bulan Awal
                  </label>
                  <input class="form-control" type="text" id="id_bulan_awal" name="bulan_awal" value="" readonly="" />
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="tahun_awal">
                    Tahun Awal
                  </label>
                  <input class="form-control" type="text" id="id_tahun_awal" name="tahun_awal" value="" readonly="" />
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="periode_spp">
                    Jumlah Periode Pembayaran
                  </label>
                  <div id="f-periode-spp">
                    <input id="id_periode_spp" name="periode_spp" value="" class="form-control" readonly />
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="id_jml_tagihan_pembayaran">
                    Jumlah Tunggakan Pembayaran
                  </label>
                  <input class="form-control" type="text" id="id_jml_tagihan_pembayaran" name="jml_tagihan_pembayaran" value="" readonly="" />
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="id_jml_pembayaran">
                    Jumlah Pembayaran
                  </label>
                  <input class="form-control" type="text" id="id_jml_pembayaran" name="jml_pembayaran" value="" readonly="" />
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="metode_pembayaran">
                    Metode Pembayaran
                  </label>
                  <select id="id_metode_pembayaran" name="metode_pembayaran" class="form-control">
                    <option value="">
                      -- PILIH --
                    </option>
                    <?php foreach($v_data->metode_pembayaran as $metode) : ?>
                      <option value="<?php echo($metode->id_metode_pembayaran); ?>">
                        <?php echo($metode->desc_metode_pembayaran); ?>
                      </option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="col-md-4 mx-auto">
                <div class="form-group">
                  <label for="id_file_bukti">
                    Berkas Bukti Pembayaran
                  </label>
                  <input class="form-control" type="file" id="id_file_bukti" name="file_bukti" />
                </div>
              </div>

              <div class="col-md-12 mb-2"></div>

              <div class="col-md-6">
                <button type="submit" class="btn btn-primary">
                  <i class="fas fa-save"> </i>
                  Simpan
                </button>
                <a href="<?php echo(base_url("pembayaran")); ?>" class="btn btn-danger">
                  <i class="fas fa-arrow-left"> </i>
                  Kembali
                </a>
              </div>
            </div>
          <?php echo(form_close()); ?>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    const clearSelect = function(selector) {

        if($(`#${selector} option`).length > 0) {
              
          $(`#${selector} option`).filter((i, node) => {
            
            if(i == 0) {
              return false;
            }
            return node;

          }).remove();
          
        };
      }

    const onChangePeriodeSpp = (ctx) => {
      if($(ctx).val() == "") {
        $("#id_jml_pembayaran").val(0);
        return;
      }
      $.LoadingOverlay("show");
      let kode_unit = $("#id_kode_unit").val();
      let jml_periode_spp = $(ctx).val();

      $.post(base_url("pembayaran/get_jml_tagihan_pembayaran_by_kode_unit"), {
        kode_unit: kode_unit,
        jml_periode: jml_periode_spp
      })
      .done(r => {
        let data = r.data;
        $("#id_jml_pembayaran").val(data.jml_tagihan_pembayaran);
        $.LoadingOverlay("hide");

      })
      .fail(e => {
        $.LoadingOverlay("hide");
      })
    }

    window.addEventListener("load", () => {

      $("#id_kode_unit").select2({
        theme: "bootstrap4"
      });

      $("#id_kode_unit").on("change", function() {
        
        $.LoadingOverlay("show");

        let kode_unit = $(this).val();

        $.post(base_url("pembayaran/get_unit_by_kode_unit"), {
          kode_unit: kode_unit
        })
        .done((r) => {
          let data = r.data;
          
          $("#id_namapemilik").val(data.nama_pemilik);
          $("#id_email").val(data.email);
          $("#id_msisdn").val(data.msisdn);
          $("#id_bulan_awal").val(data.bulan_awal);
          $("#id_tahun_awal").val(data.tahun_awal);

          $("#id_jml_tagihan_pembayaran").val(data.jml_tagihan_pembayaran);
          $("#id_jml_pembayaran").val(data.jml_tagihan_pembayaran);

          let html = `
            <option value="">
            -- PILIH --
            </option>
          `;

          if(data.is_tunggakan) {

            $("#f-periode-spp").html(`
              <input id="id_periode_spp" name="periode_spp" value="" class="form-control" readonly />
            `);
            $("#id_periode_spp").val(data.jml_tagihan);

          } else {

            $("#f-periode-spp").html(`
              <select id="id_periode_spp" name="periode_spp" onchange="onChangePeriodeSpp(this)" class="form-control">
                <option value="">-- PILIH --</option>
              </select>
            `);

            data.master_spp.forEach(v => {
              html += `
                <option value="${v}">${v}</option>
              `;
            });

            $("#id_periode_spp").html(html);

          }
          $.LoadingOverlay("hide");
        })
        .fail(e => {
          $.LoadingOverlay("hide");
        });

      });

      $("#form_validation").validate({
        rules: {
          kode_unit: {
            required: true
          },
          metode_pembayaran: {
            required: true
          },
          file_bukti: {
            required: true
          }
        },
        errorPlacement: function(error, element) {
          $(element).addClass("is-invalid");
        },
        success: function(label, element) {
          $(element).removeClass("is-invalid");
        }
      });
      
    });
  </script>
  