

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Tabel User
            </h1>
          </div>
          <div class="col-sm-6">
            
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Daftar User
          </h3>
          <div class="card-tools">
            <a href="<?php echo(base_url("user/create")); ?>" data-toggle="tooltip" data-placement="top" title="Tambah Data" class="btn btn-primary" >
              <i class="fas fa-plus" tool></i></a>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="id-datatable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th class="text-center" width="20">No</th>
                  <th class="text-center">Kode Unit</th>
                  <th class="text-center">Username</th>
                  <th class="text-center">Grup</th>
                  <th class="text-center">Status</th>
                  <th class="text-center">Aksi</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  
  <script type="text/javascript">

    var table;

    const onActive = (id) => {
      Swal.fire({
        title: 'Apakah anda yakin ?',
        text: "User ini akan di aktifkan",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya!'
      }).then((result) => {
        if (result.value) {

          $.post(base_url("user/ajax_active"), {
            id:id
          })
          .done(r => {

            table.ajax.reload(null, false);

            Swal.fire(
              'Pesan Sukses',
              'Berhasil diaktifkan.',
              'success'
            );
            

          })
          .fail(e => {
            alert(e.toString());
          })
          
        }
      });
    }
    
    const onDeactive = (id) => {

      Swal.fire({
        title: 'Apakah anda yakin ?',
        text: "User ini akan tidak di aktifkan",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya!'
      }).then((result) => {
        if (result.value) {

          $.post(base_url("user/ajax_deactive"), {
            id:id
          })
          .done(r => {

            table.ajax.reload(null, false);

            Swal.fire(
              'Pesan Sukses',
              'Berhasil tidak diaktifkan.',
              'success'
            );
            

          })
          .fail(e => {
            alert(e.toString());
          })
          
        }
      });

    }

    const onDestroy = (id) => {

      Swal.fire({
        title: 'Apakah anda yakin ?',
        text: "Anda tidak akan dapat mengembalikan ini!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus!'
      }).then((result) => {
        if (result.value) {

          $.post(base_url("user/ajax_destroy"), {
            id:id
          })
          .done(r => {

            table.ajax.reload(null, false);

            Swal.fire(
              'Pesan Sukses',
              'File Anda telah dihapus.',
              'success'
            );
            

          })
          .fail(e => {
            alert(e.toString());
          })
          
        }
      })
    }

    window.addEventListener("load", () => {

      table = $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("user/get_datatables"),
            'type': 'GET'
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs: [
          {
              targets: [0],
              className: 'text-right'
          },
          {
            targets: -1,
            className: 'text-center'
          }
        ]
      });

    });
  </script>