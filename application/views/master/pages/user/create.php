

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1> Add User</h1>
          </div>
          <div class="col-sm-6">
            
          </div>
        </div>
      </div> --><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Form Add User
          </h3>
        </div>
        <div class="card-body">
          <?php echo(form_open("", [
            "id" => "form_validation"
          ])); ?>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="group_id">
                    Group
                  </label>
                  <select id="group_id" name="group_id" class="form-control" style="width: 100%">
                    <option value="">
                      -- PILIH --
                    </option>
                  <?php foreach($v_data->group as $g) : ?>
                    <?php if($g->group_id == $v_data->user->group_id) : ?>
                      <option value="<?php echo($g->group_id); ?>" selected>
                        <?php echo($g->group_name); ?>
                      </option>
                    <?php else : ?>
                      <option value="<?php echo($g->group_id); ?>">
                        <?php echo($g->group_name); ?>
                      </option>
                    <?php endif; ?>
                  <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="kode_unit">
                    Kode Unit
                  </label>
                  <select id="kode_unit" name="kode_unit" class="form-control" style="width: 100%">

                    <option value="">
                      -- PILIH --
                    </option>
                    <?php foreach($v_data->unit as $u) : ?>
                      <?php if($u->sts_disabled == "Y") : ?>
                        <option value="<?php echo($u->kode_unit); ?>" disabled="disabled">
                          <?php echo($u->kode_unit); ?>
                        </option>
                      <?php else : ?>
                        <option value="<?php echo($u->kode_unit); ?>">
                          <?php echo($u->kode_unit); ?>
                        </option>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                    <label for="username">
                      Username
                    </label>
                    <input
                      type="text"
                      class="form-control"
                      id="username"
                      name="username"
                      value=""
                      placeholder="Ketik disini . . .">
                  </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                    <label for="password">
                      Password
                    </label>
                    <input
                      type="text"
                      class="form-control"
                      id="password"
                      name="password"
                      placeholder="Ketik disini . . .">
                  </div>
              </div>
              <div class="col-md-12 mb-2"></div>
              <div class="col-md-12">
                <button type="submit" class="btn btn-primary">
                  <i class="fas fa-save"> </i>
                  Simpan
                </button>
                <a href="<?php echo(base_url("user")); ?>" class="btn btn-danger">
                  <i class="fas fa-arrow-left"> </i>
                  Kembali
                </a>
              </div>
            </div>
          <?php echo(form_close()); ?>
        </div>
        <!-- /.card-body -->
       <!--  <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">

    window.addEventListener("load", () => {

      $("#kode_unit").select2({
        theme: 'bootstrap4'
      });

      $("#group_id").select2({
        theme: 'bootstrap4'
      });

      $("#group_id").on("change", function(){
        let v = $(this).val();
        // WARGA
        if(v === '4') {
          $("#kode_unit").removeAttr("disabled");
          $("#username").attr("readonly", true);
        } else {
          $("#kode_unit").attr("disabled", "disabled");
          $("#kode_unit").val("").trigger("change");
          $("#username").removeAttr("readonly");
        }
        
      });

      $("#kode_unit").on("change", function() {
        
        let group_id = $("#group_id").val();
        let i = $(this).val();
        $("#username").val(i);

      });

      $("#group_id").trigger("change");

      $("#form_validation").validate({
        rules: {
          group_id: {
            required: true
          },
          kode_unit: {
            required: true
          },
          username: {
            required: true
          },
          password: {
            required: true
          }
        },
        errorPlacement: function(error, element) {
          $(element).addClass("is-invalid");
        },
        success: function(label, element) {
          $(element).removeClass("is-invalid");
        }
      });

    });
  </script>
  