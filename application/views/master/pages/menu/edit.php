

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detil Edit Menu</h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Form Edit Menu
          </h3>
        </div>
        <div class="card-body">
          
          <?php echo(form_open("", [
            "id" => "form_validation"
          ])); ?>

            <input type="hidden" name="id" value="<?php echo($v_data->menu->ref_id_menu); ?>">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="namamenu">
                    Nama Menu
                  </label>
                  <input type="text" name="namamenu" class="form-control" id="id-txt-namamenu" value="<?php echo($v_data->menu->ref_name_menu); ?>" placeholder="Ketik disini . . .">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="url">Url Menu</label>
                  <input type="text" name="url" class="form-control" id="id-txt-url" value="<?php echo($v_data->menu->ref_url_menu); ?>" placeholder="Ketik disini . . .">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="ikon">Ikon Menu</label>
                  <input type="text" name="ikon" class="form-control" id="id-txt-ikon" value="<?php echo($v_data->menu->ref_icon_menu); ?>" placeholder="Ketik disini . . .">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="deskripsi">Deskripsi Menu</label>
                  <textarea class="form-control" name="deskripsi" id="id-text-deskripsi"><?php echo($v_data->menu->ref_desc_menu); ?></textarea>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="indukmenu">Induk Menu</label>
                  <select class="form-control" name="indukmenu" id="id-txt-indukmenu">
                    <option value="">Tidak ada</option>
                  <?php foreach($v_data->menus as $menu) : ?>

                    <?php if($menu->ref_id_menu == $v_data->menu->ref_parent_id_menu) : ?>
                      <option value="<?php echo($menu->ref_id_menu); ?>" selected>
                        <?php echo($menu->ref_name_menu); ?>
                      </option>
                    <?php else : ?>
                      <option value="<?php echo($menu->ref_id_menu); ?>">
                      <?php echo($menu->ref_name_menu); ?>
                    </option>
                    <?php endif; ?>
                  <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="status">Status Menu</label>
                  <select class="form-control" name="status" id="id-txt-status">
                    <?php if($v_data->menu->sts_active == 1) : ?>
                      <option value="1" selected>Aktif</option>
                      <option value="0">Tidak Aktif</option>
                    <?php else : ?>
                      <option value="1">Aktif</option>
                      <option value="0" selected>Tidak Aktif</option>
                    <?php endif; ?>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="posisimenu">No Urut Menu</label>
                  <input type="text" name="posisimenu" class="form-control" id="id-txt-posisimenu" value="<?php echo($v_data->menu->ref_position_menu); ?>" placeholder="Ketik disini . . .">
                </div>
              </div>
              <div class="col-md-12 mb-2"></div>
              <div class="col-md-6">
                <button type="submit" class="btn btn-primary">
                  <i class="fas fa-save"> </i>
                  Simpan
                </button>
                <a href="<?php echo(base_url("menu")); ?>" class="btn btn-danger">
                  <i class="fas fa-arrow-left"> </i>
                  Kembali
                </a>
              </div>
            </div>
          <?php echo(form_close()); ?>
        </div>
        <!-- /.card-body -->
       <!--  <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    window.addEventListener("load", () => {
      $("#form_validation").validate({
        rules: {
          namamenu: {
            required: true
          },
          url: {
            required: true
          },
          ikon: {
            required: true
          }
        },
        errorPlacement: function(error, element) {
          $(element).addClass("is-invalid");
        },
        success: function(label, element) {
          $(element).removeClass("is-invalid");
        }
      });
    });
  </script>
  