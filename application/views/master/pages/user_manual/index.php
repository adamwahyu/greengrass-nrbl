

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Tabel User
            </h1>
          </div>
          <div class="col-sm-6">

          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <?php
              $perumahan = $this->db->query("select * from ref_perumahan where sts_active=1
              ")->row_array();
              $result = $perumahan['nama_perumahan'];

              ?>

      <!-- Default box -->
      <div class="card">
        <div class="card-header">

          <h3 class="card-title">
            User Manual <?=$result?>
          </h3>
        </div>
        <div class="box-body">
          <div class="col-xs-12 form-group">&nbsp;</div>
                               <div class="col-xs-12 form-group text-center">
                                  <?php if($this->session->userdata("user")->group_id == 1 ){ ?>
                                   <a href="<?php echo base_url('/asset/user_manual/ADMIN - User Manual Aplikasi Perumahan Green Grass.pdf')?>" target="_blank" class="btn btn-app pointer download">
                                       <i class="fa fa-address-book"></i> User Manual Admin
                                   </a>
                                   <?php }?>

                                      <?php if($this->session->userdata("user")->group_id == 3 || $this->session->userdata("user")->group_id == 1){ ?>
                                   <a href="<?php echo base_url('/asset/user_manual/PENGELOLA - User Manual Aplikasi Perumahan Green Grass.pdf')?>" target="_blank" class="btn btn-app pointer download">
                                        <i class="fa fa-address-book"></i> User Manual Pengelola
                                    </a>
                                    <?php }?>
                                    <?php if($this->session->userdata("user")->group_id == 2 || $this->session->userdata("user")->group_id == 1){ ?>

                                    <a href="<?php echo base_url('/asset/user_manual/PENGURUS - User Manual Aplikasi Perumahan Green Grass.pdf')?>" target="_blank" class="btn btn-app pointer download">
                                         <i class="fa fa-address-book"></i> User Manual Pengurus
                                     </a>
                                   <?php }?>
                                   <?php if($this->session->userdata("user")->group_id == 4 || $this->session->userdata("user")->group_id == 1){ ?>

                                     <a href="<?php echo base_url('/asset/user_manual/WARGA - User Manual Aplikasi Perumahan Green Grass.pdf')?>" target="_blank" class="btn btn-app pointer download">
                                          <i class="fa fa-address-book"></i> User Manual Warga
                                      </a>
                                    <?php }?>

                                 </div>
                               </div>
                             </div>

        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
