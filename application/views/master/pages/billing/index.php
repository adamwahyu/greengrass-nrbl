

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Tabel Billing
            </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Daftar Billing
          </h3>

          <!-- <div class="card-tools">

            <a href="<?php echo(base_url("billing/create")); ?>" class="btn btn-primary" >
              <i class="fas fa-plus"></i></a>
          </div> -->
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="id-datatable" class="table table-bordered table-striped" style="width:100%">
              <thead>
                <tr>
                  <th class="text-center" style="width: 1%;">No</th>
                  <th class="text-center" style="width: 10%;">Kode Billing</th>
                  <th class="text-center" style="width: 3px;">Kode Unit</th>
                  <th class="text-center" style="width: 3px;">Nama Pemilik</th>
                  <th class="text-center" style="width: 3px;">Tagihan</th>
                  <th class="text-center" style="width: 3px;">Tgl Billing</th>
                  <th class="text-center" style="width: 3px;">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <!-- Footer -->
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Detil Billing</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">

            <div class="col-md-6">
              <input type="hidden" name="id_unit" id="id_unit">
              <div class="form-group">
                <label for="id_kodebilling">
                  Kode Billing
                </label>
                <input type="text" class="form-control" id="id_kodebilling" placeholder="-" readonly>
              </div>
              <div class="form-group">
                <label for="id_kodeunit">
                  Kode Unit
                </label>
                <input type="text" class="form-control" id="id_kodeunit" placeholder="-" readonly>
              </div>
              <div class="form-group">
                <label for="id_namapemilik">
                  Nama Pemilik
                </label>
                <input type="text" class="form-control" id="id_namapemilik" placeholder="-" readonly>
              </div>
              <div class="form-group">
                <label for="id_email">
                  Email
                </label>
                <input type="text" class="form-control" id="id_email" placeholder="-" readonly>
              </div>
              <div class="form-group">
                <label for="id_tglbilling">
                  Tgl Billing
                </label>
                <input type="text" class="form-control" readonly="" id="id_tglbilling">
              </div>
              <div class="form-group">
                <label for="id_status">
                  Status
                </label>
                <input type="text" class="form-control" readonly="" id="id_status">
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="id_msisdn">
                  MSISDN
                </label>
                <input type="text" class="form-control" id="id_msisdn" placeholder="-" readonly>
              </div>

              <div class="form-group">
                <label for="id_luasunit">
                  Nominal IPL
                </label>
                <input type="text" class="form-control" id="id_luasunit" placeholder="-" readonly="">
              </div>

              <div class="form-group">
                <label for="id_tarifparameter">
                  Tarif Permeter
                </label>
                <input type="text" class="form-control" id="id_tarifparameter" placeholder="-" readonly="">
              </div>
              <div class="form-group">
                <label for="id_nilaibilling">
                  Nilai Billing
                </label>
                <input type="text" class="form-control" readonly="" id="id_nilaibilling">
              </div>
              <div class="form-group">
                <label for="id_nomervanarobil">
                  Nomer VA Narobil
                </label>
                <input type="text" class="form-control" readonly="" id="id_nomervanarobil">
              </div>
              <div class="form-group">
                <label for="id_nomervabca">
                  Nomer VA BCA
                </label>
                <input type="text" class="form-control" readonly="" id="id_nomervabca">
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group">
                <label for="id_notes">
                  Notes
                </label>
                <textarea class="form-control" id="id_notes" name="notes" readonly=""></textarea>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="id_created_date">
                  Created Date
                </label>
                <input type="text" class="form-control" readonly="" id="id_created_date">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="id_created_date">
                  Updated Date
                </label>
                <input type="text" class="form-control" readonly="" id="id_updated_date">
              </div>
            </div>
            <!-- <div class="col-md-6 mx-auto">
              <div class="form-group">
                <label for="id_bulan">
                  Bulan
                </label>
                <input type="text" class="form-control" readonly="" id="id_bulan">
              </div>
            </div> -->

          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">

    const onView = (id) => {

      $.LoadingOverlay("show");

      $.post(base_url("billing/ajax_get_billing_by_id"), {
        id: id
      })
      .done(r => {

        let data = r.data;
        $("#id_kodebilling").val(data.kode_billing);
        $("#id_kodeunit").val(data.kode_unit);
        $("#id_namapemilik").val(data.nama_pemilik);
        $("#id_email").val(data.email);
        $("#id_tglbilling").val(data.tgl_billing);

        if(data.sts_active == 1) {
          $("#id_status").val("Aktif");
        } else {
          $("#id_status").val("Tidak Aktif");
        }

        $("#id_msisdn").val(data.msisdn);
        $("#id_luasunit").val(data.luas_unit);
        $("#id_tarifparameter").val(rupiah(data.tarif_permeter));
        $("#id_nilaibilling").val(rupiah(data.nilai_billing));
        $("#id_nomervanarobil").val(data.nomor_va_narobil);
        $("#id_nomervabca").val(data.nomor_va_bca);
        $("#id_notes").val(data.notes);
        $("#id_created_date").val(data.created_date);
        $("#id_updated_date").val(data.updated_date);

        $("#modal-xl").modal({
          show: true
        });

        $('.modal-dialog').draggable({
          handle: ".modal-header"
        });
        $('.modal-content').resizable();
        $.LoadingOverlay("hide");
      })
      .fail(e => {
        alert(e.toString());
      });

    }

    window.addEventListener("load", () => {

      table = $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        "autoWidth": false,
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("billing/get_datatables"),
            'type': 'GET'
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs: [
          {
              targets: [0, 1, 4],
              className: 'text-right'
          },
          {
            targets: [-1],
            className: "text-center"
          },
          {
            targets: [0],
            width: "5%"
          }
        ]
      });

    });
  </script>
