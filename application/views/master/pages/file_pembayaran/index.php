

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Tabel Menu
            </h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Daftar Upload Pembayaran
          </h3>
          <div class="card-tools">
            <a href="<?php echo(base_url("file_pembayaran/upload")); ?>" data-toggle="tooltip" data-placement="top" title="Tambah Data" class="btn btn-primary" >
              <i class="fas fa-plus"></i></a>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <p>
                <span class="text-red">*</span>
                Untuk melakukan form upload pembayaran di haruskan memakai template yang sudah disediakan, Link : <a href="<?php echo(base_url("asset/download/template_upload.xlsx")); ?>">
                  <u><i>Template Upload</i></u>
                </a>
              </p>
              <span></span>
            </div>
          </div>
          <div class="table-responsive">
            <table id="id-datatable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th class="text-center" style="width: 1px;">No</th>
                  <th class="text-center" style="width: 3px;">Kode</th>
                  <th class="text-center" style="width: 3px;">Tanggal</th>
                  <th class="text-center" style="width: 3px;">File</th>
                  <th class="text-center" style="width: 3px;">Status</th>
                  <th class="text-center" style="width: 3px;">Metode Pembayaran</th>
                  <th class="text-center" style="width: 3px;">Jml Berhasil</th>
                  <th class="text-center" style="width: 3px;">Jml Gagal</th>
                  <th class="text-center" style="width: 3px;">
                    Aksi
                  </th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Detil Data <span id="id_batch_code"></span></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <table id="id-datatable-batch-detil" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th class="text-center">No</th>
                      <th class="text-center">Kode Unit</th>
                      <th class="text-center">Kode</th>
                      <th class="text-center">Tanggal</th>
                      <th class="text-center">Jumlah</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Catatan</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    var table, table_batch_detil;

    const onTts = async (batch_code) => {
      $.LoadingOverlay("show");

      let url = base_url("file_pembayaran/ajax_get_kode_unit_batch_detil_by_batch_code");
      
      const getKodeUnit = (batch_code) => {
        return new Promise((resolve, reject) => {
          $.post(url, {
            batch_code: batch_code
          })
          .done(r => {
            resolve(r.data);
          })
        });
      }

      let kode_units = await getKodeUnit(batch_code);
      let opt = {};
      kode_units.forEach(v => {
        opt[v] = v;
      });
      
      $.LoadingOverlay("hide");

      const { value: v_select } = await Swal.fire({
        title: 'UNIT',
        input: 'select',
        inputOptions: opt,
        inputPlaceholder: 'Pilih Unit',
        showCancelButton: true,
        inputValidator: (value) => {
          return new Promise((resolve) => {
            if (value != '') {
              resolve()
            } else {
              resolve('You need to select :)')
            }
          })
        }
      });

      if(v_select) {
        window.location.href = base_url("file_pembayaran/create_ttp_pdf/" + batch_code + "/" + v_select);
      }

    }
    const onView = (batch_code) => {
      $("#id_batch_code").html(batch_code);
      $.LoadingOverlay("show");

      if(typeof table_batch_detil !== "undefined") {
        table_batch_detil.destroy();
      }

      table_batch_detil = $("#id-datatable-batch-detil").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        "autoWidth": false,
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("file_pembayaran/get_datatables_detil"),
            'type': 'GET',
            'data': {
              batch_code: batch_code
            }
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        initComplete: () => {
          $.LoadingOverlay("hide");
          $("#modal-xl").modal({
            show: true
          });
          $('.modal-dialog').draggable({
            handle: ".modal-header"
          });
          $('.modal-content').resizable();
          
        },
        columnDefs: [
          {
              targets: [0, 2],
              className: 'text-right'
          },
          // {
          //   targets: [-1],
          //   className: "text-right"
          // },
          // {
          //   targets: [0],
          //   width: "5%"
          // },
          // {
          //   targets: [1, 2, 3],
          //   width: "10%"
          // },
        ]
      });
    }

    window.addEventListener("load", () => {

      table = $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        "autoWidth": false,
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("file_pembayaran/get_datatables_header"),
            'type': 'GET'
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs: [
          {
              targets: [0],
              className: 'text-right'
          },
          {
            targets: [-1, 3],
            className: "text-center"
          },
          {
            targets: [6, 7],
            className: "text-right"
          },
          {
            targets: [0],
            width: "3%"
          },
          {
            targets: [1],
            width: "20%"
          },
          {
            targets: [3, -1],
            width: "10%"
          },
          {
            targets: [4],
            width: "5%"
          },
          {
            targets: [2],
            width: "7%"
          },
          {
            targets: [6, 7],
            width: "7%"
          },
        ]
      });

    });
  </script>