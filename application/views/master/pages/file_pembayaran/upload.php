

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detil Form Tambah Menu</h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Form Upload Pembayaran
          </h3>
        </div>

        <div class="card-body">
          <label class="badge-warning" style="font-size: 20px;">File yang dapat diupload berextensi: .xls, .xlsx</label>
          <?php echo(form_open_multipart("", [
            "id" => "form_validation"
          ])); ?>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="file_excel">
                    Masukkan File Excel
                  </label>
                  <input type="file" accept=".xls,.xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel" name="file_pembayaran" class="form-control" id="id_file_pembayaran" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="id_metode_pembayaran">
                    Metode Pembayaran
                  </label>
                  <select id="id_metode_pembayaran" name="metode_pembayaran" class="form-control">
                    <option value="">
                      -- PILIH --
                    </option>
                    <?php foreach($v_data->metode_pembayaran as $metode) : ?>
                      <option value="<?php echo($metode->id_metode_pembayaran); ?>">
                        <?php echo($metode->desc_metode_pembayaran); ?>
                      </option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="col-md-12 mb-2"></div>
              <div class="col-md-6">
                <button type="submit" class="btn btn-primary">
                  <i class="fas fa-save"> </i>
                  Simpan
                </button>
                <a href="<?php echo(base_url("file_pembayaran")); ?>" class="btn btn-danger">
                  <i class="fas fa-arrow-left"> </i>
                  Kembali
                </a>
              </div>
            </div>
          <?php echo(form_close()); ?>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    window.addEventListener("load", () => {

      $("#form_validation").validate({
        rules: {
          file_pembayaran: {
            required: true
          },
          metode_pembayaran: {
            required: true
          }
        },
        errorPlacement: function(error, element) {
          $(element).addClass("is-invalid");
        },
        success: function(label, element) {
          $(element).removeClass("is-invalid");
        }
      });
      
    });
  </script>
  