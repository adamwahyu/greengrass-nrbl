

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Manage Group</h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Manage Group
          </h3>
        </div>
        <div class="card-body">
          <?php echo(form_open()); ?>
            <div class="row">
              <div class="col-md-12">
                <table id="id-datatable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th class="text-center">Nama Menu</th>
                      <th class="text-center">Centang Menu</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($v_data->menus as $menu) : ?>
                      <input type="hidden" name="id[]" value="<?php echo($menu->ref_id_gaccess); ?>">
                      <tr>
                        <td><?php echo($menu->ref_name_menu); ?></td>
                        <td class="text-center">
                          <?php if($menu->status) : ?>
                            <div class="form-check">
                              <input name="status_<?php echo($menu->ref_id_gaccess); ?>" class="form-check-input" type="checkbox" checked/>
                            </div>
                          <?php else : ?>
                            <div class="form-check">
                              <input name="status_<?php echo($menu->ref_id_gaccess); ?>" class="form-check-input" type="checkbox" />
                            </div>
                          <?php endif; ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <div class="col-md-12 mb-2"></div>
              <div class="col-md-6">
                <button type="submit" class="btn btn-primary">
                  <i class="fas fa-plus"> </i>
                  Simpan
                </button>
                <a href="<?php echo(base_url("group")); ?>" class="btn btn-danger">
                  <i class="fas fa-arrow-left"> </i>
                  Kembali
                </a>
              </div>
            </div>
          <?php echo(form_close()); ?>
        </div>
        <!-- /.card-body -->
        <!-- div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  