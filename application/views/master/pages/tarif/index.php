

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Daftar Tarif
          </h3>

          <div class="card-tools">

            <a href="<?php echo(base_url("tarif/create")); ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Tambah Data" >
              <i class="fas fa-plus"></i></a>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="id-datatable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th class="text-left" style="width: 3px;">No</th>
                  <th class="text-left" style="width: 3px;">Tarif</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <!-- Footer -->
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    var table;

    const onDestroy = (id) => {

      Swal.fire({
        title: 'Apakah anda yakin ?',
        text: "Anda tidak akan dapat mengembalikan ini!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus!'
      }).then((result) => {
        if (result.value) {

          $.post(base_url("tarif/ajax_destroy"), {
            id:id
          })
          .done(r => {

            Swal.fire(
              'Pesan Sukses',
              'File Anda telah dihapus.',
              'success'
            );
            table.ajax.reload(null, false);

          })
          .fail(e => {
            alert(e.toString());
          })

        }
      })

    }

    window.addEventListener("load", () => {

      table = $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        "autoWidth": false,
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("tarif/get_datatables"),
            'type': 'GET'
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs: [
          {
              targets: [0],
              className: 'text-right'
          },
          {
            targets: [-1],
            className: "text-center"
          },
          {
            targets: [0],
            width: "5%"
          }
        ]
      });

    });
  </script>
