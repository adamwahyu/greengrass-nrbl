

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <!-- <div class="card-header">
          <h3 class="card-title">
            Dashboard
          </h3>
        </div> -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <h3 class="text-center mt-0">
                <u>
                  Monitoring Pembayaran IPL <u><?php echo(date_indo("Y-m-d")); ?></u>
                </u>
              </h3>
            </div>
            <div class="col-md-12">
              <?php $this->load->view("master/pages/dashboard/widgets/indikator-warna-perumahan"); ?>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>