

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Hallo</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <h4>Kamu login sebagai unit <label class="badge badge-success"><?php echo $v_data->summary_unit->kode_unit?></h4></label>

          <div class="row">

            <div class="col-md-6">
              <input type="hidden" name="id_unit" id="id_unit">
              <div class="form-group">
                <label for="id_kodeunit">
                  Kode Unit
                </label>
                <input type="text" class="form-control" id="id_kodeunit" placeholder="-" value="<?php echo $v_data->summary_unit->kode_unit?>" readonly>
              </div>
              <div class="form-group">
                <label for="id_luasunit">
                  Nominal IPL
                </label>
                <input type="text" class="form-control" id="id_luasunit" placeholder="-" value="<?php echo $v_data->summary_unit->luas_unit?>" readonly>
              </div>
              <div class="form-group">
                <label for="id_nomerva_bca">
                  VA BCA
                </label>
                <input type="text" class="form-control" id="id_nomerva_bca" placeholder="-" value="<?php echo $v_data->summary_unit->nomor_va_bca?>" readonly>
              </div>
              <div class="form-group">
                <label for="id_nomerva_narobil">
                  VA Narobil Rutin
                </label>
                <input type="text" class="form-control" id="id_nomerva_narobil" placeholder="-" value="<?php echo $v_data->summary_unit->nomor_va_narobil?>" readonly>
              </div>
              <div class="form-group">
                <label for="id_tarif">
                  Tarif IPL bulan ini
                </label>
                <input type="text" class="form-control" id="id_tarif" placeholder="-" value="<?php echo rupiah($v_data->summary_unit->tarif)?>" readonly>
              </div>
              <div class="form-group">
                <label for="id_tarif">
                  VA Narobil Tunggakkan
                </label>
                <input type="text" class="form-control" id="id_nomerva_narobil" placeholder="-" value="<?php echo $v_data->vanonrutin->va_sementara ?? 'Tidak Ada'; ?>" readonly>

                  </div>
                </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="id_namapemilik">
                  Nama Pemilik
                </label>
                <input type="text" class="form-control" id="id_namapemilik" placeholder="-" value="<?php echo $v_data->summary_unit->nama_pemilik?>" readonly>
              </div>

              <div class="form-group">
                <label for="id_msisdn">
                  MSISDN
                </label>
                <input type="text" class="form-control" id="id_msisdn" placeholder="-" value="<?php echo $v_data->summary_unit->msisdn?>" readonly="">
              </div>
              <div class="form-group">
                <label for="id_email">
                  Email
                </label>
                <input type="text" class="form-control" id="id_email" placeholder="-" value="<?php echo $v_data->summary_unit->email?>" readonly="">
              </div>
              <div class="form-group">
                <label for="id_nilai_tunggakan">
                  Jumlah Tagihan Rutin
                </label>
                <input type="text" class="form-control" id="id_nilai_tunggakan"  value="<?php echo rupiah($v_data->summary_unit->nilai_tunggakan)?>" placeholder="-" readonly>
              </div>
              <div class="form-group">
                <label for="status-huni">
                  Status Hunian
                </label>
                <input type="text" class="form-control"  value="<?php echo $v_data->summary_unit->sts_huni?>" readonly="" id="id_sts_huni">
              </div>

              <div class="form-group">
                <label for="id_nilai_tunggakan">
                  Jumlah Tunggakan
                </label>
                <input type="text" class="form-control" id="id_nilai_tunggakan"  value="<?php echo rupiah($v_data->jumlahtunggakan->count); ?>" placeholder="-" readonly>
              </div>
            </div>

            <div class="col-md-12 col-xs-12">
              <div class="form-group">
                <label for="id_notes">
                  Notes
                </label>
                <textarea class="form-control" id="id_notes"  value="<?php echo $v_data->summary_unit->notes?>" name="notes" readonly=""></textarea>
              </div>
            </div>
          </div>
        <!-- </div> -->
        <div class="row">
            <div class="col-md-12">
              <hr />
            </div>
        </div>

          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="modal-title" style="text-align: center;">
                    Daftar Tagihan
                  </h4>
                </div>
                <div class="col-md-12 mb-2"></div>
              </div>
            </div>
          </div>

        <div class="row">
          <div class="col-md-12">
              <div class="table-responsive">
                <table id="id-datatable-trans-kewajiban" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th class="text-center">No</th>
                      <th class="text-center">Tahun</th>
                      <th class="text-center">Bulan</th>
                      <th class="text-center">Nominal IPL</th>
                      <th class="text-center">Tarif Permeter</th>
                      <th class="text-center">Kewajiban</th>
                      <th class="text-center">Pemenuhan</th>
                      <th class="text-center">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
              <hr />
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="modal-title" style="text-align: center;">
                    Daftar Tagihan Yang Harus Dibayar
                  </h4>
                </div>
                <div class="col-md-12 mb-2"></div>
              </div>
            </div>
        </div>

          <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                  <table id="id-datatable-trans-kewajiban" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th class="text-center">
                          No
                        </th>
                        <th class="text-center">
                          Kode Unit
                        </th>
                        <th class="text-center">
                          Total Tagihan
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach($v_data->data_tagihanyangharusdibayar as $i => $thd) : ?>
                      <tr>
                        <td class="text-center">
                            <?php echo($i+1); ?>
                        </td>
                        <td>
                          <?php echo($thd->kode_unit); ?>
                        </td>
                        <td class="text-right">
                          <?php echo(rupiah($thd->amount)); ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
          </div>

        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
    </section>
  </div>

<script type="text/javascript">
  window.addEventListener("load", () => {

      table = $("#id-datatable-trans-kewajiban").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("dashboard/get_datatables_trans_kewajiban"),
            'type': 'GET'
        },
        drawCallback: () => {
            $('[data-toggle="tooltip"]').tooltip()
          },
          columnDefs: [
            {
                targets: [0, 1, 2, 3, 4,5,6, 7],
                className: 'text-right'
            },
            {
                targets: [-1],
                className: 'text-left'
            }
          ]
      });

    });
  </script>
