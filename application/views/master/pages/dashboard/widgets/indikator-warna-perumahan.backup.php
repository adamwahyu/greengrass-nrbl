<div class="row">
<?php foreach($v_data->indikator_warna_perumahan as $indikator) : 
  ?>
  <div class="col-md-6">
    <div class="card" style="margin-bottom: 0.5rem !important;">
      <div class="card-body">
        <!-- <span>a</span> -->
        <div class="row">
          <div class="col-md-12">
            <h5 class="text-center mb-0 mt-0">
              <?php echo title_case($indikator->name); ?>
            </h5>
          </div>
          <div class="col-md-4">
              <div class="color-palette-set">
                <div class="bg-danger color-palette" style="opacity: 90%">
                  Total : <u style="cursor: pointer;" onclick="onViewUnitByIndikator('<?php echo($indikator->keyword); ?>', 'merah', '<?php echo title_case($indikator->name); ?>')"><?php echo($indikator->merah); ?></u> 
                  <!-- <span>
                    Ada tagihan pembayaran lebih dari 4
                  </span> -->
                </div>
                <div class="bg-danger color-palette">
                  <span style="text-align:center; font-size: 15px;  color: white;">
                    
                    Rp. <u style="cursor: pointer;" onclick="onViewUnitByIndikator('<?php echo($indikator->keyword); ?>', 'merah', '<?php echo title_case($indikator->name); ?>')"><?php echo(number_format($indikator->merah_total)); ?></u>
                  </span>
                </div>
              </div>
          </div>
          <div class="col-md-4">
              <div class="color-palette-set">
                <div class="bg-warning color-palette" style="opacity: 90%">
                  Total : <u style="cursor: pointer;" onclick="onViewUnitByIndikator('<?php echo($indikator->keyword); ?>', 'kuning', '<?php echo title_case($indikator->name); ?>')"><?php echo($indikator->kuning); ?></u>
                </div>
                <div class="bg-warning color-palette">
                  <span style="text-align:center; font-size: 15px;  color: black;">
                    Rp. <u style="cursor: pointer;" onclick="onViewUnitByIndikator('<?php echo($indikator->keyword); ?>', 'kuning', '<?php echo title_case($indikator->name); ?>')"><?php echo(number_format($indikator->kuning_total)); ?></u>
                  </span>
                </div>
              </div>
          </div>
          <div class="col-md-4">
              <div class="color-palette-set">
                <div class="bg-success color-palette" style="opacity: 90%">
                  Total : <u style="cursor: pointer;" onclick="onViewUnitByIndikator('<?php echo($indikator->keyword); ?>', 'hijau', '<?php echo title_case($indikator->name); ?>')"><?php echo($indikator->hijau); ?></u>
                </div>
                <div class="bg-success color-palette">
                  <span style="text-align:center; font-size: 15px;  color: white;">
                    
                  </span>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php endforeach; ?>

  <div class="col-md-12">
    <div class="card" style="margin-bottom: 0.5rem !important;">
      <div class="card-body">
        <!-- <span>a</span> -->
        <div class="row">
          <div class="col-md-12">
            <h3 class="text-center mt-0">
                <u>
                  Indikator Perumahan Residence One
                </u>
              </h3>
          </div>
          <div class="col-md-4">
              <div class="color-palette-set">
                <div class="bg-danger color-palette" style="opacity: 90%">
                  Total : <u style="cursor: pointer;" onclick="onViewUnitByIndikatorAlls('merah')"><?php echo($v_data->indikator_warna_perumahan_all->merah); ?></u> 
                  <!-- <span>
                    Ada tagihan pembayaran lebih dari 4
                  </span> -->
                </div>
                <div class="bg-danger color-palette">
                  <span style="text-align:center; font-size: 20px;  color: white;">
                    
                    Rp. <u style="cursor: pointer;" onclick="onViewUnitByIndikatorAlls('merah')"><?php echo(number_format($v_data->indikator_warna_perumahan_all->merah_total)); ?></u>
                  </span>
                </div>
              </div>
          </div>
          <div class="col-md-4">
              <div class="color-palette-set">
                <div class="bg-warning color-palette" style="opacity: 90%">
                  Total : <u style="cursor: pointer;" onclick="onViewUnitByIndikatorAlls('kuning')"><?php echo($v_data->indikator_warna_perumahan_all->kuning); ?></u>
                </div>
                <div class="bg-warning color-palette">
                  <span style="text-align:center; font-size: 20px;  color: black;">
                    Rp. <u style="cursor: pointer;" onclick="onViewUnitByIndikatorAlls('kuning')"><?php echo(number_format($v_data->indikator_warna_perumahan_all->kuning_total)); ?></u>
                  </span>
                </div>
              </div>
          </div>
          <div class="col-md-4">
              <div class="color-palette-set">
                <div class="bg-success color-palette" style="opacity: 90%">
                  Total : <u style="cursor: pointer;" onclick="onViewUnitByIndikatorAlls('hijau')"><?php echo($v_data->indikator_warna_perumahan_all->hijau); ?></u>
                </div>
                <div class="bg-success color-palette">
                  <span style="text-align:center; font-size: 20px;  color: white;">
                    
                  </span>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>



</div>


<div class="row" style="float: center;">
  <div class="col-lg-12 col-xl-12 col-md-12 d-none d-md-block" style="text-align: center; font-size: 20px;">
    <span class="mr-2">
      <i class="fas fa-square text-danger"></i> Tunggakkan lebih dari 2 bulan
    </span>
    <span class="mr-2">
      <i class="fas fa-square text-warning"></i> Tunggakkan 1 sampai 2 bulan
    </span>
    <span class="mr-2">
      <i class="fas fa-square text-success"></i> Tidak ada Tunggakkan
    </span>
  </div>

    <div class="col-xs-12 col-sm-12 d-block d-md-none" style="text-align: center; font-size: 20px;">
        <div class="col-xs-12 col-sm-12">
        <span class="mr-2">
          <i class="fas fa-square text-danger"></i> Tunggakkan lebih dari 2 bulan
        </span>
        </div>
        <div class="col-xs-12 col-sm-12">
        <span class="mr-2">
          <i class="fas fa-square text-warning"></i> Tunggakkan 1 sampai 2 bulan
        </span>
      </div>
        <div class="col-xs-12 col-sm-12">
        <span class="mr-2">
          <i class="fas fa-square text-success"></i> Tidak ada Tunggakkan
        </span>
      </div>
    </div>
</div>

<div class="col-md-12">
<div class="modal fade" id="modal-xl">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detil Data Cluster <span id="id_title_indikator"></span></h4>
        <div style="width: 20%; margin-left: 10px;">
          <div class="color-palette-set">
          <div id="id_color_indikator" class="bg-success color-palette"><span></span></div>
        </div>

        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table id="id-datatable-indikator-warna-perumahan" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Kode Unit</th>
                    <th class="text-center">Nama Pemilik</th>
                    <!-- <th class="text-center">Email</th>
                    <th class="text-center">Msisdn</th> -->
                    <th class="text-center">Nominal IPL</th>
                    <th class="text-center">Nomer VA Narobil</th>
                    <th class="text-center">Nomer VA BCA</th>
                    <th class="text-center">Status Hunian</th>
                    <th class="text-center">Jumlah Tagihan</th>
                    <th class="text-center">Rupiah</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
  var table;

  const onViewUnitByIndikator = (keyword, category, name) => {
    if (keyword == 'SB') {
      keyword = "SB%' or kode_unit like 'In"
    }

    $("#id_title_indikator").html(name);
    $("#id_color_indikator").removeClass("bg-danger");
    $("#id_color_indikator").removeClass("bg-warning");
    $("#id_color_indikator").removeClass("bg-success");
    if(category.toLowerCase() == "merah") {
      $("#id_color_indikator").addClass("bg-danger");
    } else if(category.toLowerCase() == "kuning") {
      $("#id_color_indikator").addClass("bg-warning");
    } else if(category.toLowerCase() == "hijau") {
      $("#id_color_indikator").addClass("bg-success");
    }

    $.LoadingOverlay("show");

    if(typeof table != "undefined") {
      table.destroy();
    }
    table = $("#id-datatable-indikator-warna-perumahan").DataTable({
      'language': {
        "emptyTable": "Data tidak ditemukan"
      },
      "pageLength": 10,
      'autoWidth': false,
      'lengthChange': false,
      'searching': true,
      'ordering': false,
      'processing': true,
      'serverSide': true,
      'ajax': {
          'url': base_url("dashboard/get_datatables_indikator_warna_perumahan"),
          'type': 'GET',
          'data': {
            keyword: keyword,
            category: category
          }
      },
      drawCallback: () => {
        // $('[data-toggle="tooltip"]').tooltip()
      },
      initComplete: () => {
        $("#modal-xl").modal({
          show: true
        });

        $('.modal-dialog').draggable({
          handle: ".modal-header"
        });
        $('.modal-content').resizable();

        $.LoadingOverlay("hide");
      },
      columnDefs: [
        {
            targets: [0, 4,5,6, 7],
            className: 'text-right'
        },
        {
            targets: [-1, 2],
            className: 'text-left'
        }
      ]
    });



  }

  const onViewUnitByIndikatorAlls = (category, name) => {

    $("#id_title_indikator").html(name);
    $("#id_color_indikator").removeClass("bg-danger");
    $("#id_color_indikator").removeClass("bg-warning");
    $("#id_color_indikator").removeClass("bg-success");
    if(category.toLowerCase() == "merah") {
      $("#id_color_indikator").addClass("bg-danger");
    } else if(category.toLowerCase() == "kuning") {
      $("#id_color_indikator").addClass("bg-warning");
    } else if(category.toLowerCase() == "hijau") {
      $("#id_color_indikator").addClass("bg-success");
    }

    $.LoadingOverlay("show");

    if(typeof table != "undefined") {
      table.destroy();
    }
    table = $("#id-datatable-indikator-warna-perumahan").DataTable({
      'language': {
        "emptyTable": "Data tidak ditemukan"
      },
      "pageLength": 10,
      'autoWidth': false,
      'lengthChange': false,
      'searching': true,
      'ordering': false,
      'processing': true,
      'serverSide': true,
      'ajax': {
          'url': base_url("dashboard/get_datatables_indikator_warna_perumahan_all"),
          'type': 'GET',
          'data': {
            category: category
          }
      },
      drawCallback: () => {
        // $('[data-toggle="tooltip"]').tooltip()
      },
      initComplete: () => {
        $("#modal-xl").modal({
          show: true
        });

        $('.modal-dialog').draggable({
          handle: ".modal-header"
        });
        $('.modal-content').resizable();

        $.LoadingOverlay("hide");
      },
      columnDefs: [
        {
            targets: [0, 4,5,6, 7],
            className: 'text-right'
        },
        {
            targets: [-1, 2],
            className: 'text-left'
        }
      ]
    });



  }
</script>