

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Tabel Unit
            </h1>
          </div>
          <div class="col-sm-6">

          </div>
        </div>
      </div> --><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Daftar Unit
          </h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="id-datatable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">Kode Unit</th>
                  <th class="text-center">Nama Pemilik</th>
                  <th class="text-center">Nominal IPL</th>
                  <th class="text-center">Nomer VA Narobil</th>
                  <th class="text-center">Status Hunian</th>
                  <th class="text-center">Tarif</th>
                  <th class="text-center">Indikator</th>
                  <th class="text-center">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Detil Data Pembayaran <span id="id_title_kode_unit"></span></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <table id="id-datatable-trans-kewajiban" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th class="text-center">No</th>
                      <th class="text-center">Tahun</th>
                      <th class="text-center">Bulan</th>
                      <th class="text-center">Luas Unit</th>
                      <th class="text-center">Tarif Permeter</th>
                      <th class="text-center">Kewajiban</th>
                      <th class="text-center">Pemenuhan</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">
                        Aksi
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    var table, table_trans_kewajiban;

    const onDestroy = (id_kewajiban) => {
      Swal.fire(id_kewajiban.toString());
      Swal.fire({
        title: 'Apakah anda yakin ?',
        text: "Anda tidak akan dapat mengembalikan ini!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus!'
      }).then((result) => {
        if (result.value) {

          $.post(base_url("kewajiban/ajax_destroy"), {
            id:id_kewajiban
          })
          .done(r => {

            Swal.fire(
              'Pesan Sukses',
              'File Anda telah dihapus.',
              'success'
            );

            table_trans_kewajiban.ajax.reload(null, false);
          })
          .fail(e => {
            alert(e.toString());
          })
          
        }
      })

    }

    const onDetail = async (id_unit, kode_unit) => {
      
      $.LoadingOverlay("show");

      $("#id_title_kode_unit").html(kode_unit);
      
      await onTagihan(kode_unit);

      $("#modal-lg").modal({
        show: true
      });
      $('.modal-dialog').draggable({
        handle: ".modal-header"
      });
      $('.modal-content').resizable();

      $.LoadingOverlay("hide");

    }

    const onTagihan = (kode_unit) => {

      return new Promise((resolve, reject) => {

        if(typeof table_trans_kewajiban != 'undefined') {
          table_trans_kewajiban.destroy();
        }
        table_trans_kewajiban = $("#id-datatable-trans-kewajiban").DataTable({
          'language': {
            "emptyTable": "Data tidak ditemukan"
          },
          // scrollX:        true,
          // scrollCollapse: true,
          'autoWidth': false,
          'lengthChange': false,
          'searching': true,
          'ordering': false,
          'processing': true,
          'serverSide': true,
          'ajax': {
              'url': base_url("kewajiban/get_datatables"),
              'type': 'GET',
              'data': {
                kode_unit: kode_unit
              }
          },
          drawCallback: () => {
            $('[data-toggle="tooltip"]').tooltip()
          },
          initComplete: () => {
            resolve()
          },
          columnDefs: [
            {
                targets: [0, 1, 2, 3, 4,5,6, 7],
                className: 'text-right'
            },
            {
                targets: [-2],
                className: 'text-left'
            },
            {
                targets: [-1],
                className: 'text-center'
            }
          ]
        });

      })

    }

    window.addEventListener("load", () => {

      table = $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("kewajiban/get_datatables_unit"),
            'type': 'GET'
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs: [
          {
              targets: [0, 3, 4,5,6],
              className: 'text-right'
          },
          {
            targets: -1,
            className: 'text-center'
          },
          {
            targets: [2],
            width: "15%"
          }
        ]
      });

    });
  </script>
