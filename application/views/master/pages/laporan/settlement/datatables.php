<div class="card" id="data_datatables">
  <div class="card-body" >
    <div class="table-responsive">
      <table id="id-datatable" class="table table-bordered table-striped" style="">
        <thead>
          <tr>
            <th class="text-center" style="">No</th>
            <th class="text-center" style="">Nomor Transaksi</th>
            <th class="text-center" style="">Tanggal</th>
            <th class="text-center" style="">Status</th>
            <th class="text-center" style="">Keterangan</th>
            <th class="text-center" style="">Nilai</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</div>


<script type="text/javascript">
  var startdate = "<?php echo $startdate ?>";
  var enddate = "<?php echo $enddate ?>";
  $(document).ready(function() {
    $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        "autoWidth": false,
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("laporan_settlement/get_datatables"),
            'type': 'GET',
            'data': {
              startdate: startdate,
              enddate: enddate
            }
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs: [
          {
              targets: [-1],
              className: 'text-right'
          },
          {
            targets: [1, 2, 3, 4],
            className: "text-center"
          },
          // {
          //   targets: [ -1]
          //   // width: "5%"
          // },
          { "width": "2%", "targets": 0 },
          { "width": "100px", "targets": -1 }
          // { "width": "80px", "targets": 2 },
          // { "width": "120px", "targets": 3 },
          // { "width": "120px", "targets": 4 },
          // { "width": "140px", "targets": 5 },
          // { "width": "80px", "targets": 6 },
          // { "width": "70px", "targets": 4 },
          // { "width": "70px", "targets": 8 }
        ]
      })
  });
</script>
