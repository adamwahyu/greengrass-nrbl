<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Laporan Pembayaran Tahunan
                </h3>
            </div>
            <div class="card-body">
              <div class="row">
                
                <div class="col-md-3">
                  
                  <div class="form-group">
                    <label>
                      Tahun
                    </label>
                    <select id="id_tahun" class="form-control select2" style="width: 100%;">
                      <option selected="selected" value="">- Pilih -</option>
                    <?php foreach($v_data->mst_tahun as $tahun) : ?>
                      <option value="<?php echo($tahun->tahun); ?>">
                        <?php echo($tahun->tahun); ?>
                      </option>
                    <?php endforeach; ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Cluster</label>
                    <select id="id_cluster" class="form-control select2" style="width: 100%;">
                      <option selected="selected" value="all">Semua</option>
                    <?php foreach($v_data->mst_cluster as $cluster) : ?>
                      <option value="<?php echo($cluster->id_cluster); ?>">
                        <?php echo($cluster->nama_cluster); ?>
                      </option>
                    <?php endforeach; ?>
                    </select>
                  </div>

                </div>
                <div class="col-md-12 mb-2"></div>
                <div class="col-md-6">
                  <div>
                      <button class="btn btn-primary" type="button" id="btn-cari">
                        <i class="fas fa-search"> </i>
                        Cari
                      </button>
                      <button class="btn btn-danger" type="button" id="btn-reset">
                        <i class="fas fa-refresh"> </i>
                        Reset
                      </button>
                      <button class="btn btn-success" type="button" id="btn-excel">
                        <i class="fas fa-file-excel"> </i>
                        Excel
                      </button>
                  </div>
                </div>
              </div>
                
            </div>
        </div>

        <div class="card" id="lay_laporan_pembayaran" style="display: none;">
            <div class="card-header">
                <h3 class="card-title">
                    Pencarian Tahun <u><span id="id_title_tahun"></span></u> Cluster <u><span id="id_title_cluster"></span></u>
                </h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table id="id-datatable" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="text-center" style="width: 1px;">No</th>
                          <th class="text-center" style="width: 3px;">Kode Unit</th>
                          <th class="text-center" style="width: 3px;">Status Hunian</th>
                        <?php foreach($v_data->mst_bulan as $bulan) : ?>
                          <th class="text-center" style="width: 3px;">
                            <?php echo($bulan->nama_bulan); ?>
                          </th>
                        <?php endforeach; ?>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>

                </div>
              </div>  
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

<script type="text/javascript">
  var table;

  window.addEventListener("load", () => {
    $(".select2").select2({
      theme: 'bootstrap4'
    });

    $("#id_tahun, #id_cluster").on("change", () => {
      $("#lay_laporan_pembayaran").hide();
    });
    $("#btn-excel").on("click", () => {

      let tahun = $("#id_tahun").val();

      let cluster = $("#id_cluster").val();
      let cluster_text = $( "#id_cluster option:selected" ).text();

      if($.isEmptyObject(tahun) || tahun == "") {
        
        Swal.fire("Form Tahun Tidak Boleh Kosong !");
        $("#lay_laporan_pembayaran").hide();
        return;

      } else {

        let form = $("<form />", {
          action: base_url("laporan_pembayaran/export_excel"),
          method: "POST"
        });
        let txtCsrf = $("<input />", {
          type: "hidden",
          name: $("meta[name='csrf-name']").attr("content"),
          value: $("meta[name='csrf-token']").attr("content")
        });
        let txtTahun = $("<input />", {
          type: "hidden",
          name: "tahun",
          value: tahun
        });
        let txtCluster = $("<input />", {
          type: "hidden",
          name: "cluster",
          value: cluster
        });
        form.append(txtCsrf);
        form.append(txtTahun);
        form.append(txtCluster);
        form.appendTo("body");
        form.submit();

      }
    });

    $("#btn-reset").on("click", () => {
      $("#id_tahun").val("").trigger("change");
      $("#id_cluster").val("all").trigger("change");
    });

    $("#btn-cari").on("click", () => {

      let tahun = $("#id_tahun").val();
      let cluster = $("#id_cluster").val();
      let cluster_text = $( "#id_cluster option:selected" ).text();
      // alert(tahun);
      if(tahun == "") {
        Swal.fire("Form Tahun Tidak Boleh Kosong !");
        $("#lay_laporan_pembayaran").hide();
        return;
      }

      $("#id_title_tahun").html(tahun);
      if(cluster != "all") {
        $("#id_title_cluster").html(cluster_text.toString().initCap());
      } else {
        $("#id_title_cluster").html(cluster.toString().initCap());
      }
      

      $.LoadingOverlay("show");
      $("#lay_laporan_pembayaran").hide();

      
      if(typeof table !== 'undefined') {
        table.destroy();
      }

      table = $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        "autoWidth": false,
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("laporan_pembayaran/get_datatables"),
            'type': 'GET',
            'data': {
              tahun: tahun,
              cluster: cluster
            }
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        initComplete: () => {
          $("#lay_laporan_pembayaran").show();
          $.LoadingOverlay("hide");   
        },
        columnDefs: [
          {
              targets: [0],
              className: 'text-right'
          },
          {
              targets: [1, 2],
              className: 'text-left'
          },
          {
            targets: [-1],
            className: "text-right"
          },
          {
            targets: [0],
            width: "5%"
          },
        <?php $i=2; ?>
        <?php foreach($v_data->mst_bulan as $bulan) : ?>
          {
            targets: [<?php echo($i); ?>],
            className: 'text-right'
          },
        <?php $i++; ?>
        <?php endforeach; ?>
        ]
      });

    });
  });
</script>