

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Laporan Pembayaran Narobil
          </h3>
        </div>
        <div class="card-body">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <div id="alert-tanggal" style="display: none">
                  <div class="alert alert-danger alert-dismissible col-md-6 col-xs-12">
                  <!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> -->
                    Silahkan Pilih Tanggal.!
                  </div>
                  </div>
                  <label>Pilih Tanggal:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control float-right" id="reservation" autocomplete="off">
                  </div>
                </div>
              </div>
              <div class="col-md-12 mb-2"></div>
              <div class="col-md-12">
                  <button class="btn btn-primary" type="button" id="get_datatables" onclick="get_datatables()"><i class="fas fa-search"> </i> Lihat</button>
                  <button class="btn btn-success" type="button" onclick="export_excel()"> <i class="fas fa-file-excel"> </i> Excel</button>
                  <button class="btn btn-warning" type="button" onclick="reset_datatables()"> <i class="fas fa-refresh"> </i> Reset</button>
              </div>
            </div>
          </div>
        </div>

      <div id="history_datatables">
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>


  <script type="text/javascript">

    var startdate = null;
    var enddate = null;
    window.addEventListener("load", () => {
    function pick_date(){
      startdate = moment().format('DD/MM/YYYY');
      enddate = moment().format('DD/MM/YYYY');
      $('#reservation').daterangepicker({
      "autoApply": true,
      "locale": {
        "format": "DD/MM/YYYY",
        "separator": " - ",
        "applyLabel": "Apply",
        "cancelLabel": "Batal",
        "fromLabel": "Dari",
        "toLabel": "Sampai",
    },
    "startDate": moment().format('DD/MM/YYYY'),
    "endDate": moment().format('DD/MM/YYYY')
    }, function(start, end, label) {
      startdate = start.format('MM/DD/YYYY');
      enddate = end.format('MM/DD/YYYY');
    });
    };
    pick_date();

    });   

    function reset_datatables(){
      $('#history_datatables').html('');
      $("#reservation").val('');
      startdate = null;
      enddate = null;
    };

    function get_datatables(){
      let v = $("#reservation").val();
      let [sd, ed] = v.split("-");
      if(isNaN(sd,ed)){
        startdate = moment(sd,'DD/MM/YYYY').format('MM/DD/YYYY');
        enddate = moment(ed,'DD/MM/YYYY').format('MM/DD/YYYY');  
      };
      
      if ((startdate || enddate) == null){
        document.getElementById('alert-tanggal').style.display = 'block';
      }else{
        if (document.getElementById('alert-tanggal').style.display = 'block') {
            document.getElementById('alert-tanggal').style.display = 'none'
        }
        $("#history_datatables").html('');
        $.LoadingOverlay("show");
        $.ajax({
        url: base_url('laporan_pembayaran_narobil/load_datatables'),
        type: "POST",
        data: {
              startdate: startdate,
              enddate: enddate
            },
        success: function (response) {
          $("#history_datatables").html(response);
          $.LoadingOverlay("hide");
        },
        error: function(jqXHR, textStatus, errorThrown) { 
           alert('ERROR, REFRESH HALAMAN.')
           $.LoadingOverlay("hide");
        }
        });
      }
    };

    function export_excel(){
      let v = $("#reservation").val();
      let [sd, ed] = v.split("-");
      if(isNaN(sd,ed)){
        startdate = moment(sd,'DD/MM/YYYY').format('MM/DD/YYYY');
        enddate = moment(ed,'DD/MM/YYYY').format('MM/DD/YYYY');  
      };
      
      if ((startdate || enddate) == null){
        document.getElementById('alert-tanggal').style.display = 'block';
      }else{
        if (document.getElementById('alert-tanggal').style.display = 'block') {
            document.getElementById('alert-tanggal').style.display = 'none'
        }
        $.LoadingOverlay("show");
        $.ajax({
        url: base_url('laporan_pembayaran_narobil/export_excel'),
        type: "get",
        data: {
              startdate: startdate,
              enddate: enddate
            },
        success: function (response) {
          window.open(this.url,'_blank');
          $.LoadingOverlay("hide");
        },
        error: function(jqXHR, textStatus, errorThrown) { 
           alert('Error :Silahkan Refresh Halaman, Error Code :',textStatus)
           $.LoadingOverlay("hide");
        }
        });
      }
    };
  </script>
