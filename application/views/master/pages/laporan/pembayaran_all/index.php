<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Laporan Pembayaran Tahunan
                </h3>
            </div>
            <div class="card-body">
              <div class="row">
                
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Cluster</label>
                    <select id="id_cluster" class="form-control select2" style="width: 100%;">
                      <option selected="selected" value="all">Semua</option>
                    <?php foreach($v_data->mst_cluster as $cluster) : ?>
                      <option value="<?php echo($cluster->id_cluster); ?>">
                        <?php echo($cluster->nama_cluster); ?>
                      </option>
                    <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Pilih Tahun</label>
                    <select
                      class="select2bs4"
                      id="id_tahun_multiple"
                      multiple="multiple"
                      data-placeholder="Pilih Tahun"
                      style="width: 100%;">
                      <?php foreach($v_data->mst_tahun as $tahun) : ?>
                        <option>
                          <?php echo($tahun->tahun); ?>
                        </option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-12 mb-2"></div>
                <div class="col-md-6">
                  <div>
                      <button class="btn btn-primary" type="button" id="btn-cari">
                        <i class="fas fa-search"> </i>
                        Cari
                      </button>
                      <button class="btn btn-danger" type="button" id="btn-reset">
                        <i class="fas fa-refresh"> </i>
                        Reset
                      </button>
                      <button class="btn btn-success" type="button" id="btn-excel">
                        <i class="fas fa-file-excel"> </i>
                        Excel
                      </button>
                  </div>
                </div>
              </div>
            </div>
        </div>

        <div class="card" id="lay_laporan_pembayaran" style="display: none;">
            <div class="card-header">
                <h3 class="card-title">
                    Pencarian Tahun <u><span id="id_title_tahun"></span></u> Cluster <u><span id="id_title_cluster"></span></u>
                </h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">

                  <div id="id_lay" class="table-responsive">

                  </div>

                </div>
              </div>  
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

<script type="text/javascript">

  const Data = {
    mst_tahun: JSON.parse(`<?php echo(json_encode($v_data->mst_tahun)); ?>`),
    mst_bulan: JSON.parse(`<?php echo(json_encode($v_data->mst_bulan)); ?>`)
  };

  var table;
  window.addEventListener("load", () => {

    $(".select2,.select2bs4").select2({
      theme: 'bootstrap4'
    });

    $("#id_cluster").on("change", () => {
      $("#lay_laporan_pembayaran").hide();
    });

    $("#btn-excel").on("click", () => {

      let tahun_multiple = $("#id_tahun_multiple").val();
      let tahun = "all"

      let cluster = $("#id_cluster").val();
      let cluster_text = $( "#id_cluster option:selected" ).text();

      if($.isEmptyObject(tahun) || tahun == "" || tahun_multiple == "") {
        
        Swal.fire("Form Tahun Tidak Boleh Kosong !");
        $("#lay_laporan_pembayaran").hide();
        return;

      } else {
        
        let form = $("<form />", {
          action: base_url("laporan_pembayaran_all/export_excel"),
          method: "POST"
        });
        let txtCsrf = $("<input />", {
          type: "hidden",
          name: $("meta[name='csrf-name']").attr("content"),
          value: $("meta[name='csrf-token']").attr("content")
        });
        let txtTahun = $("<input />", {
          type: "hidden",
          name: "tahun",
          value: tahun
        });
        let txtCluster = $("<input />", {
          type: "hidden",
          name: "cluster",
          value: cluster
        });
        let txtTahunMultiple = $("<input />", {
          type: "hidden",
          name: "tahun_multiple",
          value: tahun_multiple
        });
        form.append(txtCsrf);
        form.append(txtTahun);
        form.append(txtCluster);
        form.append(txtTahunMultiple);
        form.appendTo("body");
        form.submit();
      }
    });

    $("#btn-reset").on("click", () => {
      $("#id_tahun").val("").trigger("change");
      $("#id_cluster").val("all").trigger("change");
      $("#id_tahun_multiple").val("").trigger("change");
    });

    $("#btn-cari").on("click", () => {

      let tahun_multiple = $("#id_tahun_multiple").val();
      let tahun = "all";
      let cluster = $("#id_cluster").val();
      let cluster_text = $( "#id_cluster option:selected" ).text();

      if(tahun == "" || tahun_multiple == "") {
        Swal.fire("Form Tahun Tidak Boleh Kosong !");
        $("#lay_laporan_pembayaran").hide();
        return;
      }

      $("#id_title_tahun").html(tahun);

      if(cluster != "all") {
        $("#id_title_cluster").html(cluster_text.toString().initCap());
      } else {
        $("#id_title_cluster").html(cluster.toString().initCap());
      }

      $.LoadingOverlay("show");
      $("#lay_laporan_pembayaran").hide();

      
      if(typeof table !== 'undefined') {
        table.destroy();
      }

      let tm = tahun_multiple.toString();

      let h = ``;
      tm.split(",").forEach(v => {
        h += `
           <th class="text-center" colspan="12" style="width: 3px;">
              ${v}
            </th>
        `;
      });
      let hh = ``;
      tm.split(",").forEach(v => {
        Data.mst_bulan.forEach(vv => {
          hh += `
            <th colspan="1">
              ${vv.nama_bulan}
            </th>
          `;
        })
      });

      $("#id_lay").html(`
        <table id="id-datatable" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th class="text-center" rowspan="2" style="width: 1px;">No</th>
              <th class="text-center" rowspan="2" style="width: 3px;">Kode Unit</th>
              <th class="text-center" rowspan="2" style="width: 3px;">Status Hunian</th>
              ${h}
            </tr>
            <tr>
              ${hh}
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>    
      `);

      let columnDefsOpt = [
        {
            targets: [0],
            className: 'text-right'
        },
        {
            targets: [1, 2],
            className: 'text-left'
        },
        {
          targets: [-1],
          className: "text-right"
        },
        {
          targets: [0],
          width: "5%"
        },
      ];
      let i=2;

      tm.split(",").forEach(v => {
        Data.mst_bulan.forEach(vv => {
          columnDefsOpt.push({
            targets: [i],
            className: "text-right"
          });
          i++;
        });
      });
      table = $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        "autoWidth": false,
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("laporan_pembayaran_all/get_datatables"),
            'type': 'GET',
            'data': {
              tahun: tahun,
              cluster: cluster,
              tahun_multiple: tahun_multiple
            }
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        initComplete: () => {
          $("#lay_laporan_pembayaran").show();
          $.LoadingOverlay("hide");   
        },
        columnDefs: columnDefsOpt
      });

      $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) {
        $.LoadingOverlay("hide");
      };

    });
  });
</script>