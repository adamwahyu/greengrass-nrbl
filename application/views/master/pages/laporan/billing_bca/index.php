
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        
      </div>
      <div class="card" id="data_datatables">
        <div class="card-header">
          <h3 class="card-title">
            Laporan Billing BCA
          </h3>
        </div>
        <div class="card-body" >
          <div class="row" style="margin-bottom: 10px">
            <div>
          <button class="btn btn-success" onclick="export_excel()"><i class="fas fa-file-excel"></i> Download Excel</button>
          </div>
          </div>
          <div class="table-responsive">
            <table id="id-datatable" class="table table-bordered table-striped" style="">
              <thead>
                <tr>
                  <th class="text-center" style="">No</th>
                  <th class="text-center" style="">No VA</th>
                  <th class="text-center" style="">Kode Unit</th>
                  <th class="text-center" style="">Tunggakkan</th>
                  <?php foreach($v_data->bulan as $bulan) : ?>
                    <th class="text-center" style="width: 3px;">
                      <?php echo($bulan->bulan); ?>
                    </th>
                  <?php endforeach; ?>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>

        <div class="card-footer">
        </div>

      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
</div>


<script type="text/javascript">
  window.addEventListener("load", () => {
  $(document).ready(function() {
    $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        "autoWidth": false,
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("laporan_billing_bca/get_datatables"),
            'type': 'GET',
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs: [
          {
              targets: [-1,-2,-3,-4,-5],
              className: 'text-right'
          },
          {
            targets: [],
            className: "text-center"
          },
          { "width": "2%", "targets": 0 },
          { "width": "200px", "targets": 2 },
          { "width": "10%", "targets": 4 },
          { "width": "10%", "targets": 5 },
          { "width": "10%", "targets": 6 },
          { "width": "10%", "targets": 7 },
        ]
      });
  });
    });
  function export_excel(){
        $.LoadingOverlay("show");
        $.ajax({
        url: base_url('laporan_billing_bca/export_excel'),
        type: "get",
        success: function (response) {
          window.open(this.url,'_blank');
          $.LoadingOverlay("hide");
        },
        error: function(jqXHR, textStatus, errorThrown) { 
           alert('Error :Silahkan Refresh Halaman, Error Code :',textStatus)
           $.LoadingOverlay("hide");
        }
        });
      }
</script>
