

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detil Form Tambah Menu</h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Form Buat TTS
          </h3>
        </div>
        <div class="card-body">
          <?php echo(form_open("", [
            "id" => "form_validation"
          ])); ?>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="kode_unit">Pilih Unit</label>
                  <select class="form-control" name="kodeunit" id="id_kodeunit">
                    <option value="">
                      - PILIH -
                    </option>
                  <?php foreach($v_data->unit as $unit) : ?>
                    <option value="<?php echo($unit->kode_unit); ?>">
                      <?php echo($unit->kode_unit); ?>
                    </option>
                  <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <div id="alert-tanggal" style="display: none">
                  </div>
                  <label>Pilih Tanggal Pembayaran:</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control float-right" id="id_tanggalpembayaran" name="tanggal_pembayaran" autocomplete="off">
                  </div>
                  <!-- /.input group -->
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="metode_pembayaran">
                    Metode Pembayaran
                  </label>
                  <select class="form-control" name="metode_pembayaran" id="id_metode_pembayaran">
                    <option value="">
                      - PILIH -
                    </option>
                  <?php foreach($v_data->metode_pembayaran as $mp) : ?>
                    <option value="<?php echo($mp->id_metode_pembayaran); ?>">
                      <?php echo($mp->desc_metode_pembayaran); ?>
                    </option>
                  <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="col-md-6 mx-auto">
                <div class="form-group">
                  <!-- <label for="total">Total</label> -->
                  <input type="hidden" id="id_total" name="total" value="0" class="form-control" placeholder="Ketik disini . . ." readonly="">
                </div>
              </div>

              <div class="col-md-12 mb-3"></div>
              <div class="col-md-12 tt" style="display: none;">
                <h6>
                  <b>List Tunggakan : </b>
                </h6>
              </div>
              <div class="col-md-12 tt" style="display: none;">
                <div class="table-responsive">
                  <table id="id-datatable" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th class="text-center">Pilih</th>
                        <th class="text-center">Invoice</th>
                        <th class="text-center">Tahun</th>
                        <th class="text-center">Bulan</th>
                        <th class="text-center">Jenis Iuran</th>
                        <th class="text-center">Tarif</th>
                        <th class="text-center">Denda</th>
                        <th class="text-center">Total</th>
                      </tr>
                    </thead>
                    <tbody id="tbody-tts">

                    </tbody>
                  </table>
                </div>
              </div>

              <div class="col-md-12 mb-2"></div>
              <div class="col-md-6">
                <button id="id-btn-submit" type="submit" class="btn btn-primary">
                  <i class="fas fa-save"> </i>
                  Simpan
                </button>
                <a href="<?php echo(base_url("tts")); ?>" class="btn btn-danger">
                  <i class="fas fa-arrow-left"> </i>
                  Kembali
                </a>
              </div>
            </div>
          <?php echo(form_close()); ?>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    var table, Doto;
    const checkboxIdKewajibanChange = () => {
      let total = 0;
      $(".id_kewajiban").each((i, v) => {
        if($(v).is(":checked")) {
          total += parseInt($(v).data("total"));
        }
      });
      $("#id_total").val(rupiah(total));
    }
    window.addEventListener("load", () => {

      $('#id_tanggalpembayaran').daterangepicker({
        "singleDatePicker": true,
        "locale": {
          "format": "DD/MM/YYYY"
        },
      });


      $("#id_kodeunit").on("change", function() {

        $(".tt").each((i, v) => {
          $(v).hide();
        });

        $("#id_total").val(0);

        $.LoadingOverlay("show");

        let kodeunit = $(this).val();
        $.post(base_url("tts/get_list_t_kewajiban_open_by_kodeunit"), {
          kodeunit: kodeunit
        })
        .done(r => {
          let html = "";
          r.data.forEach(v => {

            html += `
              <tr>
                <td class="text-center">
                  <input type="checkbox" class="id_kewajiban" onchange="checkboxIdKewajibanChange()" name="id_kewajiban[]" value="${v.id_kewajiban}" data-total="${v.nilai_kewajiban}">
                </td>
                <td class="text-left">
                  ${v.invoice}
                </td>
                <td class="text-left">
                ${v.tahun}
                </td>
                <td class="text-left">
                  ${v.kd_bulan}
                </td>
                <td class="text-left">
                  ${v.jenis_iuran}
                </td>
                <td class="text-right">
                  ${rupiah(v.tarif_permeter)}
                </td>
                <td class="text-right">
                  ${v.denda}
                </td>
                <td class="text-right">
                  ${rupiah(v.nilai_kewajiban)}
                </td>
              </tr>
            `;

          });

          Doto = r.data;
          if(r.data.length > 0) {
            $(".tt").each((i, v) => {
              $(v).show();
            });
          } else {
            Swal.fire("Data tidak ada");
          }
          $("#tbody-tts").html(html);
          $.LoadingOverlay("hide");
        })
      });

      $("#id_kodeunit").select2({
        theme: "bootstrap4"
      });

      $("#form_validation").validate({
        rules: {
          kodeunit: {
            required: true
          },
          metode_pembayaran: {
            required: true
          },
          tanggal_pembayaran: {
            required: true
          }
        },
        errorPlacement: function(error, element) {
          $(element).addClass("is-invalid");
        },
        success: function(label, element) {
          $(element).removeClass("is-invalid");
        }
      });

    });
  </script>
