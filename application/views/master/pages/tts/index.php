

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Tabel Menu
            </h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Daftar TTS
          </h3>

          <div class="card-tools">

            <a href="<?php echo(base_url("tts/create")); ?>" data-toggle="tooltip" data-placement="top" title="Tambah Data" class="btn btn-primary" >
              <i class="fas fa-plus"></i></a>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="id-datatable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th class="text-center" style="width: 1px;">No</th>
                  <th class="text-center" style="width: 3px;">TTH Code</th>
                  <th class="text-center" style="width: 3px;">Kode Unit</th>
                  <th class="text-center" style="width: 3px;">Metode Pembayaran</th>
                  <th class="text-center" style="width: 3px;">Tanggal Pembayaran</th>
                  <th class="text-center" style="width: 3px">Total</th>
                  <th class="text-center" style="width: 3px;">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Detil Data TTS <span id="id_title_kode_unit"></span></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <table id="id-datatable-trans-kewajiban" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th class="text-center">Invoice</th>
                      <th class="text-center">Tahun</th>
                      <th class="text-center">Bulan</th>
                      <th class="text-center">Jenis Iuran</th>
                      <th class="text-center">Tarif</th>
                      <th class="text-center">Denda</th>
                      <th class="text-center">Total</th>
                    </tr>
                  </thead>
                  <tbody id="tbody-tts">
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    // var table;

    const onDetail = (tth_code) => {
      $.LoadingOverlay("show");

      $.post(base_url("tts/get_detail_by_tth_code"), {
        tth_code: tth_code
      })
      .done(r => {
        let html = "";
        r.data.forEach(v => {
          html += `
            <tr>
              <td class="text-left">
                ${v.no_invoice}
              </td>
              <td class="text-right">
              ${v.tahun}
              </td>
              <td class="text-right">
                ${v.bulan}
              </td>
              <td class="text-left">
                ${v.jenis}
              </td>
              <td class="text-right">
                ${rupiah(v.tarif)}
              </td>
              <td class="text-right">
                ${v.denda}
              </td>
              <td class="text-right">
                ${rupiah(v.total)}
              </td>
            </tr>
          `;
        });
        $("#tbody-tts").html(html);

        $.LoadingOverlay("hide");

        $("#modal-lg").modal({
          show: true
        });

        $('.modal-dialog').draggable({
          handle: ".modal-header"
        });
        $('.modal-content').resizable();

      })

    }

    window.addEventListener("load", () => {

      table = $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        "autoWidth": false,
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("tts/get_datatables"),
            'type': 'GET'
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs: [
          {
              targets: [0, -2],
              className: 'text-right'
          },
          {
            targets: [-1],
            className: "text-center"
          },

        ]
      });

    });
  </script>
