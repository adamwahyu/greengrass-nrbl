

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Form Edit Profile
          </h3>
        </div>
        <div class="card-body">
          
          <?php echo(form_open("", [
            "id" => "form_validation"
          ])); ?>

            <div class="row">
              
              <div class="col-md-4">
                <div class="form-group">
                  <label for="namapemilik">
                    Nama Pemilik
                  </label>
                  <input type="text" name="namapemilik" class="form-control" id="id-txt-namapemilik" value="<?php echo($v_data->user->nama_pemilik); ?>" placeholder="Ketik disini . . .">
                </div>
              </div>
              
              <div class="col-md-4">
                <div class="form-group">
                  <label for="email">
                    Email
                  </label>
                  <input type="email" name="email" class="form-control" id="id-txt-email" value="<?php echo($v_data->user->email); ?>" placeholder="Ketik disini . . .">
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="password_new_confirm">
                    MSISDN
                  </label>
                  <input type="text" name="msisdn" class="form-control" id="id-txt-msisdn" value="<?php echo($v_data->user->msisdn); ?>" placeholder="Ketik disini . . .">
                </div>
              </div>

              <div class="col-md-12 mb-2"></div>
              <div class="col-md-6">
                <button type="submit" class="btn btn-primary">
                  <i class="fas fa-save"> </i>
                  Simpan
                </button>
                <a href="<?php echo(base_url("dashboard")); ?>" class="btn btn-danger">
                  <i class="fas fa-arrow-left"> </i>
                  Kembali ke Dashboard
                </a>
              </div>
            </div>
          
          <?php echo(form_close()); ?>

        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    // window.addEventListener("load", () => {

    //   $("#form_validation").validate({
    //     rules: {
    //       passwordlama: {
    //         required: true
    //       },
    //       passwordbaru: {
    //         required: true
    //       },
    //       passwordbarukonfirm: {
    //         required: true,
    //         equalTo: "#id-txt-password_new"
    //       }
    //     },
    //     errorPlacement: function(error, element) {
    //       $(element).addClass("is-invalid");
    //     },
    //     success: function(label, element) {
    //       $(element).removeClass("is-invalid");
    //     }
    //   });
      
    // });
  </script>
  