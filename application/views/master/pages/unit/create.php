

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detil Edit Unit</h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Form Tambah Unit
          </h3>
        </div>
        <div class="card-body">
          <?php echo(form_open("", [
            "id" => "form_validation"
          ])); ?>
            <div class="row">
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="kodeunit">
                        Kode Unit
                      </label>
                      <input
                        type="text"
                        name="kodeunit"
                        class="form-control" id="id-txt-kodeunit" value="" placeholder="Ketik disini . . ." required>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="luasunit">
                        Tarif Unit
                      </label>
                      <input
                        type="number"
                        name="luasunit"
                        class="form-control" id="id-txt-luasunit" value="" placeholder="Ketik disini . . ." required>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="nomer_va_narobil">
                        Nomer VA Narobil
                      </label>
                      <input
                        type="text"
                        name="nomerva_narobil"
                        class="form-control" id="id-txt-nomer_va_narobil" value="" placeholder="Ketik disini . . ." required>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="nomer_va_bca">
                        Nomer VA BCA
                      </label>
                      <input
                        type="text"
                        name="nomerva_bca"
                        class="form-control" id="id-txt-nomer_va_bca" value="" placeholder="Ketik disini . . ." >
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="notes">
                        Notes
                      </label>
                      <input type="text" class="form-control" name="notes"></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="namamenu">
                        Nama Pemilik
                      </label>
                      <input
                        type="text"
                        name="namapemilik"
                        class="form-control" id="id-txt-namapemilik" value="" placeholder="Ketik disini . . ." required>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="msisdn">
                        Cluster
                      </label>
                    <div class="form-group">
                      <select class="form-control" name="id_cluster" id="cluster" required="">
                        <option value="">--Pilih--</option>
                      <?php foreach ($cluster as $key => $value) {
                            echo('<option value='.$value->id_cluster.'>'.$value->nama_cluster.'</option>');
                      ?>
                      <?php } ?>
                    </select>
                    </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="msisdn">
                        MSISDN
                      </label>
                      <input
                        type="number"
                        name="msisdn"
                        class="form-control" id="id-txt-msisdn" value="" min='3' placeholder="Ketik disini . . .">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="email">
                        Email
                      </label>
                      <input
                        type="text"
                        name="email"
                        class="form-control" id="id-txt-email" value="" placeholder="Ketik disini . . .">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="email">
                        Status Hunian
                      </label>
                      <div class="form-group">
                    <select class="form-control" name="id_sts_unit" id="id_sts_unit" required="">

                      <?php foreach ($combo_huni as $key => $value) {
                            echo('<option value='.$value->id_sts_unit.'>'.$value->desc_sts_unit.'</option>');
                      ?>
                      <?php } ?>
                    </select>
                  </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="col-md-12 mb-2"></div>
              <div class="col-md-12">
                <button type="submit" class="btn btn-primary">
                  <i class="fas fa-save"> </i>
                  Simpan
                </button>
                <a href="<?php echo(base_url("unit")); ?>" class="btn btn-danger">
                  <i class="fas fa-arrow-left"> </i>
                  Kembali
                </a>
              </div>
          </div>
          <?php echo(form_close()); ?>
        </div>
        <!-- /.card-body -->
       <!--  <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  