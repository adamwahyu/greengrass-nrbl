

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detil Edit Unit</h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Form Edit Unit
          </h3>
        </div>
        <div class="card-body">
          <?php echo(form_open("", [
            "id" => "form_validation"
          ])); ?>
            <div class="row">
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-12">
                    <input type="hidden" name="id_unit" value="<?php echo($v_data->unit->id_unit); ?>">
                    <div class="form-group">
                      <label for="kodeunit">
                        Kode Unit
                      </label>
                      <input
                        type="text"
                        name="kodeunit"
                        class="form-control" id="id-txt-kodeunit" value="<?php echo($v_data->unit->kode_unit); ?>" placeholder="Ketik disini . . ." readonly>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="luasunit">
                        Tarif Unit
                      </label>
                      <input
                        type="number"
                        name="luasunit"
                        class="form-control" id="id-txt-luasunit" value="<?php echo($v_data->unit->luas_unit); ?>" placeholder="Ketik disini . . .">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="nomer_va_narobil">
                        Nomer VA Narobil
                      </label>
                      <input
                        type="text"
                        name="nomerva_narobil"
                        class="form-control" id="id-txt-nomer_va_narobil" value="<?php echo($v_data->unit->nomor_va_narobil); ?>" placeholder="Ketik disini . . ." readonly>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="nomer_va_bca">
                        Nomer VA BCA
                      </label>
                      <input
                        type="text"
                        name="nomerva_bca"
                        class="form-control" id="id-txt-nomer_va_bca" value="<?php echo($v_data->unit->nomor_va_bca); ?>" placeholder="Ketik disini . . ." readonly>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="notes">
                        Notes
                      </label>
                      <input type="text" class="form-control" value="<?php echo($v_data->unit->notes); ?>" name="notes"></input>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="namamenu">
                        Nama Pemilik
                      </label>
                      <input
                        type="text"
                        name="namapemilik"
                        class="form-control" id="id-txt-namapemilik" value="<?php echo($v_data->unit->nama_pemilik); ?>" placeholder="Ketik disini . . .">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="email">
                        Cluster
                      </label>
                      <div class="form-group">
                    <select class="form-control" name="id_cluster" id="id_cluster">

                      <?php foreach ($cluster as $key => $value) {
                        if ($v_data->unit->id_cluster == $value->id_cluster) {
                            echo('<option value='.$value->id_cluster.' selected>'.$value->nama_cluster.'</option>');
                      }else{
                            echo('<option value='.$value->id_cluster.'>'.$value->nama_cluster.'</option>');
                      }
                      ?>
                      <?php } ?>
                    </select>
                  </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="msisdn">
                        MSISDN
                      </label>
                      <input
                        type="text"
                        name="msisdn"
                        class="form-control" id="id-txt-msisdn" value="<?php echo($v_data->unit->msisdn); ?>" placeholder="Ketik disini . . .">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="email">
                        Email
                      </label>
                      <input
                        type="text"
                        name="email"
                        class="form-control" id="id-txt-email" value="<?php echo($v_data->unit->email); ?>" placeholder="Ketik disini . . .">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="email">
                        Status Hunian
                      </label>
                      <div class="form-group">
                    <select class="form-control" name="id_sts_unit" id="id_sts_unit">

                      <?php foreach ($combo_huni as $key => $value) {
                        if ($v_data->unit->id_sts_unit == $value->id_sts_unit) {
                            echo('<option value='.$value->id_sts_unit.' selected>'.$value->desc_sts_unit.'</option>');
                      }else{
                            echo('<option value='.$value->id_sts_unit.'>'.$value->desc_sts_unit.'</option>');
                      }
                      ?>
                      <?php } ?>
                    </select>
                  </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="col-md-12 mb-2"></div>
              <div class="col-md-12">
                <button type="submit" class="btn btn-primary">
                  <i class="fas fa-save"> </i>
                  Simpan
                </button>
                <a href="<?php echo(base_url("unit")); ?>" class="btn btn-danger">
                  <i class="fas fa-arrow-left"> </i>
                  Kembali
                </a>
              </div>
          </div>
          <?php echo(form_close()); ?>
        </div>
        <!-- /.card-body -->
       <!--  <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  
