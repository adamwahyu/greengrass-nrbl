`

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>
              Tabel Reset Pass
            </h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div> --><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Reset Password
          </h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="id-datatable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th class="text-center" style="width: 1px !important;">No</th>
                  <th class="text-center" style="width: 3px;">Kode Unit</th>
                  <th class="text-center" style="width: 3px;">Nama Pemilik</th>
                  <th class="text-center" style="width: 3px;">Email</th>
                  <th class="text-center" style="width: 3px;">MSISDN</th>
                  <th class="text-center" style="width: 3px;">Nomer VA Narobil</th>
                  <th class="text-center" style="width: 3px;">Nomer VA BCA</th>
                  <th class="text-center" style="width: 3px;">Nominal IPL</th>
                  <th class="text-center" style="width: 3px;">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  
  <script type="text/javascript">

    var table;

    const ConfirmResetPass = (kode_unit) => {

      Swal.fire({
        title: 'Apakah anda yakin mereset password ?',
        text: "Anda tidak akan dapat mengembalikan ini!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya'
      }).then((result) => {

        if (result.value) {
          
          $.post(base_url("resetpass/ajax_reset_password"),{
            kode_unit: kode_unit
          })
          .done(r => {
            console.log(r);
            Swal.fire(
              'Pesan Sukses',
              'Data telah diperbarui.',
              'success'
            );
            table.ajax.reload(null, false);
          
          })
          .fail(e => {

          })

          
        }
      });

    }
    window.addEventListener("load", () => {

      table = $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        // "autoWidth": false,
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("resetpass/get_datatables"),
            'type': 'GET'
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs: [
          {
              targets: [],
              className: 'text-right'
          },
          {
            targets: -1,
            className: 'text-center'
          }
        ]
      });

    });
  </script>