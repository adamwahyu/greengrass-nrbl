

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Form Cluster
          </h3>
        </div>
        <div class="card-body">
          
          <?php echo(form_open("", [
            "id" => "form_validation"
          ])); ?>

            <input type="hidden" name="id_cluster" value="<?php echo($v_data->data->id_cluster); ?>" />

            <div class="row">
              <input type="hidden" name="id_perumahan" class="form-control" id="id_perumahan" value="<?php echo($v_data->data->id_perumahan); ?>">
              <input type="hidden" name="status" class="form-control" id="status" value="<?php echo($v_data->data->sts_active); ?>">

              <div class="col-md-6">
                <div class="form-group">
                  <label for="nama_cluster">
                    Nama Cluster
                  </label>
                  <input type="text" name="nama_cluster" class="form-control" id="nama_cluster" value="<?php echo($v_data->data->nama_cluster); ?>" placeholder="Ketik disini . . .">
                </div>
              </div>

              <!-- <div class="col-md-4">
                <div class="form-group">
                  <label for="namapemilik">
                    Perusahaan
                  </label>
                  <select class="form-control" name="id_perumahan" id="id_perumahan">
                    <option value="">
                      -- PILIH --
                    </option>

                  <?php foreach($v_data->perumahan as $p) : ?>
                    
                    <?php if($p->id_perumahan == $v_data->data->id_perumahan) : ?>
                      <option value="<?php echo($p->id_perumahan); ?>" selected>
                        <?php echo($p->nama_perumahan); ?>
                      </option>
                    <?php else : ?>
                      <option value="<?php echo($p->id_perumahan); ?>">
                        <?php echo($p->nama_perumahan); ?>
                      </option>
                    <?php endif; ?>
                    
                  <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="nama_cluster">
                    Nama Cluster
                  </label>
                  <input type="text" name="nama_cluster" class="form-control" id="nama_cluster" value="<?php echo($v_data->data->nama_cluster); ?>" placeholder="Ketik disini . . .">
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="status">
                    Status
                  </label>
                  <select class="form-control" name="status">
                    <option value="">
                      -- PILIH --
                    </option>
                  <?php if($v_data->data->sts_active == 1) : ?>
                    <option value="0">
                      Tidak Aktif
                    </option>
                    <option value="1" selected>
                      Aktif
                    </option>
                  <?php else : ?>
                    <option value="0" selected>
                      Tidak Aktif
                    </option>
                    <option value="1">
                      Aktif
                    </option>
                  <?php endif; ?>
                    
                  </select>
                </div>
              </div> -->

              <div class="col-md-12 mb-2"></div>
              <div class="col-md-6">
                <button type="submit" class="btn btn-primary">
                  <i class="fas fa-save"> </i>
                  Simpan
                </button>
                <a href="<?php echo(base_url("cluster")); ?>" class="btn btn-danger">
                  <i class="fas fa-arrow-left"> </i>
                  Kembali
                </a>
              </div>
            </div>
          
          <?php echo(form_close()); ?>

        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>