

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tabel Group</h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Cluster</h3>
          <div class="card-tools">

            <a href="<?php echo(base_url("cluster/create")); ?>" data-toggle="tooltip" data-placement="top" title="Tambah Data" class="btn btn-primary" >
              <i class="fas fa-plus"></i></a>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="id-datatable" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th class="text-center" width="50">No</th>
                  <th class="text-center">Nama Perumahan</th>
                  <th class="text-center">Cluster</th>
                  <th class="text-center">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.card-body -->
       <!--  <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    var table;
    
    window.addEventListener("load", () => {

      table = $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("cluster/get_datatables"),
            'type': 'GET'
        },
        columnDefs:[
          {
            targets: [-1],
            className: "text-center"
          }
        ],
        drawCallback: function() {
          $('[data-toggle="tooltip"]').tooltip()
        }
      });

    });
  </script>