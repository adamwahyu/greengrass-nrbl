

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Form Add Cluster
          </h3>
        </div>
        <div class="card-body">

          <?php echo(form_open("", [
            "id" => "form_validation"
          ])); ?>

            <div class="row">

              <input type="hidden" name="id_perumahan" class="form-control" id="id_perumahan" value="1">
              <input type="hidden" name="status" class="form-control" id="status" value="1">

              <div class="col-md-6">
                <div class="form-group">
                      <label for="nama_cluster">
                    Nama Cluster
                  </label>
                  <input type="text" name="nama_cluster" class="form-control" id="nama_cluster" value="" placeholder="Ketik disini . . .">
                </div>
              </div>

              <div class="col-md-12 mb-2"></div>
              <div class="col-md-6">
                <button type="submit" class="btn btn-primary">
                  <i class="fas fa-save"> </i>
                  Simpan
                </button>
                <a href="<?php echo(base_url("cluster")); ?>" class="btn btn-danger">
                  <i class="fas fa-arrow-left"> </i>
                  Kembali
                </a>
              </div>
            </div>

          <?php echo(form_close()); ?>

        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
