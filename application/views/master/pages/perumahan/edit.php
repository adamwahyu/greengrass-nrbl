

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detil Edit User</h1>
          </div>
          <div class="col-sm-6">
          
          </div>
        </div>
      </div> -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Form Edit Perumahan
          </h3>
        </div>
        <div class="card-body">
          <?php echo(form_open("", [
            "id" => "form_validation"
          ])); ?>

            <div class="row">
              <div class="col-md-12">
                <input type="hidden" name="id_perumahan" value="<?php echo($v_data->perumahan->id_perumahan); ?>">
              </div>

              <div class="col-md-6">
                <div class="form-group">
                    <label for="username">
                      Nama Perumahan
                    </label>
                    <input
                      type="text"
                      class="form-control"
                      id="nama_perumahan"
                      name="nama_perumahan"
                      value="<?php echo($v_data->perumahan->nama_perumahan); ?>"
                      placeholder="Ketik disini . . .">
                  </div>
              </div>

              <div class="col-md-12 mb-2"></div>
              <div class="col-md-12">
                <button type="submit" class="btn btn-primary">
                  <i class="fas fa-save"> </i>
                  Simpan
                </button>
                <a href="<?php echo(base_url("perumahan")); ?>" class="btn btn-danger">
                  <i class="fas fa-arrow-left"> </i>
                  Kembali
                </a>
              </div>
            </div>
          <?php echo(form_close()); ?>
        </div>
        <!-- /.card-body -->
       <!--  <div class="card-footer">
          Footer
        </div> -->
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">

    const Data = {
      Perumahan: JSON.parse(`<?php echo(json_encode($v_data->perumahan)); ?>`)
    }

    window.addEventListener("load", () => {
      $("#form_validation").validate({
        rules: {
          id_perumahan: {
            required: true
          },
          nama_perumahan: {
            required: true
          }
        },
        errorPlacement: function(error, element) {
          $(element).addClass("is-invalid");
        },
        success: function(label, element) {
          $(element).removeClass("is-invalid");
        }
      });

    });
  </script>
  