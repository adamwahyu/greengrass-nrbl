

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            History Pembayaran
          </h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <div id="alert-tanggal" style="display: none">
                <div class="alert alert-danger alert-dismissible col-md-6 col-xs-12">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  Silahkan Pilih Tanggal.!
                </div>
                </div>
                <label>Pilih Tanggal:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="far fa-calendar-alt"></i>
                    </span>
                  </div>
                  <input type="text" class="form-control float-right" id="reservation" autocomplete="off">
                </div>
              </div>
            </div>
            <div class="col-md-12 mb-2"></div>
            <div class="col-md-12">
              <button class="btn btn-primary" type="button" id="get_datatables" onclick="get_datatables()">
                <i class="fas fa-search"></i>
                Lihat Data
              </button>
              <button class="btn btn-success" type="button" onclick="export_excel()"> <i class="fas fa-file-excel"> </i> Excel</button>
              <button class="btn btn-warning" type="button" onclick="reset_datatables()">
                <i class="fas fa-refresh"></i>
                Reset
              </button>
            </div>
          </div>    
        </div>
        </div>

      <div id="history_datatables">
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Detil Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">

            <div class="col-xs-12 col-md-12">
              <input type="hidden" name="id_pembayaran" id="id_pembayaran">
              
              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_kodepembayaran">
                      Kode Pembayaran
                    </label>
                    <input type="text" class="form-control" id="id_kodepembayaran" placeholder="-" readonly>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_kodeunit">
                      Kode Unit
                    </label>
                    <input type="text" class="form-control" id="id_kodeunit" placeholder="-" readonly>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_nomervanarobil">
                      No Va Narobil
                    </label>
                    <input type="text" class="form-control" id="id_nomervanarobil" placeholder="-" readonly>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_nomervabca">
                      No Va BCA
                    </label>
                    <input type="text" class="form-control" id="id_nomervabca" placeholder="-" readonly>
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_tglpembayaran">
                      Tgl Pembayaran
                    </label>
                    <input type="text" class="form-control" id="id_tglpembayaran" placeholder="-" readonly>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_jmlpembayaran">
                      Jml Pembayaran
                    </label>
                    <input type="text" class="form-control" id="id_jmlpembayaran" placeholder="-" readonly>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_status">
                      Status Pembayaran
                    </label>
                    <input type="text" class="form-control" id="id_status" placeholder="-" readonly>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                    <label for="id_metodepembayaran">
                      Metode Pembayaran
                    </label>
                    <input type="text" class="form-control" id="id_metodepembayaran" placeholder="-" readonly>
                  </div>
                </div>
              </div>

            </div>
            <div class="col-md-12 col-xs-12">
              <div class="form-group">
                <label for="id_notes">
                  Notes
                </label>
                <textarea class="form-control" id="id_notes" name="notes" readonly=""></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>


  <script type="text/javascript">

    var startdate = null;
    var enddate = null;
    window.addEventListener("load", () => {
    function pick_date(){
      
      startdate = moment().format('MM/DD/YYYY');
      enddate = moment().format('MM/DD/YYYY');

      $('#reservation').daterangepicker({
      "autoApply": true,
      "locale": {
        "format": "DD/MM/YYYY",
        "separator": " - ",
        "applyLabel": "Apply",
        "cancelLabel": "Batal",
        "fromLabel": "Dari",
        "toLabel": "Sampai",
    },
    "startDate": moment().format('DD/MM/YYYY'),
    "endDate": moment().format('DD/MM/YYYY')
    }, function(start, end, label) {
      startdate = start.format('MM/DD/YYYY');
      enddate = end.format('MM/DD/YYYY');
    });
    };
    pick_date();

    });   

    function reset_datatables(){
      $('#history_datatables').html('');
      $("#reservation").val('');
      startdate = null;
      enddate = null;
    };

    function get_datatables(){
      let v = $("#reservation").val();
      let [sd, ed] = v.split("-");
      if(isNaN(sd,ed)){
        startdate = moment(sd,'DD/MM/YYYY').format('MM/DD/YYYY');
        enddate = moment(ed,'DD/MM/YYYY').format('MM/DD/YYYY');  
      };
      
      if ((startdate || enddate) == null){
        document.getElementById('alert-tanggal').style.display = 'block';
      }else{
        if (document.getElementById('alert-tanggal').style.display = 'block') {
            document.getElementById('alert-tanggal').style.display = 'none'
        }

        $("#history_datatables").html('');
        $.LoadingOverlay("show");
        $.ajax({
        url: base_url('history_pembayaran_warga/load_datatables'),
        type: "POST",
        data: {
              startdate: startdate,
              enddate: enddate
            },
        success: function (response) {
          $("#history_datatables").html(response);
          $.LoadingOverlay("hide");
        },
        error: function(jqXHR, textStatus, errorThrown) { 
           alert('Error :Silahkan Refresh Halaman, Error Code :',errorThrown)
           $.LoadingOverlay("hide");
        }
        });
      }
    };

    function export_excel(){
      let v = $("#reservation").val();
      let [sd, ed] = v.split("-");
      if(isNaN(sd,ed)){
        startdate = moment(sd,'DD/MM/YYYY').format('MM/DD/YYYY');
        enddate = moment(ed,'DD/MM/YYYY').format('MM/DD/YYYY');  
      };
      
      if ((startdate || enddate) == null){
        document.getElementById('alert-tanggal').style.display = 'block';
      }else{
        if (document.getElementById('alert-tanggal').style.display = 'block') {
            document.getElementById('alert-tanggal').style.display = 'none'
        }
        $.LoadingOverlay("show");
        $.ajax({
        url: base_url('history_pembayaran_warga/export_excel'),
        type: "get",
        data: {
              startdate: startdate,
              enddate: enddate
            },
        success: function (response) {
          window.open(this.url,'_blank');
          $.LoadingOverlay("hide");
        },
        error: function(jqXHR, textStatus, errorThrown) { 
           alert('Error :Silahkan Refresh Halaman, Error Code :',textStatus)
           $.LoadingOverlay("hide");
        }
        });
      }
    };

    const onView = (id_pembayaran) => {
      
      $.LoadingOverlay("show");

      $.post(base_url("history_pembayaran_warga/ajax_get_detail_by_id"), {
        id_pembayaran: id_pembayaran
      })

      .done(r => {

        let data = r.data;

        $("#id_pembayaran").val(data.id_pembayaran);
        $("#id_kodepembayaran").val(data.kode_pembayaran);
        $("#id_kodeunit").val(data.kode_unit);
        $("#id_nomervanarobil").val(data.nomor_va_narobil);
        $("#id_nomervabca").val(data.nomor_va_bca);
        $("#id_tglpembayaran").val(data.tgl_pembayaran);
        $("#id_jmlpembayaran").val(rupiah(data.jml_pembayaran));
        $("#id_status").val(data.status_pembayaran);
        $("#id_notes").val(data.notes);
        $("#id_metodepembayaran").val(data.desc_metode_pembayaran);

        $("#modal-xl").modal({
          show: true
        });

        $('.modal-dialog').draggable({
          handle: ".modal-header"
        });
        $('.modal-content').resizable();
        
        $.LoadingOverlay("hide");
      })
      .fail(e => {
        alert(e.toString());
      });

    }
  </script>
