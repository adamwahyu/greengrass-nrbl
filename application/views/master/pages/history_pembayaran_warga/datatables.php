<div class="card" id="data_datatables">
  <div class="card-body" >
    <div class="table-responsive">
      <table id="id-datatable" class="table table-bordered table-striped" style="margin: 0 auto;width:100%;table-layout: fixed;word-wrap:break-word;">
        <thead>
          <tr>
            <th class="text-center" style="">No</th>
            <th class="text-center" style="">Kode Pembayaran</th>
            <th class="text-center" style="">Kode Unit</th>
            <th class="text-center" style="">Tgl Pembayaran</th>
            <th class="text-center" style="">Jml Pembayaran</th>
            <th class="text-center" style="">Metode Pembayaran</th>
            <th class="text-center" style="">Action</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer">
  </div>
</div>


<script type="text/javascript">
  var startdate = "<?php echo $startdate ?>";
  var enddate = "<?php echo $enddate ?>";
  $(document).ready(function() {
    $("#id-datatable").DataTable({
        'language': {
          "emptyTable": "Data tidak ditemukan"
        },
        "autoWidth": false,
        'lengthChange': false,
        'searching': true,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': base_url("history_pembayaran_warga/get_datatables"),
            'type': 'GET',
            'data': {
              startdate: startdate,
              enddate: enddate
            }
        },
        drawCallback: () => {
          $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs: [
          {
              targets: [4],
              className: 'text-right'
          },
          {
            targets: [-2,-1, 1, 2, 3],
            className: "text-center"
          },
          // {
          //   targets: [ -1]
          //   // width: "5%"
          // },
          { "width": "10px", "targets": 0 },
          { "width": "160px", "targets": 1 },
          { "width": "80px", "targets": 2 },
          { "width": "120px", "targets": 3 },
          { "width": "120px", "targets": 4 },
          { "width": "140px", "targets": 5 },
          { "width": "80px", "targets": 6 },
          // { "width": "70px", "targets": 4 },
          // { "width": "70px", "targets": 8 }
        ]
      })
  });
</script>