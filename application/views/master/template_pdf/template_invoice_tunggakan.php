<html lang="en">
<head>
<meta charset="UTF-8">
<title>Cetak Tunggakan Invoice</title>
<style type="text/css">
  :root {
      --blue: #007bff;
      --indigo: #6610f2;
      --purple: #6f42c1;
      --pink: #e83e8c;
      --red: #dc3545;
      --orange: #fd7e14;
      --yellow: #ffc107;
      --green: #28a745;
      --teal: #20c997;
      --cyan: #17a2b8;
      --white: #ffffff;
      --gray: #6c757d;
      --gray-dark: #343a40;
      --primary: #007bff;
      --secondary: #6c757d;
      --success: #28a745;
      --info: #17a2b8;
      --warning: #ffc107;
      --danger: #dc3545;
      --light: #f8f9fa;
      --dark: #343a40;
      --breakpoint-xs: 0;
      --breakpoint-sm: 576px;
      --breakpoint-md: 768px;
      --breakpoint-lg: 992px;
      --breakpoint-xl: 1200px;
      --font-family-sans-serif: "Source Sans Pro", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
      --font-family-monospace: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
    }

    *,
    *::before,
    *::after {
      box-sizing: border-box;
    }

    html {
      font-family: sans-serif;
      line-height: 1.15;
      -webkit-text-size-adjust: 100%;
      -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }

    article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
      display: block;
    }

    body {
      margin: 0;
      font-family: "Source Sans Pro", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
      font-size: 1rem;
      font-weight: 400;
      line-height: 1.5;
      color: #212529;
      text-align: left;
      background-color: #ffffff;
    }

    [tabindex="-1"]:focus {
      outline: 0 !important;
    }

    hr {
      box-sizing: content-box;
      height: 0;
      overflow: visible;
    }

    h1, h2, h3, h4, h5, h6 {
      margin-top: 0;
      margin-bottom: 0.5rem;
    }

    p {
      margin-top: 0;
      margin-bottom: 1rem;
    }

    abbr[title],
    abbr[data-original-title] {
      text-decoration: underline;
      -webkit-text-decoration: underline dotted;
      text-decoration: underline dotted;
      cursor: help;
      border-bottom: 0;
      -webkit-text-decoration-skip-ink: none;
      text-decoration-skip-ink: none;
    }

    address {
      margin-bottom: 1rem;
      font-style: normal;
      line-height: inherit;
    }

    ol,
    ul,
    dl {
      margin-top: 0;
      margin-bottom: 1rem;
    }

    ol ol,
    ul ul,
    ol ul,
    ul ol {
      margin-bottom: 0;
    }

    dt {
      font-weight: 700;
    }

    dd {
      margin-bottom: .5rem;
      margin-left: 0;
    }

    blockquote {
      margin: 0 0 1rem;
    }

    b,
    strong {
      font-weight: bolder;
    }

    small {
      font-size: 80%;
    }

    sub,
    sup {
      position: relative;
      font-size: 75%;
      line-height: 0;
      vertical-align: baseline;
    }

    sub {
      bottom: -.25em;
    }

    sup {
      top: -.5em;
    }

    a {
      color: #007bff;
      text-decoration: none;
      background-color: transparent;
    }

    a:hover {
      color: #0056b3;
      text-decoration: none;
    }

    a:not([href]):not([tabindex]) {
      color: inherit;
      text-decoration: none;
    }

    a:not([href]):not([tabindex]):hover, a:not([href]):not([tabindex]):focus {
      color: inherit;
      text-decoration: none;
    }

    a:not([href]):not([tabindex]):focus {
      outline: 0;
    }

    pre,
    code,
    kbd,
    samp {
      font-family: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
      font-size: 1em;
    }

    pre {
      margin-top: 0;
      margin-bottom: 1rem;
      overflow: auto;
    }

    figure {
      margin: 0 0 1rem;
    }

    img {
      vertical-align: middle;
      border-style: none;
    }

    svg {
      overflow: hidden;
      vertical-align: middle;
    }

    table {
      border-collapse: collapse;
    }

    caption {
      padding-top: 0.75rem;
      padding-bottom: 0.75rem;
      color: #6c757d;
      text-align: left;
      caption-side: bottom;
    }

    th {
      text-align: inherit;
    }

    label {
      display: inline-block;
      margin-bottom: 0.5rem;
    }

    button {
      border-radius: 0;
    }

    button:focus {
      outline: 1px dotted;
      outline: 5px auto -webkit-focus-ring-color;
    }

    input,
    button,
    select,
    optgroup,
    textarea {
      margin: 0;
      font-family: inherit;
      font-size: inherit;
      line-height: inherit;
    }

    button,
    input {
      overflow: visible;
    }

    button,
    select {
      text-transform: none;
    }

    select {
      word-wrap: normal;
    }

    button,
    [type="button"],
    [type="reset"],
    [type="submit"] {
      -webkit-appearance: button;
    }

    button:not(:disabled),
    [type="button"]:not(:disabled),
    [type="reset"]:not(:disabled),
    [type="submit"]:not(:disabled) {
      cursor: pointer;
    }

    button::-moz-focus-inner,
    [type="button"]::-moz-focus-inner,
    [type="reset"]::-moz-focus-inner,
    [type="submit"]::-moz-focus-inner {
      padding: 0;
      border-style: none;
    }

    input[type="radio"],
    input[type="checkbox"] {
      box-sizing: border-box;
      padding: 0;
    }

    input[type="date"],
    input[type="time"],
    input[type="datetime-local"],
    input[type="month"] {
      -webkit-appearance: listbox;
    }

    textarea {
      overflow: auto;
      resize: vertical;
    }

    fieldset {
      min-width: 0;
      padding: 0;
      margin: 0;
      border: 0;
    }

    legend {
      display: block;
      width: 100%;
      max-width: 100%;
      padding: 0;
      margin-bottom: .5rem;
      font-size: 1.5rem;
      line-height: inherit;
      color: inherit;
      white-space: normal;
    }

    progress {
      vertical-align: baseline;
    }

    [type="number"]::-webkit-inner-spin-button,
    [type="number"]::-webkit-outer-spin-button {
      height: auto;
    }

    [type="search"] {
      outline-offset: -2px;
      -webkit-appearance: none;
    }

    [type="search"]::-webkit-search-decoration {
      -webkit-appearance: none;
    }

    ::-webkit-file-upload-button {
      font: inherit;
      -webkit-appearance: button;
    }

    output {
      display: inline-block;
    }

    summary {
      display: list-item;
      cursor: pointer;
    }

    template {
      display: none;
    }

    [hidden] {
      display: none !important;
    }

    h1, h2, h3, h4, h5, h6,
    .h1, .h2, .h3, .h4, .h5, .h6 {
      margin-bottom: 0.5rem;
      font-family: inherit;
      font-weight: 500;
      line-height: 1.2;
      color: inherit;
    }

    h1, .h1 {
      font-size: 2.5rem;
    }

    h2, .h2 {
      font-size: 2rem;
    }

    h3, .h3 {
      font-size: 1.75rem;
    }

    h4, .h4 {
      font-size: 1.5rem;
    }

    h5, .h5 {
      font-size: 1.25rem;
    }

    h6, .h6 {
      font-size: 1rem;
    }
    body {
        font-family: Verdana, Arial, sans-serif;
        font-size: 12px;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
    }
    p {
      margin-top: 0;
      margin-bottom: 0;
    }
    .h7{
      font-size:10px;
    }
    .h8{
      font-size:7px;
    }
    ol {
      padding-left: 1.8em;
    }
</style>

</head>
<body>

  <table width="100%">
    <tr>
        <td valign="top" width="10%">
          <img src="<?php echo($v_data->base64_image); ?>" width="120">
        </td>
        <td align="left">
            <h6 class="h7">FORUM WARGA GREEN GRASS</h6>
            <p>
             Jl. Kp. Rw. Bar., Pd. Pucung, Kec. Pd. Aren, Kota Tangerang Selatan,
             <br> Banten 15229 <br>
              No. Telepon : +62*********
            </p>
        </td>
        <td style="margin: 0 !important; padding: 0 !important">
          <div style="margin-top: 0px; border: 1px solid black;">
            <p>
              <b>&nbsp;&nbsp;&nbsp;PERHATIAN : </b>
            </p>
            <ol start="1">
              <li>
                Tagihan yang tertera di invoice ini adalah data yang ada sampai dengan tanggal cetak invoice
              </li>
              <br>
              <li>
                Pembayaran selain ke rekening yang tercantum dalam invoice ini dianggap tidak sah
              </li>
            </ol>
          </div>
        </td>
    </tr>
  </table>

  <h5>
    <u>INVOICE</u>
  </h5>

  <table width="100%">
    <tr>
      <td width="50%">
        <table>
          <tr>
            <th>ID Account</th>
            <td>: <?php echo($v_data->unit->kode_unit); ?></td>
          </tr>
          <tr>
            <th>Nama</th>
            <td>: <?php echo($v_data->unit->nama_pemilik); ?></td>
          </tr>
          <tr>
            <th>Cluster</th>
            <td>: <?php echo($v_data->unit->kode_unit); ?></td>
          </tr>
          <tr style="color:transparent;">
            <th>&nbsp</th>
            <td>&nbsp</td>
          </tr>

        </table>
      </td>
      <td width="50%">
        <table>
          <tr>
            <th>Tanggal Cetak</th>
            <td>: <?php echo($v_data->tanggal_cetak); ?></td>
          </tr>
          <tr>
            <th>Tanggal Jatuh Tempo</th>
            <td>: <?php echo($v_data->tanggal_jatuh_tempo); ?></td>
          </tr>
          <tr>
            <th>Nominal IPL</th>
            <td>: <?php echo($v_data->unit->luas_unit); ?></td>
          </tr>

          <tr style="color: transparent;">
            <th>&nbsp</th>
            <td>&nbsp</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <br>
  <table width="100%">
    <thead>
      <tr style="border-bottom: 1px solid black;">
        <th>No Invoice</th>
        <th>Bulan</th>
        <th style="text-align: right;">Tahun</th>
        <th>Jenis Iuran</th>
        <th style="text-align: right;">Tarif</th>
        <th style="text-align: right;">Denda</th>
        <th style="text-align: right;">Total</th>
      </tr>
    </thead>
    <tbody>
    <?php $grand_total = 0; ?>
    <?php foreach($v_data->tunggakan as $tungg) : ?>
      <tr>
        <td>IVC.<?php echo($v_data->unit->kode_unit); ?>.<?php echo($tungg->bulan); ?>.<?php echo($tungg->tahun); ?></td>
        <td>
          <?php echo(bulan((int)$tungg->bulan)); ?>
        </td>
        <td style="text-align: right;">
          <?php echo($tungg->tahun); ?>
        </td>
        <td>Standard</td>
        <td style="text-align: right;">
          <?php echo rupiah($tungg->tarif_permeter); ?>
        </td>
        <td style="text-align: right;">0</td>
        <td style="text-align: right;">
          <?php echo(rupiah($tungg->nilai_kewajiban)); ?>
        </td>
      </tr>
    <?php $grand_total += (float)$tungg->nilai_kewajiban; ?>
    <?php endforeach; ?>
    </tbody>
    <tfoot style="border-top: 1px solid black;">
        <tr>
            <td colspan="4"></td>
            <td align="right">Grand Total</td>
            <td align="right">0</td>
            <td align="right"><?php echo rupiah($grand_total); ?></td>
        </tr>
    </tfoot>
  </table>

  <table width="100%">
    <tr>
      <td width="100%">
        <div>
      <!--     <p>
            <b>CARA PEMBAYARAN :</b>
          </p>
          <ol start="1">
            <li>
              Melalui Virtual Account BCA No. 04012000275
            </li>
            <li>
              Melalui Tranfer Bank Mandiri. No. 164-00-0174-5845. a.n : Forum Warga Residence One
            </li>
            <li>
              Cek/ Bilyet Giro dianggap sah setelah dana diterima direkening Forum Warga Residence One Serpong
            </li>
          </ol> -->
        </div>
        <div>
          <p>
            <b>CATATAN PENTING :</b>
          </p>
          <ol start="1">
            <li>
              Pembayaran melalui Transfer Bank wajib ditulis keterangan No. unit dan melakukan konfirmasi pembayaran.
            </li>
            <li>
              Konfirmasi Pembayaran bisa melalui aplikasi ecluster atau datang langsung ke kantor pengelola.
            </li>
            <li>
              Pembayaran iuran paling lambat tanggal 20 setiap bulannya.
            </li>
            <li>
              Jam buka Loket : Senin -Jumat jam 09.00 s/d 14.00 WIB, sabtu jam 09.00 s/d 12.00 WIB
            </li>
            <li>
              Invoice Otomasi di cetak setiap bulannya dan tidak memerlukan tanda tangan
            </li>
          </ol>
        </div>
        <div>
          <p>
            Untuk Konfirmasi dan pertanyaan dapat menghubungi kantor Pengelola di : <br>
            Green Grass <br>
            <!-- Telp. +6221-295 29911 dengan Vidi<br> -->
            <!-- Email : Forwards.residenceone@gmail.com -->
          </p>
        </div>
      </td>
    </tr>
    <tr>
      <td width="100%">
        <p>Note : Apabila sudah melakukan Pembayaran harap diabaikan tagihan ini. Terima kasih</p>
      </td>
    </tr>
  </table>

</body>
</html>
