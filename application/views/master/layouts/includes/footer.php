<!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.0
    </div>
    <?php
            $perumahan = $this->db->query("select * from ref_perumahan where sts_active=1
            ")->row_array();
            $result = $perumahan['nama_perumahan'];

            ?>
    <strong>Copyright &copy; 2014-2019 <a href="javascript:void(0)"><?=$result?></a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo(base_url("asset/plugins/")); ?>jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url("asset/plugins/jquery-ui/jquery-ui.min.js")); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo(base_url("asset/plugins/")); ?>bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo(base_url("asset/adminlte/")); ?>js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo(base_url("asset/adminlte/")); ?>js/demo.js"></script>
<script src="<?php echo(base_url("asset/plugins/")); ?>datatables/jquery.dataTables.js"></script>
<script src="<?php echo(base_url("asset/plugins/")); ?>datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- Date Range Picker-->
<script src="<?php echo(base_url("asset/plugins/")); ?>inputmask/min/jquery.inputmask.bundle.min.js"></script>
<script src="<?php echo(base_url("asset/plugins/")); ?>moment/moment.min.js"></script>
<script src="<?php echo(base_url("asset/plugins/")); ?>inputmask/min/jquery.inputmask.bundle.min.js"></script>

<script src="<?php echo(base_url("asset/")); ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- SweetAlert2 -->
<script src="<?php echo(base_url("asset/plugins/")); ?>sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo(base_url("asset/plugins/")); ?>toastr/toastr.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url("asset/plugins/jquery-validation/jquery.validate.min.js")); ?>"></script>

<script src="<?php echo(base_url("asset/")); ?>plugins/select2/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo(base_url("asset/plugins/")); ?>jquery-loading-overlay/dist/loadingoverlay.min.js"></script>
  <!-- pace-progress -->
<script src="<?php echo(base_url("asset/")); ?>plugins/pace-progress/pace.min.js"></script>
<script type="text/javascript" src="text/css" href="<?php echo(base_url("asset/plugins/datepicker/dist/datepicker.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo(base_url("asset/script/init.js")); ?>"></script>
<script type="text/javascript">
  window.addEventListener("load", () => {

    // $("#auth\/logout").on("click", (e) => {
    //   e.preventDefault();
    //   alert("asdasd");
    // });
    <?php if($this->session->flashdata("error")) : ?>
      setTimeout(() => {
        Swal.fire(
          'Pesan Error',
          `<?php echo($this->session->flashdata("error")); ?>`,
          'error'
        );
      }, 500)
    <?php unset($_SESSION["error"]); ?>
    <?php endif; ?>
    <?php if($this->session->flashdata("success")) : ?>
      setTimeout(() => {
        Swal.fire(
          'Pesan Sukses',
          `<?php echo($this->session->flashdata("success")); ?>`,
          'success'
        );
      }, 500)
    <?php unset($_SESSION["success"]); ?>
    <?php endif; ?>

  });
</script>
</body>
</html>
