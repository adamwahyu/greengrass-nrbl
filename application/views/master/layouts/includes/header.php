
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?php
          $perumahan = $this->db->query("select * from ref_perumahan where sts_active=1
          ")->row_array();
          $result = $perumahan['nama_perumahan'];

          ?>
 
  <title>Perumahan | <?=$result?></title>
  <link rel="icon" href="<?php echo(base_url("asset/img/green_grass.png")); ?>" type="image/png" sizes="16x16">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-name" content="<?php echo($this->security->get_csrf_token_name()); ?>" charset="utf-8">
  <meta name="csrf-token" content="<?php echo($this->security->get_csrf_hash()); ?>" charset="utf-8">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/plugins/")); ?>fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Daterangepicker -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/")); ?>plugins/daterangepicker/daterangepicker.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/adminlte/")); ?>css/adminlte.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/plugins/")); ?>datatables-bs4/css/dataTables.bootstrap4.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/plugins/")); ?>sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/plugins/")); ?>toastr/toastr.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/")); ?>plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo(base_url("asset/")); ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo(base_url("asset/")); ?>plugins/jquery-ui/jquery-ui.min.css">
  <!-- pace-progress -->
  <link rel="stylesheet" href="<?php echo(base_url("asset/")); ?>plugins/pace-progress/themes/blue/pace-theme-flat-top.css">

  <link rel="stylesheet" type="text/css" href="<?php echo(base_url("asset/plugins/datepicker/dist/datepicker.min.css")); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo(base_url("asset/css/init.css")); ?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
  <style media="screen">
    body {
      /*font-family:muli,sans-serif;*/
      font-size: 0.9rem !important;
    }
  .navbar-nav>.user-menu>.dropdown-menu {
      width: 180px !important;
  }
  .color-palette {
      height: 35px;
      line-height: 35px;
      text-align: right;
      padding-right: .75rem;
    }

    .color-palette.disabled {
      text-align: center;
      padding-right: 0;
      display: block;
    }

    .color-palette-set {
      margin-bottom: 15px;
    }

    .color-palette span {
      display: block;
      font-size: 12px;
    }
    .color-palette span span {
      display: block;
      font-size: 12px;
    }
    .color-palette.disabled span {
      display: block;
      text-align: left;
      padding-left: .75rem;
    }

    .color-palette-box h4 {
      position: absolute;
      left: 1.25rem;
      margin-top: .75rem;
      color: rgba(255, 255, 255, 0.8);
      font-size: 12px;
      display: block;
      z-index: 7;
    }

    .nav-treeview {
      margin-left: 10px;
    }

    @media (min-width: 992px){
      .modal-lg, .modal-xl {
          max-width: 1000px;
      }
    }

  </style>
  <script type="text/javascript">
    const base_url = (url = "") => {
      return `<?php echo(base_url("")); ?>${url}`
    }
  </script>
  <style type="text/css">
    /*td.nowrap {
      white-space: nowrap;
    }*/
    /*th, td {
        white-space: nowrap;
    }*/
    .dropdown-menu-lg {
      min-width: 0px !important;
    }
  </style>
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <!-- <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div> -->
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <?php if(count($v_menu_widget) > 0) : ?>
        <li class="dropdown user user-menu">
            <a href="#" class="nav-link" data-toggle="dropdown">
              <img src="<?php echo(base_url("asset/img/account2.png")); ?>" class="user-image" alt="User Image">
              <span href="javascript:void(0)" class="hidden-xs">
                <?php if(is_null($v_user->kode_unit)) : ?>
                  <?php //echo($v_user->group_name); ?>
                  <!-- <br /> -->
                <?php else : ?>
                  <?php //echo($v_user->nama_pemilik); ?>
                  <!-- <br /> -->
                <?php endif; ?>
                <?php echo(ucfirst(strtolower($v_user->group_name))); ?>


              </span>
            </a>

          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <!-- <span class="dropdown-item dropdown-header">
              <b>Setting</b>
            </span> -->
            <div class="dropdown-divider"></div>
          <?php foreach($v_menu_widget as $w) : ?>
            <a href="<?php echo(base_url($w["link"])); ?>" class="dropdown-item" style="width:'20% !important'">
              <i class="<?php echo($w["icon"]); ?> mr-2"></i>
              <?php echo($w["title"]); ?>
            </a>
            <div class="dropdown-divider"></div>
          <?php endforeach; ?>
          </div>
        </li>

    <?php endif; ?>

    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo(base_url("dashboard")); ?>" class="brand-link">

      <img
        src="<?php echo(base_url("asset/img/green_grass.png")); ?>"
        alt="AdminLTE Logo"
        class="brand-image img-circle elevation-3"
        style="opacity: .8;">
      <span class="brand-text font-weight-light">
        <?=$result?>
      </span>

    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <?php echo($v_menu); ?>

      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
