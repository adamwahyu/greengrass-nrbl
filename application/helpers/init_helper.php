<?php

function has_children($rows,$id) {
  foreach ($rows as $row) {
    if ($row['parent_id'] == $id)
      return true;
  }
  return false;
}

function build_menu($rows,$parent=0, $is_first = TRUE) {

  if($is_first) {
  	$result = '<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">';
  } else {
  	$result = "<ul class='nav nav-treeview'>";
  }

  foreach ($rows as $row) {
    if ($row['parent_id'] == $parent){


      if(has_children($rows,$row['id'])){
      	$result.= "
    		  <li class='nav-item has-treeview'>
    		  	<a id='".strtolower($row["title"])."' href='".base_url(strtolower($row['link']))."' class='nav-link'>
    		      <i class='".$row['icon']." nav-icon'></i>
    		      <p>
    		      {$row['title']}
    		      	<i class='right fas fa-angle-left'></i>
    		      </p>
    		    </a>
    		";
      	$result.= build_menu($rows,$row['id'], FALSE);
      } else {
      	$result.= "
    		  <li class='nav-item'>
    		  	<a id='".strtolower($row["title"])."' href='".base_url(strtolower($row['link']))."' class='nav-link'>
    		      <i class='".$row['icon']." nav-icon'></i>
    		      <p>{$row['title']}</p>
    		    </a>
    		";
    		$result.= build_menu($rows,$row['id'], FALSE);
      }

      $result.= "</li>";

    }
  }
  $result.= "</ul>";

  return $result;
}

function get_cluster($kode_unit) {
			$cluster = substr($kode_unit, 0, 2);
			if($cluster == "BS") {
				return "Blue Saphire";
			} elseif($cluster == "WD"){
        return "White Diamond";
      } elseif($cluster == "RB"){
        return "Ruby";
      } elseif($cluster == "RD"){
        return "Red Diamond";
      } elseif($cluster == "RS"){
        return "Red Saphire";
      } elseif($cluster == "JD"){
        return "Jade";
      } elseif($cluster == "SB"){
        return "Sapphire Boulevard";
      }
			return "Cluster tidak ditemukan";
    }



function rupiah($angka){

  $hasil_rupiah = number_format($angka,0,',','.');
  return $hasil_rupiah;

}


if(!function_exists("password_hashed")) {

  function password_hashed($plain_password) {
    return password_hash($plain_password, PASSWORD_DEFAULT);
  }
  
}

if(!function_exists("password_verified")) {

  function password_verified($plain, $hashed) {
    return password_verify($plain, $hashed);
  }

}

if(!function_exists("get_periode_spp")) {

  function get_periode_spp() {
    return [
      1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
    ];
  }

}

function title_case($string, $delimiters = array(" ", "-", ".", "'", "O'", "Mc"), $exceptions = array("and", "to", "of", "das", "dos", "I", "II", "III", "IV", "V", "VI"))
{
    /*
     * Exceptions in lower case are words you don't want converted
     * Exceptions all in upper case are any words you don't want converted to title case
     *   but should be converted to upper case, e.g.:
     *   king henry viii or king henry Viii should be King Henry VIII
     */
    $string = mb_convert_case($string, MB_CASE_TITLE, "UTF-8");
    foreach ($delimiters as $dlnr => $delimiter) {
        $words = explode($delimiter, $string);
        $newwords = array();
        foreach ($words as $wordnr => $word) {
            if (in_array(mb_strtoupper($word, "UTF-8"), $exceptions)) {
                // check exceptions list for any words that should be in upper case
                $word = mb_strtoupper($word, "UTF-8");
            } elseif (in_array(mb_strtolower($word, "UTF-8"), $exceptions)) {
                // check exceptions list for any words that should be in upper case
                $word = mb_strtolower($word, "UTF-8");
            } elseif (!in_array($word, $exceptions)) {
                // convert to uppercase (non-utf8 only)
                $word = ucfirst($word);
            }
            array_push($newwords, $word);
        }
        $string = join($delimiter, $newwords);
   }//foreach
   return $string;
}

if(!function_exists("image_base64")) {
  
  function image_base64($path) {
    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
    return $base64;
  }

}