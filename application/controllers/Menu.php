
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Menu_model",
			"GroupAccess_model"
		]);

	}
	public function index() {
		$this->data["v_content"] = "menu/index";
		$this->load->view('master/layouts/main', $this->data);
	}
	public function create() {

		$this->form_validation
			->set_rules("namamenu", "Nama Menu", "required|xss_clean");
		$this->form_validation
			->set_rules("url", "Url Menu", "required|xss_clean");
		$this->form_validation
			->set_rules("ikon", "Ikon Menu", "required|xss_clean");
		$this->form_validation
			->set_rules("deskripsi", "Deskripsi Menu", "trim|xss_clean");
		$this->form_validation
			->set_rules("indukmenu", "Induk Menu", "trim");
		$this->form_validation
			->set_rules("status", "Status Menu", "trim");
		$this->form_validation
			->set_rules("posisimenu", "Posisi Menu", "trim|xss_clean");

		if($this->form_validation->run() != FALSE) {

			$data = [
				"ref_parent_id_menu" => (empty($this->input->post("indukmenu"))) ? null : $this->input->post("indukmenu"),
				"ref_name_menu" => $this->input->post("namamenu"),
				"ref_desc_menu" => $this->input->post("deskripsi"),
				"ref_url_menu" => $this->input->post("url"),
				"ref_icon_menu" => $this->input->post("ikon"),
				"ref_position_menu" => $this->input->post("posisimenu"),
				"sts_active" => (int) $this->input->post("status"),
				"create_by" => $this->session->userdata("user")->user_id
			];

			$this->Menu_model->insert($data);
			$this->Menu_model->sync_menu();
			$this->session
				->set_flashdata("success", "Berhasil diinputkan");

			redirect('/menu');

		} else {
			if(validation_errors()) {
				$this->session->set_flashdata("error", validation_errors());
			}
			$menus = $this->Menu_model->get_all_status_active();

			$this->data["v_content"] = "menu/create";
			$this->data["v_data"] = (object) [
				"menus" => $menus,
			];
			$this->load->view('master/layouts/main', $this->data);	
		}
	}

	public function edit($id) {

		$this->form_validation
			->set_rules("id", "ID Menu", "required");
		$this->form_validation
			->set_rules("namamenu", "Nama Menu", "required|xss_clean");
		$this->form_validation
			->set_rules("url", "Url Menu", "required|xss_clean");
		$this->form_validation
			->set_rules("ikon", "Ikon Menu", "required|xss_clean");
		$this->form_validation
			->set_rules("deskripsi", "Deskripsi Menu", "trim|xss_clean");
		$this->form_validation
			->set_rules("indukmenu", "Induk Menu", "trim");
		$this->form_validation
			->set_rules("status", "Status Menu", "trim");
		$this->form_validation
			->set_rules("posisimenu", "Posisi Menu", "trim|xss_clean");

		if($this->form_validation->run() != FALSE) {

			$data = [
				"ref_parent_id_menu" => (empty($this->input->post("indukmenu"))) ? null : $this->input->post("indukmenu"),
				"ref_name_menu" => $this->input->post("namamenu"),
				"ref_desc_menu" => $this->input->post("deskripsi"),
				"ref_url_menu" => $this->input->post("url"),
				"ref_icon_menu" => $this->input->post("ikon"),
				"ref_position_menu" => $this->input->post("posisimenu"),
				"sts_active" => (int) $this->input->post("status"),
				"update_by" => $this->session->userdata("user")->user_id,
				"update_date" => date("Y-m-d H:i:s")
			];

			$this->Menu_model->update_by_id($this->input->post("id"), $data);
			$this->Menu_model->sync_menu();

			$this->session
				->set_flashdata("success", "Berhasil diperbarui");

			redirect('/menu');

		} else {
			if(validation_errors()) {
				$this->session->set_flashdata("error", validation_errors());
			}
			$menus = $this->Menu_model->get_all_status_active();
			$menu = $this->Menu_model->get_one_as_object_by_id($id);
			
			$this->data["v_content"] = "menu/edit";
			$this->data["v_data"] = (object) [
				"menus" => $menus,
				"menu" => $menu
			];
			$this->load->view('master/layouts/main', $this->data);	
		}
	}
	public function get_datatables() {

		$data_detail = $this->Menu_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start")
        );

        $total_data = $this->Menu_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {
        	
        	foreach($data_detail as $dd) {

        		$data[] = [
        			$dd->rownum,
        			$dd->ref_name_menu,
        			$dd->ref_parent_name_menu,
        			$dd->ref_url_menu,
        			$dd->ref_icon_menu,
        			$dd->ref_position_menu,
        			'
					<div class="btn-group">
	                    <a href="'.base_url("menu/edit/".$dd->ref_id_menu).'" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Edit Data">
	                    	<i class="fas fa-edit"> </i>
	                    </a>
	                    <button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus Data" onclick="onDestroy('.$dd->ref_id_menu.')">
	                    	<i class="fas fa-trash"> </i>
	                    </button>
                  	</div>
        			'
        		];
        	}
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;

	}

	public function ajax_destroy() {

		$this->form_validation
			->set_rules("id", "Id Menu", "required");

		if($this->form_validation->run() != FALSE) {
			$this->Menu_model->delete($this->input->post("id"));
			$this->GroupAccess_model->delete($this->input->post("id"), "ref_id_menu_gaccess");

			$response = [
				"error" => FALSE,
				"message" => "OK!"
			];
		} else {
			$response = [
				"error" => TRUE,
				"message" => "KO"
			];
		}

		$this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

}
