<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Auth_model",
			"User_model",
			"Unit_model"
		]);

	}
	public function login() {

		$this->form_validation
			->set_rules("kodeunit", "Kode Unit", "required");
		$this->form_validation
			->set_rules("password", "Password", "required");

		if($this->form_validation->run() != FALSE) {
			$user = $this->Auth_model->login($this->input->post("kodeunit"), $this->input->post("password"));

			if(!$user) {
				$this->session->set_flashdata("error", "Kombinasi Username dan Kata Sandi Salah");
				redirect(
					"auth/login"
				);
			}

			$this->session->set_userdata([
				"is_logged_in" => TRUE,
				"user" => $user
			]);

			redirect(
				"dashboard"
			);

		} else {
			if(validation_errors()) {
				$this->session->set_flashdata("error", validation_errors());
			}
			$this->load->view('public/login');
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		$this->session->set_flashdata("error", "Anda berhasil keluar akun");
		redirect("auth/login");
	}

	public function  register() {

		$this->form_validation
			->set_rules("nomerva_narobil", "Nomer VA Narobil", "required|xss_clean");
		// $this->form_validation
		// 	->set_rules("nomerva_bca", "Nomer BCA", "required|xss_clean");
		$this->form_validation
			->set_rules("kodeunit", "Kode Unit", "required|xss_clean");
		$this->form_validation
			->set_rules("namapemilik", "Nama Pemilik", "required|xss_clean");
		$this->form_validation
			->set_rules("email", "Email", "required|xss_clean");
		$this->form_validation
			->set_rules("msisdn", "Msisdn", "required|xss_clean");
		$this->form_validation
			->set_rules("password", "Password", "required|xss_clean");
		$this->form_validation
			->set_rules("retypepassword", "Retype Password", "required|xss_clean");

		if($this->form_validation->run() != FALSE) {

			try {

				$users = $this->User_model
					->get_all_by_column_as_object("kode_unit", $this->input->post("kodeunit"));

				if(count($users) > 0) {
					throw new Exception("Pendaftaran Akun Gagal, Data Sudah Ada");
				}

				$data_unit_update = [
					"email" => $this->input->post("email"),
					"msisdn" => $this->input->post("msisdn")
				];

				$data_user_insert = [
					"kode_unit" => $this->input->post("kodeunit"),
					"group_id" => 4,
					"user_name" => $this->input->post("kodeunit"),
					"user_password" => password_hashed($this->input->post("password")),
					"sts_active" => 1
				];

				$this->User_model
					->insert($data_user_insert);

				$this->Unit_model
					->update_by_column("kode_unit", $this->input->post("kodeunit"), $data_unit_update);

			} catch(\Exception $e) {
				$this->session->set_flashdata("error", $e->getMessage());
				redirect(
					$this->input->server("HTTP_REFERER")
				);
			}

			$this->session->set_flashdata("success", "Daftar akun berhasil");

			redirect('auth/login');

		} else {
			if(validation_errors()) {
				$this->session->set_flashdata("error", validation_errors());
			}
			$this->load->view('public/register');
		}

	}

	public function check_nomerva_narobil() {

		if($this->input->method() != "post") {

			$response = array(
				"error" => TRUE,
				"data" => "Method Not Allowed"
			);

		} else {

			$nomerva_narobil = $this->input->post("nomerva_narobil");
			$ref_unit = $this->Auth_model
				->check_va_if_exists($nomerva_narobil);

			if(count($ref_unit) > 0) {

				$response = array(
					"error" => FALSE,
					"message" => "Nomer VA Valid, silahkan melanjutkan pengisian",
					"data" => $ref_unit[0]
				);

			} else {

				$response = array(
					"error" => TRUE,
					"message" => "Nomer VA tidak ditemukan"
				);

			}

		}

		$this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

}
