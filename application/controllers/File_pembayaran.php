
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_pembayaran extends MY_Controller {

	private $column_excels = [
		"NO",
		"KODE_UNIT",
		"KODE_TRANSFER",
		"TGL_TRANSFER",
		"AMOUNT"
	];

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Batch_model",
            "Pembayaran_model",
            "Unit_model",
            "TPembayaran_model"
		]);
		$config['upload_path']          = "./upload/file_pembayaran/";
        $config['allowed_types']        = 'xlsx|xls';
        $config['max_size']             = 10000;
        $this->load->library('upload', $config);
	}

	public function index() {
		$this->data["v_content"] = "file_pembayaran/index";
		$this->load->view('master/layouts/main', $this->data);
	}

	public function get_datatables_header() {
		$data_detail = $this->Batch_model->get_datatables_header(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start")
        );

        $total_data = $this->Batch_model->get_datatables_header(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {
        	
        	foreach($data_detail as $dd) {
        		$data[] = [
        			$dd->rownum,
        			$dd->batch_code,
        			$dd->batch_date,
        			'
        				<div class="btn-group">
							<a href="'.base_url("upload/file_pembayaran/".$dd->batch_file_name).'" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="'.$dd->batch_file_name.'">
		                    	<i class="fas fa-download"> </i>
		                    </a>
						</div>
        			',
        			($dd->sts_active == 1) ? '
        				<small class="badge badge-success">
                            <!-- <i class="far fa-clock"></i> -->
                            Aktif
                        </small>
                    ' : '
                    	<small class="badge badge-danger">
                            <!-- <i class="far fa-clock"></i> -->
                            Tidak Aktif
                        </small>
                    ',
                    $dd->desc_metode_pembayaran,
        			$dd->jml_berhasil,
                    $dd->jml_gagal,
                    '
        				<div class="btn-group">
                            <button onclick="onTts(\''.$dd->batch_code.'\')" type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Buat Tanda Terima">
                                <i class="fas fa-file-pdf"> </i>
                            </button>
							<button onclick="onView(\''.$dd->batch_code.'\')" type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Lihat Data">
		                    	<i class="fas fa-eye"> </i>
		                    </button>
						</div>
        			',
        		];
        	}
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

    public function ajax_get_kode_unit_batch_detil_by_batch_code() {
        
        $this->form_validation
            ->set_rules("batch_code", "Batch Code", "required");

        if($this->form_validation->run() != FALSE) {

            $kode_unit = $this->Batch_model->get_one_batch_detil_by_batch_code($this->input->post("batch_code"));
            // dd($kode_unit);
            $data = array_unique(array_map(function($v) {
                return $v->kode_unit;
            }, $kode_unit));

            $response = [
                "error" => FALSE,
                "data" => array_values($data)
            ];

        } else {
            $response = [
                "error" => FALSE,
                "message" => validation_errors()
            ];
        }

        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;

    }
    public function create_ttp_pdf($batch_code, $kode_unit) {
        $data_header = $this->Batch_model->get_one_batch_header_by_batch_code($batch_code);
        $pembayaran = $this->Batch_model->get_pembayaran_by_batch_code($batch_code, $kode_unit);

        $kode_pembayaran = array_map(function($v) {
            return "'".$v->kode_pembayaran."'";
        }, $pembayaran);

        $unit = $this->Unit_model->get_one_all_by_column_as_object("kode_unit",$kode_unit);
        $data_detail = $this->Pembayaran_model->get_data_detail_pdf_batch_by_kode_unit_and_kode_pembayaran($kode_unit, implode(",", $kode_pembayaran));

        $p_tgl_pembayaran = explode(" ", $pembayaran[0]->tgl_pembayaran);

        $this->data["v_data"] = (object) [
            "unit" => $unit,
            "data_detail" => $data_detail,
            "data_header" => $data_header,
            "code_ttp" => $this->Pembayaran_model->get_ttp_code(),
            "tgl_pembayaran" => date_indo($p_tgl_pembayaran[0])." ".$p_tgl_pembayaran[1]
        ];

        $html = $this->load->view('master/template_pdf/template_ttp_f', $this->data, TRUE);
        // echo($html);
      
        $dompdf = new Dompdf\Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'potrait');
        $dompdf->render();
        $dompdf->stream($this->data["v_data"]->code_ttp.".pdf", [
            // "Attachment" => FALSE
        ]);
        exit(0);
    }

	public function get_datatables_detil() {
		$data_detail = $this->Batch_model->get_datatables_detil(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start")
        );

        $total_data = $this->Batch_model->get_datatables_detil(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {
        	
        	foreach($data_detail as $dd) {
                $sts = "";

                if($dd->batch_detil_status == "BERHASIL") {
                    $sts = '
                        <small class="badge badge-success">
                            <!-- <i class="far fa-clock"></i> -->
                            '.$dd->batch_detil_status.'
                        </small>
                    ';
                } else if($dd->batch_detil_status == "GAGAL") {
                    $sts = '
                        <small class="badge badge-danger">
                            <!-- <i class="far fa-clock"></i> -->
                            '.$dd->batch_detil_status.'
                        </small>
                    ';
                } else {
                    $sts = '
                        <small class="badge badge-danger">
                            <!-- <i class="far fa-clock"></i> -->
                            GAGAL
                        </small>
                    ';
                }

        		$data[] = [
        			$dd->rownum,
        			$dd->kode_unit,
        			$dd->transfer_code,
        			$dd->tgl_transfer,
        			rupiah($dd->amount),
                    $sts,
                    $dd->notes
        		];
        	}
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}
	private function check_column($column) {
		return hash("sha256", implode(" ", $this->column_excels)) === hash("sha256", implode(" ", $column));
	}
	public function upload() {

		if(empty($_FILES["file_pembayaran"]["tmp_name"])) {
            $this->form_validation
                ->set_rules("file_pembayaran", "File Pembayaran", "required");
        } else {
        	$this->form_validation
                ->set_rules("file_pembayaran", "File Pembayaran", "trim");
        }

        $this->form_validation
            ->set_rules("metode_pembayaran", "Metode Pembayaran", "required");

		if($this->form_validation->run() != FALSE && $this->upload->do_upload("file_pembayaran")) {

			$data_file = $this->upload->data();
			$batch_code = $this->Batch_model->get_generate_code();
            $new_name = $batch_code.$data_file["file_ext"];
            $old_name = $data_file["full_path"];
            $path = FCPATH."/upload/file_pembayaran/".$new_name;
            $batch_file_name = $new_name;

            rename($old_name, $path);

            if($data_file["file_ext"] == ".xlsx") {
            	$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            } else if($data_file["file_ext"] == ".xls") {
            	$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            }

            try {
            	$spreadsheet = $reader->load($path);
				$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
				if(!isset($sheetData[1])) {
					throw new \Exception("File tidak ada");
				}
				$sheedDataColumn = $sheetData[1];
				$data_columns = array_values($sheedDataColumn);
				
				if(!$this->check_column($data_columns)) {
					throw new \Exception("File tidak sesuai dengan template");
				}

				unset($sheetData[1]);
				$data_header = [
					"batch_code" => $batch_code,
					"batch_file_name" => $batch_file_name,
                    "id_metode_pembayaran" => $this->input->post("metode_pembayaran"),
                    "create_by" => $this->session->userdata("user")->user_id
				];
				$user_id = $this->session->userdata("user")->user_id;
                
				$data_detail = array_map(function($v) use($batch_code, $user_id) {
                    $amount = $v["E"];
                    $amount = trim($amount);
                    $amount = str_replace(".", "", $amount);
                    $amount = str_replace(",", "", $amount);
                    $amount = str_replace("-", "", $amount);

					return [
						"batch_code" => $batch_code,
						"kode_unit" => $v["B"],
						"tgl_transfer" => $v["D"],
						"amount" => $amount,
						"transfer_code" => $v["C"],
                        "create_by" => $user_id
					];
                    
				}, array_filter($sheetData, function($v) {
                    if(trim($v["B"]) == "" && trim($v["D"]) == "" && trim($v["E"]) == "" && trim($v["C"]) == "") {
                        return FALSE;
                    }
                    return TRUE;
                }));

				$this->Batch_model->insert_header($data_header);
				$this->Batch_model->insert_batch_detail($data_detail);
				
                $this->TPembayaran_model->update_by_column("batch_code", $batch_code, [
                    "create_by" => $this->session->userdata("user")->user_id
                ]);

				$this->session->set_flashdata("success", "Berhasil diinputkan");

				redirect(
					"file_pembayaran"
				);

            } catch(\Exception $e) {
            	unlink($path);
            	$this->session->set_flashdata("error", $e->getMessage());
            	redirect(
            		$this->input->server("HTTP_REFERER")
            	);
            }

		} else {
			if(validation_errors() || $this->upload->display_errors()) {
                $this->session->set_flashdata("error", validation_errors().$this->upload->display_errors());
            }
			$this->data["v_content"] = "file_pembayaran/upload";
            $this->data["v_data"] = (object) [
                "metode_pembayaran" => array_filter($this->Pembayaran_model->get_all_metode_pembayaran(), function($v) {
                    if($v->id_metode_pembayaran == 2 || $v->id_metode_pembayaran == 5 || $v->id_metode_pembayaran == 6) {
                        return true;
                    }
                    return false;
                })

            ];
			$this->load->view('master/layouts/main', $this->data);
		}
	}

    

}
    