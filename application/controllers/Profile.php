<?php

class Profile extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model([
			"User_model",
			"Unit_model"
		]);
	}
	public function change_password() {
		
		$this->form_validation
			->set_rules("passwordlama", "Password Lama", "required|callback_validate_old_password");
		$this->form_validation
			->set_rules("passwordbaru", "Password Baru", "required");
		$this->form_validation
			->set_rules("passwordbarukonfirm", "Password Baru Konfirm", "required|matches[passwordbaru]");

		if($this->form_validation->run() != FALSE) {

			$password_baru = $this->input->post("passwordbaru");
			$user = $this->session->userdata("user");

			$this->User_model->update($user->user_id, [
				"user_password" => password_hashed($password_baru),
				"updated_date" => date("Y-m-d h:i:s")
			]);

			$this->session->unset_userdata("is_logged_in");
			$this->session->unset_userdata("user");
			$this->session->set_flashdata("success", "Kata Sandi Berhasil Diganti !");
			
			redirect("auth/login");
		} else {

			if(validation_errors()) {
				$this->session->set_flashdata("error", validation_errors());
			}

			$this->data["v_content"] = "profile/change_password";
			$this->load->view("master/layouts/main", $this->data);
		}
	}

	public function edit() {

		$this->form_validation
			->set_rules("namapemilik", "Nama Pemilik", "required|xss_clean");
		$this->form_validation
			->set_rules("email", "Email", "required|xss_clean");
		$this->form_validation
			->set_rules("msisdn", "MSISDN", "required|xss_clean");

		if($this->form_validation->run() != FALSE) {

			$user = $this->session->userdata("user");
			$this->Unit_model->update_by_column("kode_unit", $user->kode_unit, [
				"nama_pemilik" => $this->input->post("namapemilik"),
				"email" => $this->input->post("email"),
				"msisdn" => $this->input->post("msisdn"),
				"updated_date" => date("Y-m-d h:i:s")
			]);

			$this->session->set_flashdata("success", "Berhasil diperbarui");
			redirect(
				"profile/edit"
			);
		} else {
			if(validation_errors()) {
				$this->session->set_flashdata("error", validation_errors());
			}
			$user = $this->User_model->get_profile_as_object_by_kodeunit($this->session->userdata("user")->kode_unit);
			$this->data["v_content"] = "profile/edit";
			$this->data["v_data"] = (object)[
				"user" => $user
			];
			$this->load->view("master/layouts/main", $this->data);
		}

	}

	public function validate_old_password($passwordlama) {
		$user = $this->session->userdata("user");
		if(password_verified($passwordlama, $user->user_password)) {
			return TRUE;
		} else {
			$this->form_validation->set_message('validate_old_password','Password lama tidak cocok !');
			return FALSE;
		}
	}

}