<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Billing extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Billing_model"
		]);
	}

	public function index() {
		$this->data["v_content"] = "billing/index";
		$this->load->view("master/layouts/main", $this->data);
	}

	public function ajax_get_billing_by_id() {
		$this->form_validation->set_rules("id", "Id", "required");

		if($this->form_validation->run() != FALSE) {
			
			$billing = $this->Billing_model->get_one_as_object_by_id($this->input->post("id"));

			$response = [
				"error" => FALSE,
				"message" => "OK",
				"data" => $billing
			];

		} else {
			$response = [
				"error" => TRUE,
				"message" => validation_errors()
			];
		}
		$this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}
	public function get_datatables() {
		$data_detail = $this->Billing_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start")
        );

        $total_data = $this->Billing_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {
        	
        	foreach($data_detail as $dd) {

        		$data[] = [
        			$dd->rownum,
        			$dd->kode_billing,
        			$dd->kode_unit,
        			$dd->nama_pemilik,
        			rupiah($dd->nilai_billing),
        			date_indo($dd->tgl_billing),
        			'
        				<div class="btn-group">
							<button type="button" onclick="onView(\''.$dd->id_billing.'\')" data-toggle="tooltip" data-placement="top" title="Lihat Data" class="btn btn-primary">
		                    	<i class="fas fa-eye"></i>
		                    </button>
						</div>
        			'
        		];

        	}

        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

}