<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Check_va_unit extends CI_Controller {

	public function __construct() {
		parent::__construct();
        $this->load->model([
            "Check_va_unit_model",
            "Unit_model"
        ]);
	}
	public function index() {
		$this->load->view('public/check_va_unit');
	}

    public function ajax_get_unit_by_kodeunit() {
        $this->form_validation
            ->set_rules("id_unit", "ID Unit", "required");

        if($this->form_validation->run() != FALSE) {
            $data = $this->Unit_model->get_all_custom_by_kodeunit($this->input->post("id_unit"));
            unset($data->nilai_tunggakan);
            $response = [
                "error" => FALSE,
                "message" => "OK!",
                "data" => $data
            ];

        } else {
            $response = [
                "error" => TRUE,
                "message" => "KO"
            ];
        }
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }

    public function get_datatables() {
        $data_detail = $this->Check_va_unit_model->get_datatables(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start")
        );

        $total_data = $this->Check_va_unit_model->get_datatables(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start"),
            TRUE
        );

        $data = [];
        if(!empty($data_detail)) {

            foreach($data_detail as $dd) {

                $data[] = [
                    $dd->kode_unit,
                    $dd->nama_pemilik,
                    $dd->unit_cluster,
                    '
                        <div class="btn-group">
                            <button type="button" class="btn bg-gradient-primary" data-toggle="tooltip" data-placement="top" title="Lihat Data" onclick="onDetail(\''.$dd->id_unit.'\')">
                                <i class="fas fa-eye"> </i>
                            </button>
                        </div>
                    '
                ];
            }
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }

}
