<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_warga extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Unit_warga_model"
		]);
	}
	public function index() {
		$this->data["v_content"] = "unit_warga/index";
		$this->load->view("master/layouts/main", $this->data);
	}

	public function get_datatables() {
		$data_detail = $this->Unit_warga_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start")
        );

        $total_data = $this->Unit_warga_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {

        	foreach($data_detail as $dd) {

                $indikator_warna = "";

                if((int) $dd->jmltagihan == 0) {
                    $indikator_warna = '
                        <div class="color-palette-set">
                          <div class="bg-success color-palette"><span></span></div>
                        </div>
                    ';
                } else if((int) $dd->jmltagihan <=2) {
                    $indikator_warna = '
                        <div class="color-palette-set">
                          <div class="bg-warning color-palette"><span></span></div>
                        </div>
                    ';
                } else {
                    $indikator_warna = '
                        <div class="color-palette-set">
                          <div class="bg-danger color-palette"><span></span></div>
                        </div>
                    ';
                }
        		$data[] = [
        			$dd->rownum,
        			$dd->kode_unit,
        			$dd->nama_pemilik,
                    $dd->luas_unit,
        			$dd->nomor_va_narobil,
        			$dd->sts_huni,
        			rupiah($dd->tarif),
                    $indikator_warna,
        			'
                        <div class="btn-group">
                            <button data-href="'.base_url("unit_warga/create_tunggakan_invoice_pdf/".$dd->id_unit).'" onclick="onCreateInvoice(this)" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Cetak Invoice">
                                <i class="fas fa-file-pdf"> </i>
                            </button>
                            <button type="button" onclick="onDetail(\''.$dd->id_unit.'\', \''.$dd->kode_unit.'\')" data-toggle="tooltip" data-placement="top" title="Lihat Data" class="btn btn-primary">
                                <i class="fas fa-eye"> </i>
                            </button>
                        </div>

        			'
        		];
        	}
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

    public function ajax_get_unit_by_kodeunit() {
        $this->form_validation
            ->set_rules("id_unit", "ID Unit", "required");

        if($this->form_validation->run() != FALSE) {
            $data = $this->Unit_warga_model->get_all_custom_by_kodeunit($this->input->post("id_unit"));
            $data->nilai_tunggakan = rupiah($data->nilai_tunggakan);
            $response = [
                "error" => FALSE,
                "message" => "OK!",
                "data" => $data
            ];
        } else {
            $response = [
                "error" => TRUE,
                "message" => "KO"
            ];
        }
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }

    public function create_tunggakan_invoice_pdf($id_unit) {

        $this->form_validation->set_rules("tanggal_jatuh_tempo", "Tempo", "required");

        if($this->form_validation->run() != FALSE) {

            $tanggal_cetak = date_indo(date("Y-m-d"))." ".date("h:i:s");

            $unit = $this->Unit_warga_model->get_all_custom_by_kodeunit($id_unit);
            $tunggakan = $this->Unit_warga_model->get_trans_kewajiban_tunggakan_by_kodeunit($unit->kode_unit);

            $html = $this->load->view("master/template_pdf/template_invoice_tunggakan", [
                "v_data" => (object) [
                    "unit" => $unit,
                    "tunggakan" => $tunggakan,
                    "tanggal_jatuh_tempo" => date_indo(date("Y-m-d", strtotime(str_replace("/", "-", $this->input->post("tanggal_jatuh_tempo"))))),
                    "base64_image" => image_base64(FCPATH."/asset/img/green_grass.png"),
                    "tanggal_cetak" => $tanggal_cetak
                ]
            ], TRUE);
            
            $dompdf = new Dompdf\Dompdf();
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'potrait');
            $dompdf->render();
            $dompdf->stream("IVC-".$unit->kode_unit."-".date("Ymdhis").".pdf", [
                // "Attachment" => FALSE
            ]);

            exit(0);
        } else {
            show_404();
        }
    }

    public function get_datatables_trans_kewajiban() {
        $data_detail = $this->Unit_warga_model->get_datatables_trans_kewajiban(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start")
        );

        $total_data = $this->Unit_warga_model->get_datatables_trans_kewajiban(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start"),
            TRUE
        );

        $data = [];
        if(!empty($data_detail)) {

            foreach($data_detail as $dd) {

                $data[] = [
                    $dd->rownum,
                    $dd->tahun,
                    $dd->bulan,
                    rupiah($dd->luas_unit),
                    rupiah($dd->tarif_permeter),
                    rupiah($dd->nilai_kewajiban),
                    rupiah($dd->nilai_pemenuhan),
                    ($dd->status_kewajiban == 'LUNAS') ? 
                        '<span class="badge badge-success">
                            '.$dd->status_kewajiban.'</span>'
                            :
                        '<span class="badge badge-danger">
                            '.$dd->status_kewajiban.'</span>'
                        ,
                ];
            }
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }

}
