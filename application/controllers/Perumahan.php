<?php

class Perumahan extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Perumahan_model"
		]);
	}
	public function index() {

		$this->data["v_content"] = "perumahan/index";
		$this->load->view("master/layouts/main", $this->data);
	}

    // public function create() {

    //     $this->form_validation
    //         ->set_rules("nama_perumahan", "Nama perumahan", "required");

    //     if($this->form_validation->run() != FALSE) {
    //         $data_insert = [
    //             "nama_perumahan" => $this->input->post("nama_perumahan"),
    //             "sts_active" => 1,
    //             "create_by" => $this->session->userdata("user")->user_id
    //         ];

    //         $this->Perumahan_model->insert($data_insert);

    //         $this->session->set_flashdata("success", "Berhasil diinputkan");
    //         redirect('/perumahan');
    //     } else {
    //         if(validation_errors()) {
    //             $this->session->set_flashdata("error", validation_errors());
    //         }
    //         $this->data["v_content"] = "perumahan/create";
    //         $this->load->view("master/layouts/main", $this->data);
    //     }
    // }

	public function edit($id) {

        $this->form_validation
            ->set_rules("nama_perumahan", "Nama perumahan", "required");

		if($this->form_validation->run() != FALSE) {
            
            $data = [
                "nama_perumahan" => $this->input->post("nama_perumahan"),
                "update_by" => $this->session->userdata("user")->user_id,
                "updated_date" => date("Y-m-d H:i:s")
            ];


            $this->Perumahan_model->update(
                $this->input->post("id_perumahan"),
                $data
            );
            $this->session->set_flashdata("success", "Berhasil diperbarui");
            redirect('/perumahan');
        } else {
            if(validation_errors()) {
                $this->session->set_flashdata("error", validation_errors());
            }
			$perumahan = $this->Perumahan_model->get_one_all_by_column_as_object("id_perumahan", $id);

			$this->data["v_content"] = "perumahan/edit";
			$this->data["v_data"] = (object) [
				"perumahan" => $perumahan
			];
			$this->load->view("master/layouts/main", $this->data);
		}
	}
	public function get_datatables() {

		$data_detail = $this->Perumahan_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start")
        );

        $total_data = $this->Perumahan_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {
        	
        	foreach($data_detail as $dd) {
                
                $sts_html = "";
                // if($dd->status == "Y") {
                //     $sts_html .= '
                //         <small onclick="onDeactive(\''.$dd->id_perumahan.'\')" class="badge badge-success" style="cursor:pointer;">
                //            <!-- <i class="far fa-clock"></i> -->
                //             Aktif
                //         </small>
                //     ';
                // } else {
                //     $sts_html .= '
                //         <small onclick="onActive(\''.$dd->id_perumahan.'\')" class="badge badge-danger" style="cursor:pointer;">
                //             <!-- <i class="far fa-clock"></i> -->
                //             Tidak Aktif
                //         </small>
                //     ';
                // }
                

        		$data[] = [
        			$dd->rownum,
        			$dd->nama_perumahan,
        			'
						<div class="btn-group">
                            <a href="'.base_url("perumahan/edit/".$dd->id_perumahan).'" data-toggle="tooltip" data-placement="top" title="Edit Data" class="btn btn-success">
                                <i class="fas fa-edit"> </i>
                            </a>
                        </div>
        			'
        		];

        	}

        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

    public function ajax_destroy() {
        
        $this->form_validation->set_rules("id", "Id", "required");

        if($this->form_validation->run() != FALSE) {

            $this->Perumahan_model->delete($this->input->post("id"));
            $response = [
                "error" => FALSE,
                "message" => "Berhasil"
            ];

        } else {
            
            $response = [
                "error" => TRUE,
                "message" => validation_errors()
            ];

        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }

    public function ajax_deactive() {
        
        $this->form_validation->set_rules("id", "Id", "required");

        if($this->form_validation->run() != FALSE) {

            $this->Perumahan_model->update(
                $this->input->post("id"),
                [
                    "sts_active" => 0
                ]
            );
            
            $response = [
                "error" => FALSE,
                "message" => "Berhasil"
            ];

        } else {
            
            $response = [
                "error" => TRUE,
                "message" => validation_errors()
            ];
            
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }

    public function ajax_active() {
        
        $this->form_validation->set_rules("id", "Id", "required");

        if($this->form_validation->run() != FALSE) {

            $this->Perumahan_model->update(
                $this->input->post("id"),
                [
                    "sts_active" => 1
                ]
            );

            $response = [
                "error" => FALSE,
                "message" => "Berhasil"
            ];

        } else {
            
            $response = [
                "error" => TRUE,
                "message" => validation_errors()
            ];
            
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }

}