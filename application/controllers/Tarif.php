<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarif extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Tarif_model"
		]);

		// dd($this->session->userdata("user"));
	}
	public function index() {
		$this->data["v_content"] = "tarif/index";
		$this->load->view('master/layouts/main', $this->data);
	}

	public function create() {

		$this->form_validation
			->set_rules("tarif", "Tarif Permeter", "required|xss_clean");
		$this->form_validation
			->set_rules("status", "Status Tarif", "trim");

		if($this->form_validation->run() != FALSE) {

			$data = [
				"tarif_permeter" => $this->input->post("tarif"),
				"create_by" => $this->session->userdata("user")->user_id
			];
			$this->Tarif_model->update_disabled();
			$this->Tarif_model->insert($data);
			$this->session
				->set_flashdata("success", "Berhasil diinputkan");
			redirect('/tarif');
		} else {
			if(validation_errors()) {
				$this->session->set_flashdata("error", validation_errors());
			}
			$tarifs = $this->Tarif_model->get_all_status_active();


			$this->data["v_content"] = "tarif/create";
			$this->data["v_data"] = (object) [
				"tarifs" => $tarifs,
			];
			$this->load->view('master/layouts/main', $this->data);
		}
	}

	public function get_datatables() {

		$data_detail = $this->Tarif_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start")
        );

        $total_data = $this->Tarif_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {

        	foreach($data_detail as $dd) {

        		$data[] = [
        			$dd->rownum,
        			$dd->tarif_permeter
        		];
        	}
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;

	}
}
