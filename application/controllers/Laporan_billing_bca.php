<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_billing_bca extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Laporan_billing_bca_model"
		]);
	}

	public function index() {
		$this->data["v_content"] = "laporan/billing_bca/index";
        $this->data["v_data"] = (object)[
            "bulan" => $this->Laporan_billing_bca_model->dynamic_month()
        ];
		$this->load->view("master/layouts/main", $this->data);
	}

	public function get_datatables() {

		$data_detail = $this->Laporan_billing_bca_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
            $this->input->get("start")
        );

        $total_data = $this->Laporan_billing_bca_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {
        	
        	foreach($data_detail as $dd) {

        		$data[] = [
        			$dd->rownum,
        			$dd->nomor_va_bca,
        			$dd->kode_unit,
                    rupiah($dd->tunggakan),
                    rupiah($dd->b4),
                    rupiah($dd->b3),
                    rupiah($dd->b2),
        			rupiah($dd->b1)
        		];
        	}
        }

        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

    public function export_excel() {

            $data = $this->Laporan_billing_bca_model->get_billing_bca_excel();
            $bulan = $this->Laporan_billing_bca_model->dynamic_month(); 
            $spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();

            $spreadsheet->getActiveSheet()->setCellValue('A1', 'NO');
            $spreadsheet->getActiveSheet()->setCellValue('B1', 'NO VA');
            $spreadsheet->getActiveSheet()->setCellValue('C1', 'KODE UNIT');
            $spreadsheet->getActiveSheet()->setCellValue('D1', 'TUNGGAKAN');
            $a = 'E';
            foreach ($bulan as $key) {
                 $spreadsheet->getActiveSheet()->setCellValue($a.'1', $key->bulan);
                $a++;
            }
            $i = 2;
            foreach($data as $d) {
                $spreadsheet->getActiveSheet()->setCellValue('A'.$i, $d->rownum);
                $spreadsheet->getActiveSheet()->setCellValue('B'.$i, $d->nomor_va_bca);
                $spreadsheet->getActiveSheet()->setCellValue('C'.$i, $d->kode_unit);
                $spreadsheet->getActiveSheet()->setCellValue('D'.$i, $d->tunggakan);
                $spreadsheet->getActiveSheet()->setCellValue('E'.$i, $d->b4);
                $spreadsheet->getActiveSheet()->setCellValue('F'.$i, $d->b3);
                $spreadsheet->getActiveSheet()->setCellValue('G'.$i, $d->b2);
                $spreadsheet->getActiveSheet()->setCellValue('H'.$i, $d->b1);
                $i++;
            }

            foreach(range('A','H') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
            }
            $styleJudul = [
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
            ];
            $spreadsheet->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleJudul);
            $spreadsheet->getActiveSheet()->getStyle('D2:H'.$i)->getNumberFormat()->setFormatCode('#,##0');

            $writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
            $changedate = date("dmY");
            $txt_filename = "Lap-Billing-BCA-".$changedate;
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$txt_filename.'".xlsx');
            header('Cache-Control: max-age=0');
            ob_end_clean();
            return $writer->save("php://output");
    }

}