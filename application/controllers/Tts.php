<?php

class Tts extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Tts_model",
			"Unit_model"
		]);
	}

	public function index() {
		$this->data["v_content"] = "tts/index";
		$this->load->view("master/layouts/main", $this->data);
	}

	public function create_tts_pdf($tth_code) {
		$data_header = $this->Tts_model->get_one_header_by_tth_code($tth_code);
		$unit = $this->Unit_model->get_one_all_by_column_as_object("kode_unit",$data_header->kode_unit);
		$data_detail = $this->Tts_model->get_detail_by_tth_code($tth_code);
		$this->data["v_data"] = (object) [
			"unit" => $unit,
			"data_header" => $data_header,
			"data_detail" => $data_detail
		];
		$html = $this->load->view('master/template_pdf/template_tts', $this->data, TRUE);
        $dompdf = new Dompdf\Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'potrait');
        $dompdf->render();
        $dompdf->stream($this->data["v_data"]->data_header->tth_code.".pdf", [
            // "Attachment" => FALSE
        ]);
        exit(0);
	}

	public function get_detail_by_tth_code() {
		$this->form_validation->set_rules("tth_code", "TTH Code", "required");

		if($this->form_validation->run() != FALSE) {
			$data = $this->Tts_model->get_detail_by_tth_code($this->input->post("tth_code"));
			$response = [
				"error" => FALSE,
				"data" => $data
			];
		} else {
			$response = [
				"error" => TRUE,
				"message" => validation_errors()
			];
		}

		$this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

	public function create() {
		$this->form_validation->set_rules("kodeunit", "Kode Unit", "required");
		$this->form_validation->set_rules("tanggal_pembayaran", "Tanggal Pembayaran", "required");
		$this->form_validation->set_rules("metode_pembayaran", "Metode Pembayaran", "required");
		$this->form_validation->set_rules("total", "Total", "required");
		$this->form_validation->set_rules("id_kewajiban[]", "ID Kewajiban", "required");

		if($this->form_validation->run() != FALSE) {

			// dd($this->input->post());

			$tth_code = $this->Tts_model->get_generate_code();
			$user_id = $this->session->userdata("user")->user_id;
			
			$data_header = [
				"tth_code" => $tth_code,
				"kode_unit" => $this->input->post("kodeunit"),
				"id_metode_pembayaran" => $this->input->post("metode_pembayaran"),
				"tanggal_pembayaran" => date("m-d-Y", strtotime(str_replace("/", "-", $this->input->post('tanggal_pembayaran')))),
				"total" => str_replace(".", "", $this->input->post("total")),
				"create_by" => $user_id
			];

			$d_detil = $this->Tts_model->get_t_kewajiban_by_batch_id(implode(",", $this->input->post("id_kewajiban")));
			

			$data_detil = array_map(function($v) use($tth_code, $user_id) {
				$kd_bulan = intval($v->bulan);
				return [
					"tth_code" => $tth_code,
					"no_invoice" => "IVC.{$v->kode_unit}.{$kd_bulan}.{$v->tahun}",
					"id_kewajiban" => $v->id_kewajiban,
					"bulan" => $v->bulan,
					"tahun" => $v->tahun,
					"jenis" => "Standard",
					"tarif" => $v->tarif_permeter,
					"denda" => 0,
					"total" => $v->nilai_kewajiban,
					"create_by" => $user_id
				];
			}, $d_detil);

			$this->Tts_model->insert_header($data_header);
			$this->Tts_model->insert_batch_detail($data_detil);
			$this->session->set_flashdata("success", "Berhasil diinputkan");
			redirect(
				"tts"
			);
		} else {
			if(validation_errors()) {
				$this->session->set_flashdata("error", validation_errors());
			}
			$this->data["v_data"] = (object) [
				"metode_pembayaran" => $this->Tts_model->get_metode_pembayaran(),
				"unit" => $this->Unit_model->get_all_cs()
			];
			$this->data["v_content"] = "tts/create";
			$this->load->view("master/layouts/main", $this->data);
		}
		
	}

	public function get_list_t_kewajiban_open_by_kodeunit() {

		$this->form_validation->set_rules("kodeunit", "Kode Unit", "required");

		if($this->form_validation->run() != FALSE) {
			$data = $this->Tts_model->get_list_t_kewajiban_open_by_kodeunit($this->input->post("kodeunit"));
			$response = [
				"error" => FALSE,
				"data" => $data
			];
		} else {
			$response = [
				"error" => TRUE,
				"message" => validation_errors()
			];
		}

		$this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

	public function get_datatables() {
		$data_detail = $this->Tts_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start")
        );

        $total_data = $this->Tts_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {

        	foreach($data_detail as $dd) {

        		$data[] = [
        			$dd->rownum,
        			$dd->tth_code,
        			$dd->kode_unit,
        			$dd->desc_metode_pembayaran,
        			date_indo($dd->tanggal_pembayaran),
        			rupiah($dd->total),
        			'
        				<div class="btn-group">
                            <a href="'.base_url("tts/create_tts_pdf/".$dd->tth_code).'" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Buat Tanda Terima">
                                <i class="fas fa-file-pdf"></i>
                            </a>
                            <button type="button"data-toggle="tooltip" onclick="onDetail(\''.$dd->tth_code.'\')" data-placement="top" title="Lihat Data" class="btn btn-primary">
                                <i class="fas fa-eye"> </i>
                            </button>
                        </div>
        			'
        		];
        	}
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}
}