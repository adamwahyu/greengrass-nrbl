<?php

class Resetpass extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Resetpass_model",
			"User_model"
		]);
	}
	public function index() {

		$this->data["v_content"] = "resetpass/index";
		$this->load->view("master/layouts/main", $this->data);
	}

	public function ajax_reset_password() {

		$this->form_validation
			->set_rules("kode_unit", "Kode Unit", "required");

		if($this->form_validation->run() != FALSE) {

			$this->User_model->update_by_column("user_name", $this->input->post("kode_unit"), [
				"user_password" => password_hashed($this->input->post("kode_unit")),
				"updated_date" => date("Y-m-d H:i:s"),
				"update_by" => $this->session->userdata("user")->user_id
			]);

			$response = [
				"error" => FALSE,
				"message" => "",
				"params" => $this->input->post()
			];

		} else {
			
			$response = [
				"error" => FALSE,
				"message" => validation_errors()
			];

		}

		$this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

	public function get_datatables() {
		$data_detail = $this->Resetpass_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start")
        );

        $total_data = $this->Resetpass_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {
        	
        	foreach($data_detail as $dd) {

        		$data[] = [
        			$dd->rownum,
        			$dd->kode_unit,
        			$dd->nama_pemilik,
        			$dd->email,
        			$dd->msisdn,
        			$dd->nomor_va_narobil,
        			$dd->nomor_va_bca,
        			$dd->luas_unit,
        			'
        				<div class="btn-group">
							<button type="button" onclick="ConfirmResetPass(\''.$dd->kode_unit.'\')" class="btn btn-success">
		                    	<i class="fas fa fa-key"> </i>
		                    </button>
						</div>
        			'
        		];
        	}
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

}