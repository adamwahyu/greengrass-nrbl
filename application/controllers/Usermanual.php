<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * * @property Usermanual Optional description
 */

class Usermanual extends MY_Controller {
    protected $data;

    // public function __construct()
    // {
    //     parent::__construct();
    //     $this->data['tahun'] = $this->session->userdata('tahun');
    // }

    public function index() {
  		$this->data["v_content"] = "user_manual/index";
  		$this->load->view("master/layouts/main", $this->data);
  	}
}
