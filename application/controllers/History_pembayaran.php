<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History_pembayaran extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"History_pembayaran_model"
		]);
	}

	public function index() {
		$this->data["v_content"] = "history_pembayaran/index";
		$this->load->view("master/layouts/main", $this->data);
	}

    public function load_datatables() {
        $this->data['startdate'] = $this->input->post("startdate");
        $this->data['enddate'] = $this->input->post("enddate");
        $this->load->view("master/pages/history_pembayaran/datatables", $this->data);
    }


	public function get_datatables() {

		$data_detail = $this->History_pembayaran_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
            $this->input->get("startdate"),
            $this->input->get("enddate")
        );

        $total_data = $this->History_pembayaran_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
            $this->input->get("startdate"),
            $this->input->get("enddate"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {
        	
        	foreach($data_detail as $dd) {

        		$data[] = [
        			$dd->rownum,
        			$dd->kode_pembayaran,
        			$dd->kode_unit,
        			$dd->tgl_pembayaran,
        			rupiah($dd->jml_pembayaran),
        			$dd->desc_metode_pembayaran,
        			'
        				<div class="btn-group">
							<button type="button" onclick="onView(\''.$dd->id_pembayaran.'\')" data-toggle="tooltip" data-placement="top" title="Lihat Data" class="btn btn-primary">
		                    	<i class="fas fa-eye"></i>
		                    </button>
						</div>
        			'
        		];

        	}

        }

        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

    public function ajax_get_detail_by_id() {
        $this->form_validation->set_rules("id_pembayaran", "id_pembayaran", "required");

        if($this->form_validation->run() != FALSE) {
            
            $etail = $this->History_pembayaran_model->get_one_as_object_by_id($this->input->post("id_pembayaran"));

            $response = [
                "error" => FALSE,
                "message" => "OK",
                "data" => $etail
            ];

        } else {
            $response = [
                "error" => TRUE,
                "message" => validation_errors()
            ];
        }
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }


    public function export_excel() {
        
            $startdate = $this->input->get("startdate");
            $enddate = $this->input->get("enddate");

            $data = $this->History_pembayaran_model->get_history_excel($startdate,$enddate); 
            $spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();

            $spreadsheet->getActiveSheet()->setCellValue('A1', 'No');
            $spreadsheet->getActiveSheet()->setCellValue('B1', 'Kode Pembayaran');
            $spreadsheet->getActiveSheet()->setCellValue('C1', 'Kode Unit');
            $spreadsheet->getActiveSheet()->setCellValue('D1', 'Nomor VA Narobil');
            $spreadsheet->getActiveSheet()->setCellValue('E1', 'Nomor VA BCA');
            $spreadsheet->getActiveSheet()->setCellValue('F1', 'Tanggal Pembayaran');
            $spreadsheet->getActiveSheet()->setCellValue('G1', 'Status Pembayaran');
            $spreadsheet->getActiveSheet()->setCellValue('H1', 'Metode Pembayaran');
            $spreadsheet->getActiveSheet()->setCellValue('I1', 'Notes');
            $spreadsheet->getActiveSheet()->setCellValue('J1', 'Jumlah Pembayaran');

            $i = 2;
            $total = 0;
            $test = [];
            foreach($data as $d) {
                $spreadsheet->getActiveSheet()->setCellValue('A'.$i, $d->rownum);
                $check = is_numeric($d->kode_pembayaran);
                if (is_numeric($d->kode_pembayaran)) {
                   $spreadsheet->getActiveSheet()->setCellValue('B'.$i, '=TEXT('.$d->kode_pembayaran.',"0")');
                }else{
                    $spreadsheet->getActiveSheet()->setCellValue('B'.$i, $d->kode_pembayaran);
                }
                
                $spreadsheet->getActiveSheet()->setCellValue('C'.$i, $d->kode_unit);
                $spreadsheet->getActiveSheet()->setCellValue('D'.$i, '=TEXT('.$d->nomor_va_narobil.',"0")');
                $spreadsheet->getActiveSheet()->setCellValue('E'.$i, $d->nomor_va_bca);
                $spreadsheet->getActiveSheet()->setCellValue('F'.$i, $d->tgl_pembayaran);
                $spreadsheet->getActiveSheet()->setCellValue('G'.$i, $d->status_pembayaran);
                $spreadsheet->getActiveSheet()->setCellValue('H'.$i, $d->desc_metode_pembayaran);
                $spreadsheet->getActiveSheet()->setCellValue('I'.$i, $d->notes);
                $spreadsheet->getActiveSheet()->setCellValue('J'.$i, $d->jml_pembayaran);
                $total += $d->jml_pembayaran;
                array_push($test,$check);
                $i++;
            }
            // print_r($test);die;
            $styleTotal = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
            ];

            $spreadsheet->getActiveSheet()->getStyle("A".$i.":I".$i)->applyFromArray($styleTotal);
            $spreadsheet->getActiveSheet()->getStyle('J2:J'.$i)->getNumberFormat()->setFormatCode('#,##0');
            $spreadsheet->getActiveSheet()->getStyle('B2:B'.$i)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
            $spreadsheet->getActiveSheet()->getStyle('D2:D'.$i)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
            $spreadsheet->getActiveSheet()->getStyle('B2:B'.$i)->setQuotePrefix(true);
            $spreadsheet->getActiveSheet()->getStyle('D2:D'.$i)->setQuotePrefix(true);
            
            $spreadsheet->getActiveSheet()->mergeCells("A".$i.":I".$i);
            $spreadsheet->getActiveSheet()->setCellValue('A'.$i, 'TOTAL PEMBAYARAN');
            $spreadsheet->getActiveSheet()->setCellValue('J'.$i, $total);

            foreach(range('A','J') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
            }

            $writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
            $changedate = date("dmY",strtotime($startdate));
            $changedate .= "-".date("dmY",strtotime($enddate));
            $txt_filename = "Lap-History-Pembayaran-".$changedate;
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$txt_filename.'".xlsx');
            header('Cache-Control: max-age=0');
            ob_end_clean();
            return $writer->save("php://output");
    }

}