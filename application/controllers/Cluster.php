<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cluster extends MY_Controller {

	public function __construct() {
		parent::__construct();
        $this->load->model([
            "Cluster_model"
        ]);
	}
	public function index() {
        $this->data["v_content"] = "cluster/index";
        $this->load->view("master/layouts/main", $this->data);
	}

    public function create() {
        $this->form_validation
            ->set_rules("nama_cluster", "Nama Cluster", "required|xss_clean");
        $this->form_validation
            ->set_rules("id_perumahan", "ID Perumahan", "required");
        $this->form_validation
            ->set_rules("status", "Status", "required");

        if($this->form_validation->run() != FALSE) {

            $data = [
                "id_perumahan" => $this->input->post("id_perumahan"),
                "nama_cluster" => $this->input->post("nama_cluster"),
                "sts_active" => $this->input->post("status")
            ];

            $this->Cluster_model->insert($data);

            $this->session
                ->set_flashdata("success", "Berhasil diinputkan");

            redirect('/cluster');

        } else {
            if(validation_errors()) {
                $this->session->set_flashdata("error", validation_errors());
            }
            $perumahan = $this->Cluster_model->get_perumahan_all();
            $this->data["v_content"] = "cluster/create";
            $this->data["v_data"] = (object) [
                "perumahan" => $perumahan,
            ];

            $this->load->view('master/layouts/main', $this->data);  
        }

    }

    public function edit($id) {
        $this->form_validation
            ->set_rules("id_cluster", "ID Cluster", "required");
        $this->form_validation
            ->set_rules("nama_cluster", "Nama Cluster", "required|xss_clean");
        $this->form_validation
            ->set_rules("id_perumahan", "ID Perumahan", "required");
        $this->form_validation
            ->set_rules("status", "Status", "required");

        if($this->form_validation->run() != FALSE) {

            $data = [
                "id_perumahan" => $this->input->post("id_perumahan"),
                "nama_cluster" => $this->input->post("nama_cluster"),
                "sts_active" => $this->input->post("status")
            ];

            $this->Cluster_model->update_by_id(
                $this->input->post("id_cluster"),
                $data
            );

            $this->session
                ->set_flashdata("success", "Berhasil diedit");

            redirect('/cluster');

        } else {
            
            if(validation_errors()) {
                $this->session->set_flashdata("error", validation_errors());
            }
            
            $perumahan = $this->Cluster_model->get_perumahan_all();
            $cluster = $this->Cluster_model->get_one_all_by_column_as_object("id_cluster", $id);

            $this->data["v_content"] = "cluster/edit";
            $this->data["v_data"] = (object) [
                "perumahan" => $perumahan,
                "data" => $cluster
            ];

            $this->load->view('master/layouts/main', $this->data);  
        }
    }

    public function get_datatables() {
        $data_detail = $this->Cluster_model->get_datatables(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start")
        );

        $total_data = $this->Cluster_model->get_datatables(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start"),
            TRUE
        );

        $data = [];
        if(!empty($data_detail)) {

            foreach($data_detail as $dd) {

                $data[] = [
                    $dd->rownum,
                    $dd->nama_perumahan,
                    $dd->nama_cluster,
                    '
                        <div class="btn-group">
                            <a href="'.base_url("cluster/edit/".$dd->id_cluster).'" data-toggle="tooltip" data-placement="top" title="Edit Data" class="btn btn-primary">
                                <i class="fas fa-edit"></i>
                            </a>
                        </div>
                    '
                ];
            }
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }

}
