
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Group_model",
			"GroupAccess_model"
		]);
	}
	public function index() {
		$this->data["v_content"] = "group/index";
		$this->load->view('master/layouts/main', $this->data);
	}
	
	public function manage($id) {

		$this->form_validation
			->set_rules("id[]", "ID Menu", "required");

		if($this->form_validation->run() != FALSE) {
			$data_update = [];
			foreach($this->input->post("id") as $id) {
				if(!is_null($this->input->post("status_".$id))) {
					$data_update[] = [
						"ref_id_gaccess" => $id,
						"sts_active" => 1,
						"update_by" => $this->session->userdata("user")->user_id,
						"update_date" => date("Y-m-d H:i:s")
					];
				} else {
					$data_update[] = [
						"ref_id_gaccess" => $id,
						"sts_active" => 0,
						"update_by" => $this->session->userdata("user")->user_id,
						"update_date" => date("Y-m-d H:i:s")
					];
				}	
			}

			$this->GroupAccess_model->update_batch($data_update, "ref_id_gaccess");
			$this->session->set_flashdata("success", "Berhasil diperbarui");
			redirect('/group');
		} else {
			$menus = $this->GroupAccess_model->get_menu_as_object_by_group_id($id);
			$this->data["v_content"] = "group/manage";
			$this->data["v_data"] = (object)[
				"menus" => $menus
			];
			$this->load->view("master/layouts/main", $this->data);
		}
	}

	public function get_datatables() {
		$data_detail = $this->Group_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start")
        );

        $total_data = $this->Group_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {
        	
        	foreach($data_detail as $dd) {

        		$data[] = [
        			$dd->rownum,
        			$dd->group_name,
        			'
						<div class="btn-group">
							<a href="'.base_url("group/manage/".$dd->group_id).'" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Edit Data">
		                    	<i class="fas fa-edit"> </i>
		                    </a>
						</div>
        			'
        		];
        	}
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

}
