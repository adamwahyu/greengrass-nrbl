<?php

class User extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model([
			"User_model",
			"Unit_model",
            "Group_model"
		]);
	}
	public function index() {

		$this->data["v_content"] = "user/index";
		$this->load->view("master/layouts/main", $this->data);
	}

    public function create() {

        if($this->input->post("group_id") === '4') {
            $this->form_validation
                ->set_rules("kode_unit", "Kode Unit", "required");
        }

        $this->form_validation
            ->set_rules("group_id", "Group ID", "required");
        $this->form_validation
            ->set_rules("username", "Username", "required");
        $this->form_validation
            ->set_rules("password", "Password", "required");

        if($this->form_validation->run() != FALSE) {
            $pass_hash = password_hashed($this->input->post("password"));

            $data_insert = [
                "kode_unit" => $this->input->post("kode_unit"),
                "user_name" => $this->input->post("username"),
                "user_password" => $pass_hash,
                "group_id" => $this->input->post("group_id"),
                "create_by" => $this->session->userdata("user")->user_id
            ];

            $this->User_model->insert($data_insert);

            $this->session->set_flashdata("success", "Berhasil diinputkan");
            redirect('/user');
        } else {
            if(validation_errors()) {
                $this->session->set_flashdata("error", validation_errors());
            }
            $unit = $this->Unit_model->get_all_cs();
            $group = $this->Group_model->get_all();

            $this->data["v_content"] = "user/create";
            $this->data["v_data"] = (object) [
                "unit" => $unit,
                "group" => $group
            ];
            $this->load->view("master/layouts/main", $this->data);
        }
    }

	public function edit($id) {

        if($this->input->post("group_id") === '4') {
            $this->form_validation
                ->set_rules("kode_unit", "Kode Unit", "required");
        }

        $this->form_validation
            ->set_rules("user_id", "User ID", "required");

        $this->form_validation
            ->set_rules("group_id", "Group ID", "required");
        $this->form_validation
            ->set_rules("username", "Username", "required");
        $this->form_validation
            ->set_rules("password", "Password", "trim");

		if($this->form_validation->run() != FALSE) {
            
            $data = [
                "group_id" => $this->input->post("group_id"),
                "user_name" => $this->input->post("username"),
                "kode_unit" => $this->input->post("kode_unit"),
                "update_by" => $this->session->userdata("user")->user_id,
                "updated_date" => date("Y-m-d H:i:s")
            ];

            if($this->input->post("password") != "") {
                $data["user_password"] = password_hashed($this->input->post("password"));
            }

            $this->User_model->update(
                $this->input->post("user_id"),
                $data
            );
            $this->session->set_flashdata("success", "Berhasil diperbarui");
            redirect('/user');
        } else {
            if(validation_errors()) {
                $this->session->set_flashdata("error", validation_errors());
            }
			$user = $this->User_model->get_one_all_by_column_as_object("user_id", $id);
			$unit = $this->Unit_model->get_all_cs($id);
            $group = $this->Group_model->get_all();

			$this->data["v_content"] = "user/edit";
			$this->data["v_data"] = (object) [
				"user" => $user,
				"unit" => $unit,
                "group" => $group
			];
			$this->load->view("master/layouts/main", $this->data);
		}
	}
	public function get_datatables() {

		$data_detail = $this->User_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start")
        );

        $total_data = $this->User_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {
        	
        	foreach($data_detail as $dd) {
                
                $sts_html = "";
                if($dd->status == "Y") {
                    $sts_html .= '
                        <small onclick="onDeactive(\''.$dd->user_id.'\')" class="badge badge-success" style="cursor:pointer;">
                           <!-- <i class="far fa-clock"></i> -->
                            Aktif
                        </small>
                    ';
                } else {
                    $sts_html .= '
                        <small onclick="onActive(\''.$dd->user_id.'\')" class="badge badge-danger" style="cursor:pointer;">
                            <!-- <i class="far fa-clock"></i> -->
                            Tidak Aktif
                        </small>
                    ';
                }
                

        		$data[] = [
        			$dd->rownum,
        			$dd->kode_unit,
        			$dd->user_name,
        			$dd->group_name,
        			$sts_html,
        			'
						<div class="btn-group">
                            <a href="'.base_url("user/edit/".$dd->user_id).'" data-toggle="tooltip" data-placement="top" title="Edit Data" class="btn btn-success">
                                <i class="fas fa-edit"> </i>
                            </a>
                            <button type="button" onclick="onDestroy(\''.$dd->user_id.'\')" data-toggle="tooltip" data-placement="top" title="Hapus Data" class="btn btn-danger">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
        			'
        		];

        	}

        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

    public function ajax_destroy() {
        
        $this->form_validation->set_rules("id", "Id", "required");

        if($this->form_validation->run() != FALSE) {

            $this->User_model->delete($this->input->post("id"));
            $response = [
                "error" => FALSE,
                "message" => "Berhasil"
            ];

        } else {
            
            $response = [
                "error" => TRUE,
                "message" => validation_errors()
            ];

        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }

    public function ajax_deactive() {
        
        $this->form_validation->set_rules("id", "Id", "required");

        if($this->form_validation->run() != FALSE) {

            $this->User_model->update(
                $this->input->post("id"),
                [
                    "sts_active" => 0
                ]
            );
            
            $response = [
                "error" => FALSE,
                "message" => "Berhasil"
            ];

        } else {
            
            $response = [
                "error" => TRUE,
                "message" => validation_errors()
            ];
            
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }

    public function ajax_active() {
        
        $this->form_validation->set_rules("id", "Id", "required");

        if($this->form_validation->run() != FALSE) {

            $this->User_model->update(
                $this->input->post("id"),
                [
                    "sts_active" => 1
                ]
            );

            $response = [
                "error" => FALSE,
                "message" => "Berhasil"
            ];

        } else {
            
            $response = [
                "error" => TRUE,
                "message" => validation_errors()
            ];
            
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }

}