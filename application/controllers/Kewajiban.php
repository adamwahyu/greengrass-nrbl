<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kewajiban extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Unit_model",
			"Kewajiban_model"
		]);
	}
	public function index() {
		$this->data["v_content"] = "kewajiban/index";
		$this->load->view('master/layouts/main', $this->data);
	}
	public function get_datatables() {
        $data_detail = $this->Unit_model->get_datatables_trans_kewajiban(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start")
        );

        $total_data = $this->Unit_model->get_datatables_trans_kewajiban(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start"),
            TRUE
        );

        $data = [];
        if(!empty($data_detail)) {

            foreach($data_detail as $dd) {
                $btn_aksi = "";

                if(trim($dd->status_kewajiban) == "LUNAS") {
                    $btn_aksi = '
                        <div class="btn-group">
                            <button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus Data" onclick="onDestroy('.$dd->id_kewajiban.')">
                                <i class="fas fa-trash"> </i>
                            </button>
                        </div>
                    ';
                }

                $html_status_badge = "";

                if($dd->status_kewajiban === "LUNAS") {
                    $html_status_badge = '
                        <span class="badge badge-success">
                            '.$dd->status_kewajiban.'
                        </span>
                    ';
                } else {
                    $html_status_badge = '
                        <span class="badge badge-danger">
                            '.$dd->status_kewajiban.'
                        </span>
                    ';
                }

                $data[] = [
                    $dd->rownum,
                    $dd->tahun,
                    $dd->bulan,
                    rupiah($dd->luas_unit),
                    rupiah($dd->tarif_permeter),
                    rupiah($dd->nilai_kewajiban),
                    rupiah($dd->nilai_pemenuhan),
                    $html_status_badge,
                    $btn_aksi
                ];

            }
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }
	public function get_datatables_unit() {
		$data_detail = $this->Unit_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start")
        );

        $total_data = $this->Unit_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {

        	foreach($data_detail as $dd) {

                $indikator_warna = "";

                if((int) $dd->jmltagihan == 0) {
                    $indikator_warna = '
                        <div class="color-palette-set">
                          <div class="bg-success color-palette"><span></span></div>
                        </div>
                    ';
                } else if((int) $dd->jmltagihan <=2) {
                    $indikator_warna = '
                        <div class="color-palette-set">
                          <div class="bg-warning color-palette"><span></span></div>
                        </div>
                    ';
                } else {
                    $indikator_warna = '
                        <div class="color-palette-set">
                          <div class="bg-danger color-palette"><span></span></div>
                        </div>
                    ';
                }
        		$data[] = [
        			$dd->rownum,
        			$dd->kode_unit,
        			$dd->nama_pemilik,
                    $dd->luas_unit,
        			$dd->nomor_va_narobil,
        			$dd->sts_huni,
        			rupiah($dd->tarif),
                    $indikator_warna,
        			'
                        <div class="btn-group">
                            <button type="button" onclick="onDetail(\''.$dd->id_unit.'\', \''.$dd->kode_unit.'\')" data-toggle="tooltip" data-placement="top" title="Lihat Kewajiban" class="btn btn-primary">
                                <i class="fas fa-eye"> </i>
                            </button>
                        </div>

        			'
        		];
        	}
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

	public function ajax_destroy() {
		$this->form_validation
			->set_rules("id", "Id Kewajiban", "required");

		if($this->form_validation->run() != FALSE) {

			$this->Kewajiban_model->delete_soft($this->input->post("id"));
			$response = [
				"error" => FALSE,
				"message" => "OK!",
			];

		} else {
			$response = [
				"error" => TRUE,
				"message" => "KO"
			];
		}

		$this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}
}
