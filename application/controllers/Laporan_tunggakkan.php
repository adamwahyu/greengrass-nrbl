
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_tunggakkan extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Laporan_tunggakkan_model"
		]);
	}
	public function index() {
		$this->data["v_content"] = "laporan/tunggakkan/index";
		$this->data["v_data"] = (object)[
			"mst_cluster" => $this->Laporan_tunggakkan_model->get_cluster()
		];
		$this->load->view('master/layouts/main', $this->data);
	}

	public function export_excel() {
		$this->form_validation
			->set_rules("cluster", "Cluster", "required");

		if($this->form_validation->run() != FALSE) {
			$cluster = $this->input->post("cluster");
			$name_cluster = $this->input->post("cluster_text");

			$data_detail = $this->Laporan_tunggakkan_model->get_tunggakkan($cluster);
			$spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();

			$i = 1;
			$spreadsheet->getActiveSheet()->setCellValue('A'.$i, 'No');
			$spreadsheet->getActiveSheet()->setCellValue('B'.$i, 'Kode Unit');
			$spreadsheet->getActiveSheet()->setCellValue('C'.$i, 'VA Tunggakkan');
			$spreadsheet->getActiveSheet()->setCellValue('D'.$i, 'Jumlah');
			$spreadsheet->getActiveSheet()->setCellValue('E'.$i, 'Keterangan');

			$i++;
			foreach($data_detail as $d) {
				$ii = "D";
				$spreadsheet->getActiveSheet()->setCellValue('A'.$i, $d->rownum);
				$spreadsheet->getActiveSheet()->setCellValue('B'.$i, $d->kode_unit);
				$spreadsheet->getActiveSheet()->setCellValue('C'.$i, $d->va_sementara);
				$spreadsheet->getActiveSheet()->setCellValue('D'.$i, $d->amount);
				$spreadsheet->getActiveSheet()->setCellValue('E'.$i, $d->description);
				$i++;
			}

			$writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
			$txt_filename = "Lap.Tunggakkan-(".$name_cluster.")-".date("Y-m-d");
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$txt_filename.'".xlsx');
			header('Cache-Control: max-age=0');
			ob_end_clean();
			return $writer->save("php://output");
		} else {
			show_404();
		}
    }

	public function get_datatables() {
		$data_detail = $this->Laporan_tunggakkan_model->get_datatables_tunggakkan(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start")
        );

        $total_data = $this->Laporan_tunggakkan_model->get_datatables_tunggakkan(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start"),
            TRUE
        );

        $data = [];
        if(!empty($data_detail)) {

            foreach($data_detail as $dd) {
            	$data[] = [
        			$dd->rownum,
        			$dd->kode_unit,
        			$dd->va_sementara,
        			$dd->amount,
        			$dd->description
        		];
            }
        }

        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}

}
