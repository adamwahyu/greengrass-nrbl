<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Pembayaran_model",
            "Unit_model",
            "Tarif_model",
            "TPembayaran_model"
		]);
        $config['upload_path']          = "./upload/pembayaran/";
        $config['allowed_types']        = 'jpg|png|jpeg|pdf';
        $config['max_size']             = 5000;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
        $this->load->library('upload', $config);
	}

	public function index() {
		$this->data["v_content"] = "pembayaran/index";
		$this->load->view("master/layouts/main", $this->data);
	}
    
    public function maximum_periode($number) {
        $number = intval($number);
        if(is_numeric($number) && $number > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function create() {
        $this->form_validation
            ->set_rules("kode_unit", "Kode Unit", "required");
        $this->form_validation
            ->set_rules("namapemilik", "Nama Pemilik", "required");
        // $this->form_validation
        //     ->set_rules("email", "Email", "required");
        $this->form_validation
            ->set_rules("bulan_awal", "Bulan Awal", "required");
        $this->form_validation
            ->set_rules("tahun_awal", "Tahun Awal", "required");
        $this->form_validation
            ->set_rules("periode_spp", "Periode SPP", "required|callback_maximum_periode");
        $this->form_validation
            ->set_rules("jml_tagihan_pembayaran", "Jumlah Tagihan Pembayarn", "required");
        $this->form_validation
            ->set_rules("jml_pembayaran", "Jumlah Pembayarn", "required");
        $this->form_validation
            ->set_rules("metode_pembayaran", "Metode Pembayarn", "required");

        $this->form_validation
            ->set_rules("tgl_pembayaran", "Tanggal Pembayaran", "required");

        if($this->input->post("metode_pembayaran") !== "7") {

            if(empty($_FILES["file_bukti"]["tmp_name"])) {
                $this->form_validation
                    ->set_rules("file_bukti", "Bukti File", "required");
            }

        }
        
        if($this->form_validation->run() != FALSE) {

            

            $unit = $this->Unit_model->get_one_all_by_column_as_object("kode_unit", $this->input->post("kode_unit"));
            $tarif = $this->Tarif_model->get_tarif();
            $jml_pembayaran = str_replace(".", "", $this->input->post("jml_pembayaran"));
            
            $tgl_pembayaran = $this->input->post("tgl_pembayaran");
            $tgl_pembayaran = date("Y-m-d", strtotime(str_replace("/", "-", $tgl_pembayaran)));

            $data = [
                "kode_unit" => $this->input->post("kode_unit"),
                "nama_pemilik" => $this->input->post("namapemilik"),
                "nomor_va_narobil" => $unit->nomor_va_narobil,
                "nomor_va_bca" => $unit->nomor_va_bca,
                "tgl_spp" => date("Y-m-d"),
                "bulan_awal" => $this->input->post("bulan_awal"),
                "tahun_awal" => $this->input->post("tahun_awal"),
                "periode_spp" => $this->input->post("periode_spp"),
                "luas_unit" => $unit->luas_unit,
                "tarif_permeter" => $tarif->tarif_permeter,
                "nilai_spp" => $jml_pembayaran,
                "spp_created_by" => $this->session->userdata("user")->user_id,
                "sts_active" => 1,
                "status_spp" => "DONE",
                "id_metode_pembayaran" => $this->input->post("metode_pembayaran"),
                "tgl_bayar" => $tgl_pembayaran,
                "nilai_kewajiban" => $jml_pembayaran,
                "nilai_bayar" => $jml_pembayaran,
                "selisih" => 0,
                "create_by" => $this->session->userdata("user")->user_id
            ];

            if($this->input->post("metode_pembayaran") !== "7") {
                if($this->upload->do_upload("file_bukti")) {
                    
                    $data_file = $this->upload->data();
                    $new_name = str_replace($data_file["file_ext"], "", $data_file["file_name"])."_".$this->input->post("kode_unit")."_".date("Ymdhis").$data_file["file_ext"];

                    $old_name = $data_file["full_path"];
                    rename($old_name, FCPATH."/upload/pembayaran/".$new_name);
                    $data["file_bukti_bayar"] = $new_name;

                } else {
                    if($this->upload->display_errors()) {
                        $this->session->set_flashdata("error", validation_errors().$this->upload->display_errors());
                    }
                    redirect("pembayaran/create");
                }   
            }

            $t_spp_id = $this->Pembayaran_model->insert($data);
            
            $this->TPembayaran_model->update_by_column("id_spp", $t_spp_id, [
                "create_by" => $this->session->userdata("user")->user_id
            ]);
            
            $this->session->set_flashdata("success", "Berhasil diinputkan");
            
            redirect(
                "pembayaran"
            );

        } else {
            if(validation_errors() || $this->upload->display_errors()) {
                $this->session->set_flashdata("error", validation_errors().$this->upload->display_errors());
            }
            $unit = $this->Unit_model->get_all();

            $this->data["v_content"] = "pembayaran/create";
            $this->data["v_data"] = (object) [
                "unit" => $unit,
                "metode_pembayaran" => $this->Pembayaran_model->get_metode_pembayaran_form_pembayaran()
            ];
            $this->load->view("master/layouts/main", $this->data);
        }
    }

    public function get_nominal_pembayaran() {

        $this->form_validation
            ->set_rules("kode_unit", "Kode Unit", "required");
        $this->form_validation
            ->set_rules("jml_periode", "Jumlah Periode", "required");

        if($this->form_validation->run() != FALSE) {

            $data = $this->Pembayaran_model->get_nominal_pembayaran($this->input->post("kode_unit"), $this->input->post("jml_periode"));
            $response = [
                "error" => FALSE,
                "data" => $data
            ];

        } else {
            
            $response = [
                "error" => TRUE,
                "message" => validation_errors()
            ];

        }
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;

    }

    public function get_jml_tagihan_pembayaran_by_kode_unit() {

        $this->form_validation
            ->set_rules("kode_unit", "Kode Unit", "required");
        $this->form_validation
            ->set_rules("jml_periode", "Jumlah Periode", "required");

        if($this->form_validation->run() != FALSE) {

            $pembayaran = $this->Pembayaran_model->get_one_pembayaran_as_object_by_kode_unit_and_jml_periode($this->input->post("kode_unit"), $this->input->post("jml_periode"));
            $pembayaran->jml_tagihan_pembayaran = rupiah($pembayaran->jml_tagihan_pembayaran);

            $response = [
                "error" => TRUE,
                "data" => $pembayaran
            ];

        } else {
            
            $response = [
                "error" => TRUE,
                "message" => validation_errors()
            ];

        }
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }

    public function get_unit_by_kode_unit() {
        $this->form_validation
            ->set_rules("kode_unit", "Kode Unit", "required");

        if($this->form_validation->run() != FALSE) {

            $unit = $this->Pembayaran_model->get_one_unit_as_object_by_kode_unit($this->input->post("kode_unit"));

            if($unit->jml_tagihan == 0) {
                $unit->is_tunggakan = FALSE; 
            } else {
                $unit->is_tunggakan = TRUE;
            }
            $unit->master_spp = array_values(array_filter(get_periode_spp(), function($v) use ($unit) {
                if($unit->jml_tagihan <= $v) {
                    return TRUE;
                }
                return FALSE;
            }));

            $unit->jml_tagihan_pembayaran = rupiah($unit->jml_tagihan_pembayaran);

            $response = [
                "error" => TRUE,
                "data" => $unit
            ];

        } else {
            
            $response = [
                "error" => TRUE,
                "message" => validation_errors()
            ];

        }
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }

    public function ajax_get_pembayaran_by_id() {
        
        $this->form_validation
            ->set_rules("id", "ID", "required");

        if($this->form_validation->run() != FALSE) {

            $data = $this->Pembayaran_model->get_one_as_object_by_id($this->input->post("id"));

            $data->tgl_bayar = date_indo(date("Y-m-d", strtotime($data->tgl_bayar)));
            $response = [
                "error" => FALSE,
                "data" => $data
            ];

        } else {
            $response = [
                "error" => FALSE,
                "message" => validation_errors()
            ];
        }

        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }

	public function get_datatables() {
		$data_detail = $this->Pembayaran_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start")
        );

        $total_data = $this->Pembayaran_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {
        	
        	foreach($data_detail as $dd) {

        		$data[] = [
        			$dd->rownum,
        			$dd->id_spp,
        			$dd->kode_unit,
        			$dd->nama_pemilik,
        			$dd->tgl_spp,
        			$dd->periode_spp,
        			rupiah($dd->nilai_spp),
        			$dd->desc_metode_pembayaran,
        			'
        				<div class="btn-group">
                            <a href="'.base_url("pembayaran/create_ttp_pdf/".$dd->kode_unit)."/".$dd->id_spp.'" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Buat Tanda Terima">
                                <i class="fas fa-file-pdf"></i>
                            </a>
							<button type="button" onclick="onView(\''.$dd->id_spp.'\')" data-toggle="tooltip" data-placement="top" title="Lihat Data" class="btn btn-primary">
		                    	<i class="fas fa-eye"></i>
		                    </button>
						</div>
        			'
        		];

        	}

        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

    public function create_ttp_pdf($kode_unit,$id_spp) {
        $unit = $this->Unit_model->get_one_all_by_column_as_object("kode_unit",$kode_unit);
        $data_head = $this->Pembayaran_model->get_data_head_pdf($id_spp);
        $data_detail = $this->Pembayaran_model->get_data_detail_pdf($kode_unit,$id_spp);
        $this->data["v_data"] = (object) [
            "unit" => $unit,
            "data_header" => $data_head,
            "data_detail" => $data_detail,
            "code_ttp" => $this->Pembayaran_model->get_ttp_code()
        ];
        $html = $this->load->view('master/template_pdf/template_ttp', $this->data, TRUE);

        $dompdf = new Dompdf\Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'potrait');
        $dompdf->render();
        $dompdf->stream($this->data["v_data"]->code_ttp.".pdf", [
        ]);
        exit(0);
    }

}