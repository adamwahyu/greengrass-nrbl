<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Unit_model"
		]);
	}
	public function index() {

		$this->data["v_content"] = "unit/index";
		$this->load->view("master/layouts/main", $this->data);
	}
      public function ajax_deactive() {
        
        $this->form_validation->set_rules("id_unit", "id_unit", "required");

        if($this->form_validation->run() != FALSE) {

            $this->Unit_model->update(
                $this->input->post("id_unit"),
                [
                    "sts_active" => 0,
                    "id_sts_unit" => 0
                ]
            );
            
            $response = [
                "error" => FALSE,
                "message" => "Berhasil"
            ];

        } else {
            
            $response = [
                "error" => TRUE,
                "message" => validation_errors()
            ];
            
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }

    public function create_tunggakan_invoice_pdf($id_unit) {

        $this->form_validation->set_rules("tanggal_jatuh_tempo", "Tempo", "required");

        if($this->form_validation->run() != FALSE) {

            $unit = $this->Unit_model->get_all_custom_by_kodeunit($id_unit);
            $tunggakan = $this->Unit_model->get_trans_kewajiban_tunggakan_by_kodeunit($unit->kode_unit);

            $tanggal_cetak = date_indo(date("Y-m-d"))." ".date("h:i:s");

            $html = $this->load->view("master/template_pdf/template_invoice_tunggakan", [
                "v_data" => (object) [
                    "unit" => $unit,
                    "tunggakan" => $tunggakan,
                    "tanggal_jatuh_tempo" => date_indo(date("Y-m-d", strtotime(str_replace("/", "-", $this->input->post("tanggal_jatuh_tempo"))))),
                    "base64_image" => image_base64(FCPATH."/asset/img/green_grass.png"),
                    "tanggal_cetak" => $tanggal_cetak
                ]
            ], TRUE);
            
            $dompdf = new Dompdf\Dompdf();
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'potrait');
            $dompdf->render();
            $dompdf->stream("IVC-".$unit->kode_unit."-".date("Ymdhis").".pdf", [
                // "Attachment" => FALSE
            ]);

            exit(0);
        } else {
            show_404();
        }
    }

    public function create() {
        // var_dump($this->input->post());die;
        $this->form_validation
            ->set_rules("kodeunit", "Nama Pemilik", "required|xss_clean");
        $this->form_validation
            ->set_rules("namapemilik", "Nama Pemilik", "required|xss_clean");
        $this->form_validation
            ->set_rules("luasunit", "Luas Unit", "required|xss_clean");
        $this->form_validation
            ->set_rules("nomerva_narobil", "VA Narobil", "required|xss_clean");
        $this->form_validation
            ->set_rules("id_cluster", "Cluster", "required|xss_clean");
        $this->form_validation
            ->set_rules("msisdn", "MSISDN", "trim|xss_clean");
        $this->form_validation
            ->set_rules("email", "Email", "trim|xss_clean");
        $this->form_validation
            ->set_rules("notes", "Notes", "trim|xss_clean");
        $this->form_validation
            ->set_rules("id_sts_unit", "Status Huni", "trim|xss_clean");

        if($this->form_validation->run() != FALSE) {

            $data_insert = [
                "kode_unit" => $this->input->post("kodeunit"),
                "nomor_va_narobil" => $this->input->post("nomerva_narobil"),
                "nomor_va_bca" => $this->input->post("nomor_va_bca"),
                "nama_pemilik" => $this->input->post("namapemilik"),
                "msisdn" => $this->input->post("msisdn"),
                "email" => $this->input->post("email"),
                "luas_unit" => $this->input->post("luasunit"),
                "id_cluster" => $this->input->post("id_cluster"),
                "notes" => $this->input->post("notes"),
                "id_sts_unit" => $this->input->post("id_sts_unit"),
                "create_by" => $this->session->userdata("user")->user_id,
                "sts_active" => 1
            ];
            $this->Unit_model->insert($data_insert);
            $this->session->set_flashdata("success", "Berhasil diinputkan");
            redirect('/unit');
        } else {
            if(validation_errors()) {
                $this->session->set_flashdata("error", validation_errors());
            }
            $this->data['combo_huni'] = $this->Unit_model->get_hunian();
            $this->data['cluster'] = $this->Unit_model->get_cluster();
            $this->data["v_content"] = "unit/create";
            // $this->data["v_data"] = (object) [
            //     "unit" => $data
            // ];
            $this->load->view("master/layouts/main", $this->data);
        }
    }

    public function edit($id_unit) {

        $this->form_validation
            ->set_rules("namapemilik", "Nama Pemilik", "required|xss_clean");
	$this->form_validation
            ->set_rules("luasunit", "Luas Unit", "required|xss_clean");
        $this->form_validation
            ->set_rules("msisdn", "MSISDN", "trim|xss_clean");
        $this->form_validation
            ->set_rules("email", "Email", "trim|xss_clean");
        $this->form_validation
            ->set_rules("notes", "Notes", "trim|xss_clean");
        $this->form_validation
            ->set_rules("id_unit", "Status Huni", "trim|xss_clean");

        if($this->form_validation->run() != FALSE) {

            $data_update = [
                "nama_pemilik" => $this->input->post("namapemilik"),
                "msisdn" => $this->input->post("msisdn"),
                "email" => $this->input->post("email"),
		"luas_unit" => $this->input->post("luasunit"),
                "id_sts_unit" => $this->input->post("id_sts_unit"),
                "id_cluster" => $this->input->post("id_cluster"),
                "notes" => $this->input->post("notes"),
                "update_by" => $this->session->userdata("user")->user_id,
                "updated_date" => date("Y-m-d H:i:s")
            ];
            $this->Unit_model->update_by_column("id_unit", $this->input->post("id_unit"), $data_update);

            $this->session->set_flashdata("success", "Berhasil diperbarui");
            redirect('/unit');
        } else {
            if(validation_errors()) {
                $this->session->set_flashdata("error", validation_errors());
            }
            $data = $this->Unit_model->get_one_as_object_by_kodeunit($id_unit);
            $this->data['combo_huni'] = $this->Unit_model->get_hunian();
            $this->data['cluster'] = $this->Unit_model->get_cluster();
            $this->data["v_content"] = "unit/edit";
            $this->data["v_data"] = (object) [
                "unit" => $data
            ];
            $this->load->view("master/layouts/main", $this->data);
        }
    }

	public function get_datatables() {
		$data_detail = $this->Unit_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start")
        );

        $total_data = $this->Unit_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {

        	foreach($data_detail as $dd) {

                $indikator_warna = "";

                if((int) $dd->jmltagihan == 0) {
                    $indikator_warna = '
                        <div class="color-palette-set">
                          <div class="bg-success color-palette"><span></span></div>
                        </div>
                    ';
                } else if((int) $dd->jmltagihan <=2) {
                    $indikator_warna = '
                        <div class="color-palette-set">
                          <div class="bg-warning color-palette"><span></span></div>
                        </div>
                    ';
                } else {
                    $indikator_warna = '
                        <div class="color-palette-set">
                          <div class="bg-danger color-palette"><span></span></div>
                        </div>
                    ';
                }
        		$data[] = [
        			$dd->rownum,
        			$dd->kode_unit,
        			$dd->nama_pemilik,
                    $dd->cluster,
                    // $dd->luas_unit,
        			$dd->nomor_va_narobil,
        			$dd->sts_huni,
        			rupiah($dd->tarif),
                    $indikator_warna,
        			'
                        <div class="btn-group">
                            <button data-href="'.base_url("unit/create_tunggakan_invoice_pdf/".$dd->id_unit).'" onclick="onCreateInvoice(this)" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Cetak Invoice Tunggakan">
                                <i class="fas fa-file-pdf"> </i>
                            </button>
                            <a href="'.base_url("unit/edit/".$dd->id_unit).'" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Edit Data">
                                <i class="fas fa-edit"> </i>
                            </a>
                            <button type="button" onclick="onDetail(\''.$dd->id_unit.'\', \''.$dd->kode_unit.'\')" data-toggle="tooltip" data-placement="top" title="Lihat Data" class="btn btn-primary">
                                <i class="fas fa-eye"> </i>
                            </button>
                             <button type="button" onclick="onDeactive(\''.$dd->id_unit.'\')" data-toggle="tooltip" data-placement="top" title="Hapus Data" class="btn btn-danger">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>

        			'
        		];
        	}
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

    public function ajax_get_unit_by_kodeunit() {
        $this->form_validation
            ->set_rules("id_unit", "ID Unit", "required");

        if($this->form_validation->run() != FALSE) {
            $data = $this->Unit_model->get_all_custom_by_kodeunit($this->input->post("id_unit"));
            $data->nilai_tunggakan = rupiah($data->nilai_tunggakan);
            $response = [
                "error" => FALSE,
                "message" => "OK!",
                "data" => $data
            ];
        } else {
            $response = [
                "error" => TRUE,
                "message" => "KO"
            ];
        }
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }

    public function get_datatables_trans_kewajiban() {
        $data_detail = $this->Unit_model->get_datatables_trans_kewajiban(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start")
        );

        $total_data = $this->Unit_model->get_datatables_trans_kewajiban(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start"),
            TRUE
        );

        $data = [];
        if(!empty($data_detail)) {

            foreach($data_detail as $dd) {

                $data[] = [
                    $dd->rownum,
                    $dd->tahun,
                    $dd->bulan,
                    rupiah($dd->luas_unit),
                    rupiah($dd->tarif_permeter),
                    rupiah($dd->nilai_kewajiban),
                    rupiah($dd->nilai_pemenuhan),
                    ($dd->status_kewajiban == 'LUNAS') ? 
                        '<span class="badge badge-success">
                            '.$dd->status_kewajiban.'</span>'
                            :
                        '<span class="badge badge-danger">
                            '.$dd->status_kewajiban.'</span>'
                        ,
                ];
            }
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }

}
