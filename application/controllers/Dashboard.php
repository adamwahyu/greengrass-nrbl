<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model([
			"Dashboard_model",
            "Unit_warga_model"
		]);
	}
	public function index() {
        $v_data_addon = [];
        if (empty($this->session->userdata("user")->id_unit)) {
            $this->session->userdata("user")->id_unit = 0;
        }
        if($this->session->userdata("user")->group_id == 4) {
            $v_data_addon["data_tagihanyangharusdibayar"] = $this->Dashboard_model->get_tagihanyangharusdibayar_bykodeunit(
                $this->session->userdata("user")->kode_unit
            );
            $this->data["v_content"] = "dashboard/dashboard_warga";
        } else {
            $this->data["v_content"] = "dashboard/dashboard_administrator";
        }
        $v_data_addon["summary_unit"] = $this->Unit_warga_model->get_all_custom_by_kodeunit($this->session->userdata("user")->id_unit);
        $v_data_addon["mst_indikator"] = $this->Dashboard_model->get_indikator_warna_perumahan();
        $v_data_addon["vanonrutin"] = $this->Unit_warga_model->get_vanonrutin($this->session->userdata("user")->kode_unit);
        $v_data_addon["jumlahtunggakan"] = $this->Unit_warga_model->get_jumlahtunggakan($this->session->userdata("user")->kode_unit);


        $this->data["v_data"] = (object) $v_data_addon;
        $this->load->view('master/layouts/main', $this->data);
    }

    public function get_datatables_indikator_warna_perumahan() {

        $data_detail = $this->Dashboard_model->get_datatables_indikator_warna_perumahan(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start")
        );

        $total_data = $this->Dashboard_model->get_datatables_indikator_warna_perumahan(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start"),
            TRUE
        );

        $data = [];
        if(!empty($data_detail)) {

            foreach($data_detail as $dd) {

                $data[] = [
                    $dd->rownum,
                    $dd->kode_unit,
                    $dd->nama_pemilik,
                    // $dd->email,
                    // $dd->msisdn,
                    $dd->luas_unit,
                    $dd->nomor_va_narobil,
                    $dd->nomor_va_bca,
                    $dd->id_sts_unit_desc,
                    $dd->jml_tagihan,
                    number_format($dd->total)
                ];
            }
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }

    public function get_datatables_indikator_warna_perumahan_all() {

        $data_detail = $this->Dashboard_model->get_datatables_indikator_warna_perumahan_all(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start")
        );

        $total_data = $this->Dashboard_model->get_datatables_indikator_warna_perumahan_all(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start"),
            TRUE
        );

        $data = [];
        if(!empty($data_detail)) {

            foreach($data_detail as $dd) {

                $data[] = [
                    $dd->rownum,
                    $dd->kode_unit,
                    $dd->nama_pemilik,
                    $dd->luas_unit,
                    $dd->nomor_va_narobil,
                    $dd->nomor_va_bca,
                    $dd->id_sts_unit_desc,
                    $dd->jml_tagihan,
                    number_format($dd->total)
                ];
            }
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }

    public function get_datatables_trans_kewajiban() {
        $data_detail = $this->Unit_warga_model->get_datatables_trans_kewajiban(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start")
        );

        $total_data = $this->Unit_warga_model->get_datatables_trans_kewajiban(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start"),
            TRUE
        );

        $data = [];
        if(!empty($data_detail)) {

            foreach($data_detail as $dd) {

                $data[] = [
                    $dd->rownum,
                    $dd->tahun,
                    $dd->bulan,
                    rupiah($dd->luas_unit),
                    rupiah($dd->tarif_permeter),
                    rupiah($dd->nilai_kewajiban),
                    rupiah($dd->nilai_pemenuhan),
                    ($dd->status_kewajiban == 'LUNAS') ?
                        '<span class="badge badge-success">
                            '.$dd->status_kewajiban.'</span>'
                            :
                        '<span class="badge badge-danger">
                            '.$dd->status_kewajiban.'</span>'
                        ,
                ];
            }
        }
        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
    }

	public function register() {
		$this->load->view('public/register');
	}
}
