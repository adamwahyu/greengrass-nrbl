<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	public function __construct() {
		parent::__construct();
		set_time_limit(0);
		$this->load->database();
		$this->load->model([
			"Laporan_model"
		]);
	}

	public function index() {

		dd(md5("hotincream2018"));

		$mst_tahun = $this->db->query("
			select
			distinct
			tahun
			from t_kewajiban order by tahun asc limit 2
		")->result();
		$mst_bulan = $this->db->query("
			select
			*
			from ref_bulan order by id_bulan asc
		")->result();

		$this->load->view("public/test-table", [
			"mst_tahun" => $mst_tahun,
			"mst_bulan" => $mst_bulan
		]);
	}

	public function create_excel() {

		$mst_tahun = $this->db->query("
			select
			distinct
			tahun
			from t_kewajiban order by tahun asc limit 5
		")->result();
		$mst_bulan = $this->db->query("
			select
			*
			from ref_bulan order by id_bulan asc
		")->result();
		
		$mst_unit = $this->db->query("
			select
			x.kode_unit,
			x.nomor_va_narobil,
			x.nomor_va_bca,
			x.nama_pemilik,
			(
				select
				desc_sts_unit
				from ref_status_unit
				where id_sts_unit = x.id_sts_unit
			) desc_sts_unit
			from ref_unit x
			order by x.kode_unit asc
			limit 5
		")->result();

		$spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();
		$i = 1;
		$spreadsheet->getActiveSheet()->setCellValue('A'.$i, 'No');
		$spreadsheet->getActiveSheet()->setCellValue('B'.$i, 'Kode Unit');
		$spreadsheet->getActiveSheet()->setCellValue('C'.$i, 'Status Hunia');
		$spreadsheet->getActiveSheet()->mergeCells("A1:A2");
		$spreadsheet->getActiveSheet()->mergeCells("B1:B2");
		$spreadsheet->getActiveSheet()->mergeCells("C1:C2");
		$ii = "D";
		foreach($mst_tahun as $tahun) {
			$first = TRUE;
			$afirst = $ii.$i;
			foreach($mst_bulan as $bulan) {
				$spreadsheet->getActiveSheet()->setCellValue($ii.$i, $tahun->tahun);
				$spreadsheet->getActiveSheet()->setCellValue($ii.($i+1), $bulan->nama_bulan);
				$ii++;
			}
			if(strlen($ii) > 1) {
				$kk = chr(ord(substr($ii, 1, 1)) - 1);
				$ka = substr($ii, 0, 1);
				$a = $ka.$kk;
			} else {
				$a = chr(ord($ii) - 1);
			}
			$efirst = $a.$i;
			$spreadsheet->getActiveSheet()->mergeCells("$afirst:$efirst");
		}

		$i = $i+1;
		$i++;

		foreach($mst_unit as $unit) {
			$ii = "D";
			$spreadsheet->getActiveSheet()->setCellValue('A'.$i, "");
			$spreadsheet->getActiveSheet()->setCellValue('B'.$i, $unit->kode_unit);
			$spreadsheet->getActiveSheet()->setCellValue('C'.$i,  $unit->desc_sts_unit);
			foreach($mst_tahun as $tahun) {
				foreach($mst_bulan as $bulan) {
					$r = $this->Laporan_model->get_datatables_kewajiban_detail($unit->kode_unit, $bulan->id_bulan, $tahun->tahun);
					if($r) {
						$spreadsheet->getActiveSheet()->setCellValue($ii.$i, $r->nilai_pemenuhan);
					} else {
						$spreadsheet->getActiveSheet()->setCellValue($ii.$i, 0);
					}
					$ii++;
				}
			}
			$i++;
		}

		$writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		$txt_filename = "LAPORAN-PEMBAYARAN-ALL-TAHUN-".date("Ymd");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$txt_filename.'".xlsx');
		header('Cache-Control: max-age=0');
		ob_end_clean();
		return $writer->save("php://output");

	}
}