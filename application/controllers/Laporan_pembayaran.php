
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_pembayaran extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Laporan_model"
		]);
	}
	public function index() {
		$this->data["v_content"] = "laporan/pembayaran/index";
		$this->data["v_data"] = (object)[
			"mst_tahun" => $this->Laporan_model->get_mst_tahun_kewajiban(),
			"mst_bulan" => $this->Laporan_model->get_mst_bulan_kewajiban(),
			"mst_cluster" => $this->Laporan_model->get_mst_cluster_kewajiban()
		];
		$this->load->view('master/layouts/main', $this->data);
	}

	public function export_excel() {
		
		$this->form_validation
			->set_rules("tahun", "Tahun", "required");
		$this->form_validation
			->set_rules("cluster", "Cluster", "required");

		if($this->form_validation->run() != FALSE) {

			$tahun = $this->input->post("tahun");
			$cluster = $this->input->post("cluster");

			$data_detail = $this->Laporan_model->get_kewajiban($cluster);
			$mst_bulan = $this->Laporan_model->get_mst_bulan_kewajiban();
			$spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();

			$i = 1;
			$spreadsheet->getActiveSheet()->setCellValue('A'.$i, 'No');
			$spreadsheet->getActiveSheet()->setCellValue('B'.$i, 'Kode Unit');
			$spreadsheet->getActiveSheet()->setCellValue('C'.$i, 'Status Hunian');

			$ii = "D";
			foreach($mst_bulan as $bulan) {
				$spreadsheet->getActiveSheet()->setCellValue($ii.$i, $bulan->nama_bulan);
				$ii++;
			}
			$i++;
			foreach($data_detail as $d) {
				$ii = "D";
				$spreadsheet->getActiveSheet()->setCellValue('A'.$i, $d->rownum);
				$spreadsheet->getActiveSheet()->setCellValue('B'.$i, $d->kode_unit);
				$spreadsheet->getActiveSheet()->setCellValue('C'.$i, $d->desc_sts_unit);

				foreach($mst_bulan as $bulan) {
					$r = $this->Laporan_model->get_datatables_kewajiban_detail($d->kode_unit, $bulan->id_bulan, $tahun);
					if($r) {
						$spreadsheet->getActiveSheet()->setCellValue($ii.$i, $r->nilai_pemenuhan);
					} else {
						$spreadsheet->getActiveSheet()->setCellValue($ii.$i, 0);
					}
					$ii++;
				}
				$i++;
			}

			$writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
			$txt_filename = "PEMBAYARAN-".$tahun."(".$cluster.")-".date("Y-m-d");
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$txt_filename.'".xlsx');
			header('Cache-Control: max-age=0');
			ob_end_clean();
			return $writer->save("php://output");
		} else {
			show_404();
		}
    }

	public function get_datatables() {
		$data_detail = $this->Laporan_model->get_datatables_kewajiban(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start")
        );

        $total_data = $this->Laporan_model->get_datatables_kewajiban(
            $this->input->get("search")["value"],
            $this->input->get("length"),
            $this->input->get("start"),
            TRUE
        );

        $mst_bulan = $this->Laporan_model->get_mst_bulan_kewajiban();

        $data = [];
        if(!empty($data_detail)) {

            foreach($data_detail as $dd) {

            	$fd = [];
            	array_push($fd, $dd->rownum);
            	array_push($fd, $dd->kode_unit);
            	array_push($fd, $dd->desc_sts_unit);

            	foreach($mst_bulan as $bulan) {
            		$dt = $this->Laporan_model->get_datatables_kewajiban_detail($dd->kode_unit, $bulan->id_bulan, $this->input->get("tahun"));
            		if($dt) {
            			array_push($fd, rupiah($dt->nilai_pemenuhan));
            		} else {
            			array_push($fd, 0);
            		}
            	}
                $data[] = $fd;

            }
        }

        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit;
	}

}
