<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_settlement extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model([
			"Laporan_settlement_model"
		]);
	}

	public function index() {
		$this->data["v_content"] = "laporan/settlement/index";
		$this->load->view("master/layouts/main", $this->data);
	}

    public function load_datatables() {
        $this->data['startdate'] = $this->input->post("startdate");
        $this->data['enddate'] = $this->input->post("enddate");
        $this->load->view("master/pages/laporan/settlement/datatables", $this->data);
    }

	public function get_datatables() {

		$data_detail = $this->Laporan_settlement_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
            $this->input->get("startdate"),
            $this->input->get("enddate")
        );

        $total_data = $this->Laporan_settlement_model->get_datatables(
        	$this->input->get("search")["value"],
        	$this->input->get("length"),
        	$this->input->get("start"),
            $this->input->get("startdate"),
            $this->input->get("enddate"),
        	TRUE
        );

        $data = [];
        if(!empty($data_detail)) {
        	
        	foreach($data_detail as $dd) {

        		$data[] = [
        			$dd->rownum,
        			$dd->transaction_number,
        			$dd->tanggal_transaksi,
                    $dd->transaction_state,
                    $dd->description,
        			rupiah($dd->amount)
        		];

        	}

        }

        $response = array(
            "draw" => $this->input->get("draw"),
            "recordsTotal" => $total_data,
            "recordsFiltered" => $total_data,
            "data" => $data
        );

        $this->output
		        ->set_status_header(200)
		        ->set_content_type('application/json', 'utf-8')
		        ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
		        ->_display();
		exit;
	}

    public function export_excel() {
        
            $startdate = $this->input->get("startdate");
            $enddate = $this->input->get("enddate");

            $data = $this->Laporan_settlement_model->get_settlement_excel($startdate,$enddate); 
            $spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();

            $spreadsheet->getActiveSheet()->setCellValue('A1', 'No');
            $spreadsheet->getActiveSheet()->setCellValue('B1', 'No Transaksi');
            $spreadsheet->getActiveSheet()->setCellValue('C1', 'Tanggal');
            $spreadsheet->getActiveSheet()->setCellValue('D1', 'Status');
            $spreadsheet->getActiveSheet()->setCellValue('E1', 'Keterangan');
            $spreadsheet->getActiveSheet()->setCellValue('F1', 'Nilai');

            $i = 2;
            $total = 0;
            foreach($data as $d) {
                $spreadsheet->getActiveSheet()->setCellValue('A'.$i, $d->rownum);
                $spreadsheet->getActiveSheet()->setCellValue('B'.$i, $d->transaction_number);
                $spreadsheet->getActiveSheet()->setCellValue('C'.$i, $d->tanggal_transaksi);
                $spreadsheet->getActiveSheet()->setCellValue('D'.$i, $d->transaction_state);
                $spreadsheet->getActiveSheet()->setCellValue('E'.$i, $d->description);
                $spreadsheet->getActiveSheet()->setCellValue('F'.$i, $d->amount);
                $total += $d->amount;
                $i++;
            }

            $styleTotal = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
            ];

            $spreadsheet->getActiveSheet()->getStyle("A".$i.":E".$i)->applyFromArray($styleTotal);
            $spreadsheet->getActiveSheet()->getStyle('F2:F'.$i)->getNumberFormat()->setFormatCode('#,##0');
            $spreadsheet->getActiveSheet()->mergeCells("A".$i.":E".$i);
            $spreadsheet->getActiveSheet()->setCellValue('A'.$i, 'TOTAL SETTLEMENT');
            $spreadsheet->getActiveSheet()->setCellValue('F'.$i, $total);

            foreach(range('A','F') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
            }

            $writer = new PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
            $changedate = date("dmY",strtotime($startdate));
            $changedate .= "-".date("dmY",strtotime($enddate));
            $txt_filename = "Lap-Settlement-".$changedate;
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$txt_filename.'".xlsx');
            header('Cache-Control: max-age=0');
            ob_end_clean();
            return $writer->save("php://output");
    }

}