<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resetpass_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get_datatables($like = null, $length = null, $start, $count = NULL) {
		$sql = "
			select 
				a.kode_unit,
				b.nama_pemilik,
				b.email,
				b.msisdn,
				b.nomor_va_narobil,
				b.nomor_va_bca,
				b.luas_unit
			FROM ref_user a
			INNER JOIN ref_unit b
			ON a.kode_unit = b.kode_unit
			where a.sts_active = 1
			and b.sts_active = 1
		";

		$sql_count = "select count(*) from({$sql}) x where 1=1";
        
        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
            	"x.rownum",
            	"x.kode_unit",
            	"x.nama_pemilik",
            	"x.email",
            	"x.msisdn",
            	"x.nomor_va_narobil",
            	"x.nomor_va_bca",
            	"x.luas_unit",
            	null
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
            	$sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];	
            }
        }
        $sql = "select * from({$sql})x where 1=1";
        $sql = "
            SELECT
            x.*,
            row_number() over(ORDER BY x.kode_unit asc) as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";
		if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.kode_unit ||
                x.nama_pemilik ||
                x.email ||
                x.msisdn ||
                x.nomor_va_narobil ||
                x.nomor_va_bca ||
                x.luas_unit
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
	}

}
