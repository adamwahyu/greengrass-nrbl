<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

    public function get_tagihanyangharusdibayar_bykodeunit($kode_unit) {
        $sql = "
            select
            a.kode_unit,
            (
                select
                nomor_va_narobil
                from ref_unit
                where kode_unit = a.kode_unit
            ) kode_billing,
            sum(a.nilai_kewajiban) as amount
            from t_kewajiban a
            where a.status_kewajiban = 'OPEN'
            and a.kode_unit = '".$kode_unit."'
            group by a.kode_unit order by a.kode_unit asc;
        ";
        return $this->db->query($sql)->result();
    }
    
    public function get_indikator_warna_perumahan() {
        $mst_perumahan = $this->db->query("
            select
            *
            from ref_perumahan
        ")->result();

        $data = [];
        foreach ($mst_perumahan as $p) {

            $cluster = $this->db->query("
                select
                *
                from ref_cluster where id_perumahan = ".$p->id_perumahan."
            ")->result();
            $cluster_new = [];
            foreach ($cluster as $c) {
                $mst_indikator = $this->db->query(
                    "
                        select
                            a.id_cluster,a.nama_cluster,a.warna,
                            count(1) total_unit,
                            (select sum(func_get_tagihan(a.kode_unit))) total
                        from (SELECT
                            a.id_cluster,a.nama_cluster,b.kode_unit,
                                        case
                                            when (func_get_jmltagihan(b.kode_unit)) = 0 then 'Green'
                                            when (func_get_jmltagihan(b.kode_unit)) > 0 and (func_get_jmltagihan(b.kode_unit)) <= 2 then 'Yellow'
                                            when (func_get_jmltagihan(b.kode_unit)) > 2 then 'Red'
                                        end as warna
                        from ref_cluster a
                        inner join ref_unit b on a.id_cluster = b.id_cluster
                        where id_perumahan = ".$p->id_perumahan."
                        and a.sts_active = 1
                        and b.sts_active = 1)a
                        where a.id_cluster = ".$c->id_cluster."
                        GROUP BY a.id_cluster,a.nama_cluster,a.warna
                        order by
                        id_cluster asc,
                        case
                        when upper(warna) = upper('red')
                        then 0
                        when upper(warna) = upper('yellow')
                        then 1
                        when upper(warna) = upper('green')
                        then 2
                        else 3
                        end
                    "
                )->result();
                array_push($cluster_new, (object)[
                    "cluster" => $c,
                    "indikator" => $mst_indikator
                ]);
            }
            array_push($data, (object)[
                "perumahan" => $p,
                "indikator" => $cluster_new
            ]);
        }
         return $data;
    }
    public function get_datatables_indikator_warna_perumahan_all($like = null, $length = null, $start, $count = NULL) {
        $id_perumahan = $this->input->get("id_perumahan");
        switch (strtolower($this->input->get("category"))) {
            case 'red':
                $sql = "
                    select
                    x.*,
                    a.*,
                    b.*,
                    func_get_jmltagihan(x.kode_unit) jml_tagihan,
                    func_get_tagihan(x.kode_unit) total,
                    (
                            select
                            desc_sts_unit
                            from ref_status_unit
                            where id_sts_unit = x.id_sts_unit
                    ) id_sts_unit_desc
                    from ref_unit x
                    inner join ref_cluster a on a.id_cluster = x.id_cluster
                    inner join ref_perumahan b on b.id_perumahan = a.id_perumahan
                    where x.sts_active = 1
                    and b.id_perumahan = ".$id_perumahan."
                    and func_get_jmltagihan(x.kode_unit) > 2
                ";
                break;
            case 'yellow':
                $sql = "
                    select
                    x.*,
                    a.*,
                    b.*,
                    func_get_jmltagihan(x.kode_unit) jml_tagihan,
                    func_get_tagihan(x.kode_unit) total,
                    (
                            select
                            desc_sts_unit
                            from ref_status_unit
                            where id_sts_unit = x.id_sts_unit
                    ) id_sts_unit_desc
                    from ref_unit x
                    inner join ref_cluster a on a.id_cluster = x.id_cluster
                    inner join ref_perumahan b on b.id_perumahan = a.id_perumahan
                    where x.sts_active = 1
                    and b.id_perumahan = ".$id_perumahan."
                    and func_get_jmltagihan(x.kode_unit) > 0
                    and func_get_jmltagihan(x.kode_unit) <= 2
                ";
                break;
            case 'green':
                $sql = "
                    select
                    x.*,
                    a.*,
                    b.*,
                    func_get_jmltagihan(x.kode_unit) jml_tagihan,
                    func_get_tagihan(x.kode_unit) total,
                    (
                            select
                            desc_sts_unit
                            from ref_status_unit
                            where id_sts_unit = x.id_sts_unit
                    ) id_sts_unit_desc
                    from ref_unit x
                    inner join ref_cluster a on a.id_cluster = x.id_cluster
                    inner join ref_perumahan b on b.id_perumahan = a.id_perumahan
                    where x.sts_active = 1
                    and b.id_perumahan = ".$id_perumahan."
                    and func_get_jmltagihan(x.kode_unit) = 0
                ";
                break;
            default:
                throw new Exception("Error Processing Request", 1);
                break;
        }

        $sql_count = "select count(*) from({$sql}) x where 1=1";

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
                "x.rownum",
                "x.kode_unit"
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
                $sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";

        $sql = "
            SELECT
            x.*,
            row_number() over(ORDER BY x.kode_unit DESC) as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";

        if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.kode_unit ||
                x.nama_pemilik ||
                                x.luas_unit ||
                                x.nomor_va_narobil ||
                                x.nomor_va_bca ||
                                x.id_sts_unit_desc ||
                                x.jml_tagihan
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
    }

    public function get_datatables_indikator_warna_perumahan($like = null, $length = null, $start, $count = NULL) {

        switch (strtolower($this->input->get("category"))) {
            case 'red':
                $sql = "
                    select
                    x.*,
                    func_get_jmltagihan(x.kode_unit) jml_tagihan,
                    func_get_tagihan(x.kode_unit) total,
                    (
                        select
                        desc_sts_unit
                        from ref_status_unit
                        where id_sts_unit = x.id_sts_unit
                    ) id_sts_unit_desc
                    from ref_unit x
                    where sts_active = 1
                    and id_cluster = ".$this->input->get("id_cluster")."
                    and func_get_jmltagihan(x.kode_unit) > 2
                ";
                break;
            case 'yellow':
                $sql = "
                    select
                    x.*,
                    func_get_jmltagihan(x.kode_unit) jml_tagihan,
                    func_get_tagihan(x.kode_unit) total,
                    (
                        select
                        desc_sts_unit
                        from ref_status_unit
                        where id_sts_unit = x.id_sts_unit
                    ) id_sts_unit_desc
                    from ref_unit x
                    where sts_active = 1
                    and id_cluster = ".$this->input->get("id_cluster")."
                    and func_get_jmltagihan(x.kode_unit) > 0
                    and func_get_jmltagihan(x.kode_unit) <= 2
                ";
                break;
            case 'green':
                $sql = "
                    select
                    x.*,
                    func_get_jmltagihan(x.kode_unit) jml_tagihan,
                    func_get_tagihan(x.kode_unit) total,
                    (
                        select
                        desc_sts_unit
                        from ref_status_unit
                        where id_sts_unit = x.id_sts_unit
                    ) id_sts_unit_desc
                    from ref_unit x
                    where sts_active = 1
                    and id_cluster = ".$this->input->get("id_cluster")."
                    and func_get_jmltagihan(x.kode_unit) = 0
                ";
                break;
            default:
                throw new Exception("Error Processing Request", 1);
                break;
        }

        $sql_count = "select count(*) from({$sql}) x where 1=1";

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
                "x.rownum",
                "x.kode_unit"
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
                $sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";
        $sql = "
            SELECT
            x.*,
            row_number() over(ORDER BY x.kode_unit DESC) as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";

        if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.kode_unit ||
                x.nama_pemilik ||
                                x.luas_unit ||
                                x.nomor_va_narobil ||
                                x.nomor_va_bca ||
                                x.id_sts_unit_desc ||
                                x.jml_tagihan
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }
        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
    }
}
