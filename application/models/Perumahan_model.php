<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perumahan_model extends CI_Model {

	protected $table = "ref_perumahan";
	protected $primaryKey = "id_perumahan";

	public function __construct() {
		parent::__construct();
	}
	
	public function get_all() {
		$sql = "
			select
			*
			from {$this->table}
		";
		return $this->db->query($sql)->result();
	}

	public function get_datatables($like = null, $length = null, $start, $count = NULL) {
		$sql = "
			select
			x.id_perumahan,
			x.nama_perumahan,
			case
			when(x.sts_active=1)
			then 'Y'
			else 'T'
			end as status
			from ref_perumahan x
		";

		$sql_count = "select count(*) from({$sql}) x where 1=1";
		
        $sql = "select * from({$sql})x where 1=1";
        $sql = "
			SELECT
			x.*,
			row_number() over(ORDER BY x.nama_perumahan asc) as rownum
            FROM( $sql ) x WHERE 1 = 1
		";
		if(!is_null($count)) {
            $sql = $sql_count;
        }
        
        if(!empty($like)) {
            $sql .= "AND upper(
            	x.nama_perumahan ||
                x.status
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
	}

	public function get_all_by_column_as_object($column, $value) {
		$where[$column] = $value;
		return $this->db->get_where($this->table, $where)->result();
	}
	public function get_one_all_by_column_as_object($column, $value) {
		$where[$column] = $value;
		return $this->db->get_where($this->table, $where)->row();
	}
	public function get_profile_as_object_by_kodeunit($kodeunit) {
		$sql = "
			select 
				a.kode_unit,
				b.nama_pemilik,b.email,
				b.msisdn,b.nomor_va_narobil,
				b.nomor_va_bca,b.luas_unit
			FROM ref_user a
			INNER JOIN ref_unit b
			ON a.kode_unit = b.kode_unit
			where a.sts_active = 1
			and b.sts_active = 1
			and a.kode_unit = '{$kodeunit}'
		";
		return $this->db->query($sql)->row();
	}

	public function get_one_as_object_by_id($id) {
		$sql = "
			select
			*
			from {$this->table}
			where {$this->primaryKey}={$id}
			limit 1
		";
		return $this->db->query($sql)->row();
	}

	public function insert($data) {
		$this->db
			->insert($this->table, $data);
		return $this->db->insert_id();
	}
	public function insert_batch($data) {
		$this->db
			->insert_batch($this->table, $data);
		return TRUE;
	}
	public function update_by_column($column, $value, $data) {
		$this->db->where($column, $value);
		return $this->db->update($this->table, $data);
	}
	public function update($id, $data) {
		$this->db->where($this->primaryKey, $id);
		return $this->db->update($this->table, $data);
	}
	public function update_batch($data, $id) {
		$this->db
			->update_batch($this->table, $data, $id);
		return TRUE;
	}
	public function delete($value, $column = "") {
		$column = (empty($column)) ? $this->primaryKey : $column;
		$this->db->where($column, $value);
		$query = $this->db->delete($this->table);
		return $query;
	}

}
