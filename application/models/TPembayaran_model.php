<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TPembayaran_model extends CI_Model {

	protected $table = "t_pembayaran";
	protected $primaryKey = "id_pembayaran";

	public function __construct() {
		parent::__construct();
	}

	public function get_all() {
		$sql = "
			select
			*
			from {$this->table}
		";
		return $this->db->query($sql)->result();
	}
	public function get_all_by_column_as_object($column, $value) {
		$where[$column] = $value;
		return $this->db->get_where($this->table, $where)->result();
	}
	public function get_one_all_by_column_as_object($column, $value) {
		$where[$column] = $value;
		return $this->db->get_where($this->table, $where)->row();
	}
	public function get_one_as_object_by_id($id) {
		$sql = "
			select
			*
			from {$this->table}
			where {$this->primaryKey}={$id}
			limit 1
		";
		return $this->db->query($sql)->row();
	}

	public function insert($data) {
		$this->db
			->insert($this->table, $data);
		return $this->db->insert_id();
	}
	public function insert_batch($data) {
		$this->db
			->insert_batch($this->table, $data);
		return TRUE;
	}
	public function update_by_column($column, $value, $data) {
		$this->db->where($column, $value);
		return $this->db->update($this->table, $data);
	}
	public function update_by_id($id, $data) {
		$this->db->where($this->primaryKey, $id);
		return $this->db->update($this->table, $data);
	}
	public function update_batch($data, $id) {
		$this->db
			->update_batch($this->table, $data, $id);
		return TRUE;
	}
	public function delete($value, $column = "") {
		$column = (empty($column)) ? $this->primaryKey : $column;
		$this->db->where($column, $value);
		$query = $this->db->delete($this->table);
		return $query;
	}

}
