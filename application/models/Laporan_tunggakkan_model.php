<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_tunggakkan_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get_cluster() {
		$sql = "
			select
			*
			from ref_cluster
			where sts_active = 1
		";

		return $this->db->query($sql)->result();
	}

	public function get_datatables_tunggakkan($like = null, $length = null, $start, $count = NULL) {

		if($this->input->get("cluster") == "all") {
			$sql = "
				SELECT 
					payment_code va_sementara,name as kode_unit,
					description,amount
				FROM narobil_pay_collect_billing_list x
				where payment_code not in (select nomor_va_narobil from ref_unit where sts_active = 1)
			";
		} else {
			$sql = "
				SELECT 
					payment_code va_sementara,name as kode_unit,
					description,amount
				FROM narobil_pay_collect_billing_list x
				where payment_code not in (select nomor_va_narobil from ref_unit where sts_active = 1)
				and name in (select kode_unit from ref_unit where id_cluster =".$this->input->get("cluster")." and sts_active = 1)
				order by x.name asc
			";
		}

		$sql_count = "select count(*) from({$sql}) x where 1=1";
        $sql = "select * from({$sql})x where 1=1";
        $sql = "
			SELECT
			x.*,
			row_number() over(ORDER BY x.kode_unit asc) as rownum
            FROM( $sql ) x WHERE 1 = 1
		";
		if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.kode_unit
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
	}

	public function get_tunggakkan($cluster) {
		if($cluster == "all") {
			$sql = "
				SELECT 
					payment_code va_sementara,name as kode_unit,
					description,amount
				FROM narobil_pay_collect_billing_list x
				where payment_code not in (select nomor_va_narobil from ref_unit where sts_active = 1)
			";
		} else {
			$sql = "
				SELECT 
					payment_code va_sementara,name as kode_unit,
					description,amount
				FROM narobil_pay_collect_billing_list x
				where payment_code not in (select nomor_va_narobil from ref_unit where sts_active = 1)
				and name in (select kode_unit from ref_unit where id_cluster =".$cluster." and sts_active = 1)
				order by x.name asc
			";
		}
		$sql = "
			SELECT
			x.*,
			row_number() over(ORDER BY x.kode_unit asc) as rownum
            FROM( $sql ) x WHERE 1 = 1
		";
		return $this->db->query($sql)->result();
	}
}