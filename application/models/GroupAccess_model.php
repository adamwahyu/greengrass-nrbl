<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GroupAccess_model extends CI_Model {

	protected $table = "ref_group_access";
	protected $primaryKey = "ref_id_gaccess";

	public function __construct() {
		parent::__construct();
	}

	public function get_all() {
		$sql = "
			select
			*
			from {$this->table}
		";
		return $this->db->query($sql)->result();
	}
	public function get_menu_active_as_object_by_group_id($id) {
		$sql = "
			select
			a.ref_id_menu as id,
			a.ref_parent_id_menu as parent_id,
			a.ref_name_menu as title,
			a.ref_desc_menu as description,
			a.ref_url_menu as link,
			a.ref_position_menu as position,
			a.ref_icon_menu as icon
			from ref_group_access x
			inner join ref_menu a on x.ref_id_menu_gaccess = a.ref_id_menu
			where x.ref_id_group_gaccess = {$id} and x.sts_active = 1
			order by a.ref_position_menu asc
		";

		return $this->db->query($sql)->result_array();
	}
	public function get_menu_as_object_by_group_id($id) {
		$sql = "
			select
			x.ref_id_gaccess,
			(
				select
				ref_name_menu
				from ref_menu
				where ref_id_menu = x.ref_id_menu_gaccess
			)ref_name_menu,
			x.sts_active status
			from ref_group_access x
			where x.ref_id_group_gaccess = {$id}
		";

		return $this->db->query($sql)->result();
	}

	public function get_all_by_column_as_object($column, $value) {
		$where[$column] = $value;
		return $this->db->get_where($this->table, $where)->result();
	}
	public function get_one_all_by_column_as_object($column, $value) {
		$where[$column] = $value;
		return $this->db->get_where($this->table, $where)->row();
	}
	public function get_one_as_object_by_id($id) {
		$sql = "
			select
			*
			from {$this->table}
			where id={$id}
			limit 1
		";
		return $this->db->query($sql)->row();
	}

	public function insert($data) {
		$this->db
			->insert($this->table, $data);
		return $this->db->insert_id();
	}
	public function insert_batch($data) {
		$this->db
			->insert_batch($this->table, $data);
		return TRUE;
	}
	public function update_by_column($column, $value, $data) {
		$this->db->where($column, $value);
		return $this->db->update($this->table, $data);
	}
	public function update_by_id($id, $data) {
		$this->db->where("id", $id);
		return $this->db->update($this->table, $data);
	}
	public function update_batch($data, $id) {
		$this->db
			->update_batch($this->table, $data, $id);
		return TRUE;
	}
	public function delete($value, $column = "") {
		$column = (empty($column)) ? $this->primaryKey : $column;
		$this->db->where($column, $value);
		$query = $this->db->delete($this->table);
		return $query;
	}

}
