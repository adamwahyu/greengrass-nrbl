<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

    public function get_indikator_warna_perumahan() {
        $sql = "
            select
            x.*,
            REPLACE(lower(x.cluster_name), ' ', '_') as keyy
            from(
                    select 'Blue Sapphire' as cluster_name,warna, 'BS' as keyword,
                                                    count(1) total_perumahan,
                                                    (select sum(func_get_tagihan(a.kode_unit))) total
                            from (
                                    SELECT
                                            a.kode_unit,
                                             case when (func_get_jmltagihan(a.kode_unit)) = 0 then 'Green'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 0
                                                                                            and
                                                                                     (func_get_jmltagihan(a.kode_unit)) <= 2 then 'Yellow'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 2 then 'Red'
                                                                    end as
                                                            warna
                                            from ref_unit a
                                    where 
                                    a.sts_active = 1 
                                    and a.kode_unit like ('BS%'))a
                    GROUP BY warna
                    UNION ALL
                    select 'Red Sapphire' as cluster_name,warna, 'RS' as keyword,
                                                    count(1) total_perumahan,
                                                    (select sum(func_get_tagihan(a.kode_unit))) total
                            from (
                                    SELECT
                                            a.kode_unit,
                                             case when (func_get_jmltagihan(a.kode_unit)) = 0 then 'Green'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 0
                                                                                            and
                                                                                     (func_get_jmltagihan(a.kode_unit)) <= 2 then 'Yellow'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 2 then 'Red'
                                                                    end as
                                                            warna
                                            from ref_unit a
                                    where 
                                    a.sts_active = 1 
                                    and a.kode_unit like ('RS%'))a
                    GROUP BY warna
                    UNION ALL
                    select 'Jade' as cluster_name,warna, 'JD' as keyword,
                                                    count(1) total_perumahan,
                                                    (select sum(func_get_tagihan(a.kode_unit))) total
                            from (
                                    SELECT
                                            a.kode_unit,
                                             case when (func_get_jmltagihan(a.kode_unit)) = 0 then 'Green'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 0
                                                                                            and
                                                                                     (func_get_jmltagihan(a.kode_unit)) <= 2 then 'Yellow'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 2 then 'Red'
                                                                    end as
                                                            warna
                                            from ref_unit a
                                    where 
                                    a.sts_active = 1 
                                    and a.kode_unit like ('JD%'))a
                    GROUP BY warna
                    UNION ALL
                    select 'Ruby' as cluster_name,warna, 'RB' as keyword,
                                                    count(1) total_perumahan,
                                                    (select sum(func_get_tagihan(a.kode_unit))) total
                            from (
                                    SELECT
                                            a.kode_unit,
                                             case when (func_get_jmltagihan(a.kode_unit)) = 0 then 'Green'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 0
                                                                                            and
                                                                                     (func_get_jmltagihan(a.kode_unit)) <= 2 then 'Yellow'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 2 then 'Red'
                                                                    end as
                                                            warna
                                            from ref_unit a
                                    where 
                                    a.sts_active = 1 
                                    and a.kode_unit like ('RB%'))a
                    GROUP BY warna
                    UNION ALL
                    select 'Jade' as cluster_name,warna, 'JD' as keyword,
                                                    count(1) total_perumahan,
                                                    (select sum(func_get_tagihan(a.kode_unit))) total
                            from (
                                    SELECT
                                            a.kode_unit,
                                             case when (func_get_jmltagihan(a.kode_unit)) = 0 then 'Green'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 0
                                                                                            and
                                                                                     (func_get_jmltagihan(a.kode_unit)) <= 2 then 'Yellow'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 2 then 'Red'
                                                                    end as
                                                            warna
                                            from ref_unit a
                                    where 
                                    a.sts_active = 1 
                                    and a.kode_unit like ('JD%'))a
                    GROUP BY warna
                    UNION ALL
                    select 'White Diamond' as cluster_name,warna, 'WD' as keyword,
                                                    count(1) total_perumahan,
                                                    (select sum(func_get_tagihan(a.kode_unit))) total
                            from (
                                    SELECT
                                            a.kode_unit,
                                             case when (func_get_jmltagihan(a.kode_unit)) = 0 then 'Green'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 0
                                                                                            and
                                                                                     (func_get_jmltagihan(a.kode_unit)) <= 2 then 'Yellow'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 2 then 'Red'
                                                                    end as
                                                            warna
                                            from ref_unit a
                                    where 
                                    a.sts_active = 1 
                                    and a.kode_unit like ('WD%'))a
                    GROUP BY warna
                    UNION ALL
                    select 'Red Diamond' as cluster_name,warna, 'RD' as keyword,
                                                    count(1) total_perumahan,
                                                    (select sum(func_get_tagihan(a.kode_unit))) total
                            from (
                                    SELECT
                                            a.kode_unit,
                                             case when (func_get_jmltagihan(a.kode_unit)) = 0 then 'Green'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 0
                                                                                            and
                                                                                     (func_get_jmltagihan(a.kode_unit)) <= 2 then 'Yellow'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 2 then 'Red'
                                                                    end as
                                                            warna
                                            from ref_unit a
                                    where 
                                    a.sts_active = 1 
                                    and a.kode_unit like ('RD%'))a
                    GROUP BY warna
                    UNION ALL
                    select 'Sapphire Boulevard' as cluster_name,warna, 'SB' as keyword,
                                                    count(1) total_perumahan,
                                                    (select sum(func_get_tagihan(a.kode_unit))) total
                            from (
                                    SELECT
                                            a.kode_unit,
                                             case when (func_get_jmltagihan(a.kode_unit)) = 0 then 'Green'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 0
                                                                                            and
                                                                                     (func_get_jmltagihan(a.kode_unit)) <= 2 then 'Yellow'
                                                                    when (func_get_jmltagihan(a.kode_unit)) > 2 then 'Red'
                                                                    end as
                                                            warna
                                            from ref_unit a
                                    where 
                                    a.sts_active = 1 
                                    and a.kode_unit like 'SB%' or a.kode_unit like 'In%')a
                    GROUP BY warna
            )x
        ";

        $data = $this->db->query($sql)->result();

        $columns = array_map(function($v) {
            return (object)[
                "column" => $v->keyy,
                "keyword" => $v->keyword
            ];
        }, $data);

        $cols = [];
        $cols2 = [];
        foreach($columns as $c) {
            if(!in_array($c->column, $cols)) {
                $cols[] = $c->column;
                $cols2[] = $c;
            }
        }
        $columns = $cols2;
        foreach($columns as $d) {
            $merah = array_values(array_filter($data, function($v) use ($d) {
                return strtolower($v->warna) == strtolower("Red") && strtolower($v->keyy) == strtolower($d->column);
            }));
            $kuning = array_values(array_filter($data, function($v) use ($d) {
                return strtolower($v->warna) == strtolower("Yellow") && strtolower($v->keyy) == strtolower($d->column);
            }));
            $hijau = array_values(array_filter($data, function($v) use ($d) {
                return strtolower($v->warna) == strtolower("Green") && strtolower($v->keyy) == strtolower($d->column);
            }));
            $dd[$d->column] = (object)[
                "name" => ucfirst(str_replace("_", " ", $d->column)),
                "merah" => (isset($merah[0])) ? $merah[0]->total_perumahan: 0,
                "merah_total" => (isset($merah[0])) ? $merah[0]->total: 0,
                "kuning" => (isset($kuning[0])) ? $kuning[0]->total_perumahan: 0,
                "kuning_total" => (isset($kuning[0])) ? $kuning[0]->total: 0,
                "hijau" => (isset($hijau[0])) ? $hijau[0]->total_perumahan: 0,
                "hijau_total" => (isset($hijau[0])) ? $hijau[0]->total: 0,
                "keyword" => $d->keyword
            ];
        }
        return (object) $dd;

    }

    public function get_datatables_indikator_warna_perumahan($like = null, $length = null, $start, $count = NULL) {
        $keyword = $this->input->get("keyword");
        switch ($this->input->get("category")) {
            case 'merah':
                $sql = "
                    select
                    x.*,
                    func_get_jmltagihan(x.kode_unit) jml_tagihan,
                    func_get_tagihan(x.kode_unit) total,
                    (
                        select
                        desc_sts_unit
                        from ref_status_unit
                        where id_sts_unit = x.id_sts_unit
                    ) id_sts_unit_desc
                    from ref_unit x
                    where sts_active = 1
                    and (kode_unit like '{$keyword}%')
                    and func_get_jmltagihan(x.kode_unit) > 2
                ";
                break;
            case 'kuning':
                $sql = "
                    select
                    x.*,
                    func_get_jmltagihan(x.kode_unit) jml_tagihan,
                    func_get_tagihan(x.kode_unit) total,
                    (
                        select
                        desc_sts_unit
                        from ref_status_unit
                        where id_sts_unit = x.id_sts_unit
                    ) id_sts_unit_desc
                    from ref_unit x
                    where sts_active = 1
                    and (kode_unit like '{$keyword}%')
                    and func_get_jmltagihan(x.kode_unit) > 0
                    and func_get_jmltagihan(x.kode_unit) <= 2
                ";
                break;
            case 'hijau':
                $sql = "
                    select
                    x.*,
                    func_get_jmltagihan(x.kode_unit) jml_tagihan,
                    func_get_tagihan(x.kode_unit) total,
                    (
                        select
                        desc_sts_unit
                        from ref_status_unit
                        where id_sts_unit = x.id_sts_unit
                    ) id_sts_unit_desc
                    from ref_unit x
                    where sts_active = 1
                    and (kode_unit like '{$keyword}%')
                    and func_get_jmltagihan(x.kode_unit) = 0
                ";
                break;
            default:
                throw new Exception("Error Processing Request", 1);
                break;
        }

        $sql_count = "select count(*) from({$sql}) x where 1=1";

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
                "x.rownum",
                "x.kode_unit"
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
                $sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";
        $sql = "
            SELECT
            x.*,
            row_number() over(ORDER BY x.kode_unit DESC) as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";

        if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.kode_unit ||
                x.nama_pemilik ||
								x.luas_unit ||
								x.nomor_va_narobil ||
								x.nomor_va_bca ||
								x.id_sts_unit_desc ||
								x.jml_tagihan
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }
        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
    }

    public function get_indikator_warna_perumahan_all() {
        $sql = "
                SELECT
                    warna,
                    COUNT(1) total_perumahan,
                    (SELECT SUM(func_get_tagihan(A.kode_unit))) total 
                FROM
                    (
                    SELECT A.kode_unit,
                    CASE
                        WHEN ( func_get_jmltagihan ( A.kode_unit ) ) = 0 
                            THEN 'Green' 
                        WHEN ( func_get_jmltagihan ( A.kode_unit ) ) > 0 
                        AND ( func_get_jmltagihan ( A.kode_unit ) ) <= 2 
                            THEN 'Yellow' 
                        WHEN ( func_get_jmltagihan ( A.kode_unit ) ) > 2 
                            THEN 'Red' 
                        END AS warna 
                        FROM
                            ref_unit A 
                        ) A 
                GROUP BY
                    warna
        ";

        $data = $this->db->query($sql)->result();

        $columns = $data;


        foreach($columns as $d) {
            $merah = array_values(array_filter($data, function($v) use ($d) {
                return strtolower($v->warna) == strtolower("Red");
            }));
            $kuning = array_values(array_filter($data, function($v) use ($d) {
                return strtolower($v->warna) == strtolower("Yellow");
            }));
            $hijau = array_values(array_filter($data, function($v) use ($d) {
                return strtolower($v->warna) == strtolower("Green");
            }));
            $dd = (object)[
                "merah" => (isset($merah[0])) ? $merah[0]->total_perumahan: 0,
                "merah_total" => (isset($merah[0])) ? $merah[0]->total: 0,
                "kuning" => (isset($kuning[0])) ? $kuning[0]->total_perumahan: 0,
                "kuning_total" => (isset($kuning[0])) ? $kuning[0]->total: 0,
                "hijau" => (isset($hijau[0])) ? $hijau[0]->total_perumahan: 0,
                "hijau_total" => (isset($hijau[0])) ? $hijau[0]->total: 0
            ];
        }
        return (object) $dd;

    }

    public function get_datatables_indikator_warna_perumahan_all($like = null, $length = null, $start, $count = NULL) {
        $keyword = $this->input->get("keyword");
        switch ($this->input->get("category")) {
            case 'merah':
                $sql = "
                    select
                    x.*,
                    func_get_jmltagihan(x.kode_unit) jml_tagihan,
                    func_get_tagihan(x.kode_unit) total,
                    (
                        select
                        desc_sts_unit
                        from ref_status_unit
                        where id_sts_unit = x.id_sts_unit
                    ) id_sts_unit_desc
                    from ref_unit x
                    where x.sts_active = 1
                    and func_get_jmltagihan(x.kode_unit) > 2
                ";
                break;
            case 'kuning':
                $sql = "
                    select
                    x.*,
                    func_get_jmltagihan(x.kode_unit) jml_tagihan,
                    func_get_tagihan(x.kode_unit) total,
                    (
                        select
                        desc_sts_unit
                        from ref_status_unit
                        where id_sts_unit = x.id_sts_unit
                    ) id_sts_unit_desc
                    from ref_unit x
                    where x.sts_active = 1
                    and func_get_jmltagihan(x.kode_unit) > 0
                    and func_get_jmltagihan(x.kode_unit) <= 2
                ";
                break;
            case 'hijau':
                $sql = "
                    select
                    x.*,
                    func_get_jmltagihan(x.kode_unit) jml_tagihan,
                    func_get_tagihan(x.kode_unit) total,
                    (
                        select
                        desc_sts_unit
                        from ref_status_unit
                        where id_sts_unit = x.id_sts_unit
                    ) id_sts_unit_desc
                    from ref_unit x
                    where x.sts_active = 1
                    and func_get_jmltagihan(x.kode_unit) = 0
                ";
                break;
            default:
                throw new Exception("Error Processing Request", 1);
                break;
        }

        $sql_count = "select count(*) from({$sql}) x where 1=1";

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
                "x.rownum",
                "x.kode_unit"
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
                $sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";

        $sql = "
            SELECT
            x.*,
            row_number() over(ORDER BY x.kode_unit DESC) as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";
        
        if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.kode_unit ||
                x.nama_pemilik ||
                                x.luas_unit ||
                                x.nomor_va_narobil ||
                                x.nomor_va_bca ||
                                x.id_sts_unit_desc ||
                                x.jml_tagihan
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
    }

}
