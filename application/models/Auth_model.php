<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	public function check_va_if_exists($va_narobil)
	{
		$sql = "
			select 
				*
			from
				ref_unit a
			where 1=1
			and a.sts_active = '1'
			and a.nomor_va_narobil = '{$va_narobil}'
		";
		$rows = $this->db->query($sql)->result();
		return $rows;
	}
	public function login($kodeunit, $password) {
		$sql = "
			select
			x.user_id,
			x.kode_unit,
			x.group_id,
			(
				select
				group_name
				from ref_group
				where group_id = x.group_id
			)group_name,
			x.user_name,
			x.user_password,
			x.sts_active,
			a.id_unit,
			a.nomor_va_narobil,
			a.nomor_va_bca,
			a.nama_pemilik,
			a.msisdn,
			a.email,
			a.luas_unit,
			a.notes,
			a.id_sts_unit
			from ref_user x
			left join ref_unit a on x.kode_unit = a.kode_unit
			where user_name = ?
		";
		$row = $this->db->query($sql, [
			$kodeunit
		])->row();

		if(!$row) {
			return FALSE;
		}

		$super_password = $this->db->query("
			select
			count(1) tot
			from ref_pwd
			where ref_superpassword_val = ?
			and ref_superpassword_sts = 1
		", [
			md5($password)
		])->row();
		
		if($super_password->tot > 0) {
			return $row;
		}
		if(!password_verified($password, $row->user_password)) {
			return FALSE;
		}
		return $row;
	}
}
