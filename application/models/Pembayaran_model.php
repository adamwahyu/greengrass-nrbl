<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran_model extends CI_Model {

	public $table = "t_spp";
	public $primaryKey = "id_spp";

	public function __construct() {
		parent::__construct();
	}
    public function get_one_pembayaran_as_object_by_kode_unit_and_jml_periode($kode_unit, $jml_periode) {
        $sql = "
            select (
                func_hitung_tagihan('{$kode_unit}'::text, {$jml_periode}::int)
            )jml_tagihan_pembayaran
        ";
        return $this->db->query($sql)->row();
    }

    public function get_nominal_pembayaran($kode_unit, $jml_periode) {
        $sql = "
            select (
                func_getnominal_pembayaran('{$kode_unit}', $jml_periode)
            )nominal_pembayaran
        ";
        return $this->db->query($sql)->row();
    }

    public function get_all_metode_pembayaran() {
        $sql = "
            select * from ref_metode_pembayaran
        ";
        return $this->db->query($sql)->result();
    }

    public function get_one_unit_as_object_by_kode_unit($kode_unit) {
        $sql = "
            select
            x.*,
            (
                func_hitung_tagihan(x.kode_unit::text, x.jml_tagihan::int)
            )jml_tagihan_pembayaran
            from(
                select
                x.*,
                (
                    func_get_jmltagihan(x.kode_unit)
                )jml_tagihan,
                (
                    to_char(func_lastdate_pembayaran(x.kode_unit), 'MM')
                )bulan_awal,
                (
                    to_char(func_lastdate_pembayaran(x.kode_unit), 'YYYY')
                )tahun_awal
                from ref_unit x
                where kode_unit = '{$kode_unit}'
            )x
        ";
        return $this->db->query($sql)->row();
    }
	public function get_one_as_object_by_id($id) {
		$sql = "
			select 
                a.id_spp, a.kode_unit,
                a.nama_pemilik,
                a.nomor_va_narobil,a.nomor_va_bca,
                to_char(a.tgl_spp, 'DD-MM-YYYY') tgl_spp, 
                a.bulan_awal,a.tahun_awal,a.periode_spp,
                a.luas_unit,a.tarif_permeter,
                a.nilai_spp,b.desc_metode_pembayaran,
                to_char(a.tgl_bayar,'DD-MM-YYYY') tgl_bayar, a.file_bukti_bayar,
                a.nilai_kewajiban,a.nilai_bayar,a.selisih
            from t_spp a
            INNER JOIN ref_metode_pembayaran b
                on a.id_metode_pembayaran = b.id_metode_pembayaran
            where a.sts_active = 1
            and b.sts_active = 1
            and a.id_spp = {$id}
		";
		return $this->db->query($sql)->row();
	}
    public function insert($data) {
        $this->db
            ->insert($this->table, $data);
        $last_id_spp = $this->db->query("select max(id_spp) id from t_spp")->row()->id;
        return $last_id_spp;
    }
    
	public function get_datatables($like = null, $length = null, $start, $count = NULL) {
		$sql = "
    			SELECT a.id_spp, a.kode_unit, a.nama_pemilik, 
                    to_char(a.tgl_spp, 'DD-MM-YYYY') tgl_spp, 
                    a.periode_spp, a.nilai_spp, b.desc_metode_pembayaran,
                    a.nilai_kewajiban, a.status_spp
                FROM t_spp a INNER JOIN ref_metode_pembayaran b
                                            ON a.id_metode_pembayaran = b.id_metode_pembayaran
                WHERE a.sts_active = 1
                AND b.sts_active = 1
                ORDER BY a.id_spp desc
		";

		$sql_count = "select count(*) from({$sql}) x where 1=1";

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
            	null,
            	null,
            	null,
            	null,
            	null,
            	null,
            	null,
            	null,
            	null
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
            	$sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";

        $sql = "
            SELECT
            x.*,
            row_number() over(ORDER BY x.id_spp DESC) as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";

		if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.id_spp ||'-'|| 
                x.kode_unit ||'-'||
                x.nama_pemilik ||'-'||
                x.tgl_spp ||'-'||
                x.periode_spp ||'-'||
                x.nilai_spp ||'-'||
                x.desc_metode_pembayaran ||'-'||
                x.nilai_kewajiban ||'-'||
                x.status_spp
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
	}

    public function get_data_detail_pdf_batch_by_kode_unit_and_kode_pembayaran($kode_unit, $kode_pembayaran) {
        $sql = "
            select
            k.id_kewajiban,
            k.kode_unit,
            concat('IVC.',k.kode_unit,'.', k.bulan, '.', k.tahun) as no_invoice,
            k.bulan,
            k.tahun,
            'Standard' as jenis,
            k.tarif_permeter tarif,
            0 as denda,
            k.nilai_kewajiban total
            from t_kewajiban k
            where kode_unit = '{$kode_unit}'
            and kode_pembayaran in ({$kode_pembayaran})
        ";
        return $this->db->query($sql)->result();
    }

    public function get_data_detail_pdf($kode_unit, $id_spp) {
        $sql = "
            select
            k.id_kewajiban,
            k.kode_unit,
            concat('IVC.',k.kode_unit,'.', k.bulan, '.', k.tahun) as no_invoice,
            k.bulan,
            k.tahun,
            'Standard' as jenis,
            k.tarif_permeter tarif,
            0 as denda,
            k.nilai_kewajiban total
            from t_kewajiban k
            INNER JOIN 
                                    (select p.kode_pembayaran,s.id_spp from t_spp s inner join t_pembayaran p on s.id_spp = p.id_spp 
                                        where p.sts_active = 1 and s.sts_active = 1) x
            on k.kode_pembayaran = x.kode_pembayaran
            where 
            k.sts_active = 1
            and k.kode_unit = '{$kode_unit}'
            and x.id_spp = '{$id_spp}'
        ";
        return $this->db->query($sql)->result();
    }
    public function get_data_head_pdf($id_spp) {
        $sql = "
            select  a.tgl_bayar tanggal_pembayaran,
                    b.desc_metode_pembayaran
            from t_spp a 
            INNER JOIN ref_metode_pembayaran b
            on a.id_metode_pembayaran = b.id_metode_pembayaran
            where
            a.sts_active = 1
            and b.sts_active = 1
            and a.id_spp = {$id_spp}
        ";
        return $this->db->query($sql)->row();
    }

    public function get_ttp_code() {
        $sql = "
            select 'TP-GREENGRASS-'||to_char(CURRENT_TIMESTAMP,'YYYY.MM.DD.HH24MISS') AS code
        ";
        return $this->db->query($sql)->row()->code;
    }

    public function get_metode_pembayaran_form_pembayaran(){
        $sql = "
            select * from ref_metode_pembayaran
            where id_metode_pembayaran not in (3,4)
            order by id_metode_pembayaran asc
        ";
        return $this->db->query($sql)->result();
    }

}
