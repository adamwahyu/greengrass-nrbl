<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Check_va_unit_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
    public function get_datatables($like = null, $length = null, $start, $count = NULL) {
        $sql = "
				select id_unit,kode_unit, nama_pemilik,
				nomor_va_narobil,
				nomor_va_bca,
				kode_unit_,
			 (select b.nama_cluster from ref_cluster b where a.id_cluster=b.id_cluster)as unit_cluster
				from (
						select kode_unit, id_unit, nama_pemilik,
						nomor_va_narobil,
						nomor_va_bca,
						substr(kode_unit, 1, 2)kode_unit_,
						id_cluster
						from ref_unit
				)a
        ";

        $sql_count = "select count(*) from({$sql}) x where 1=1";


        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
                "x.rownum",
                "x.kode_unit"
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
                $sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";
        $sql = "
            SELECT
            x.*,
            row_number() over(ORDER BY x.kode_unit DESC) as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";
        if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.kode_unit
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT abs($length) OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
    }

}
