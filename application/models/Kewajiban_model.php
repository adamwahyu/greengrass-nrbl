<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kewajiban_model extends CI_Model {

	protected $table = "t_kewajiban";
	protected $primaryKey = "id_kewajiban";

	public function __construct() {
		parent::__construct();
	}
	
	public function get_all() {
		$sql = "
			select
			*
			from {$this->table}
		";
		return $this->db->query($sql)->result();
	}
	public function get_all_by_column_as_object($column, $value) {
		$where[$column] = $value;
		return $this->db->get_where($this->table, $where)->result();
	}
	public function get_one_all_by_column_as_object($column, $value) {
		$where[$column] = $value;
		return $this->db->get_where($this->table, $where)->row();
	}
	public function get_one_as_object_by_id($id) {
		$sql = "
			select
			*
			from {$this->table}
			where ".$this->primaryKey."={$id}
			limit 1
		";
		return $this->db->query($sql)->row();
	}

	public function insert($data) {
		$this->db
			->insert($this->table, $data);
		return $this->db->insert_id();
	}
	public function insert_batch($data) {
		$this->db
			->insert_batch($this->table, $data);
		return TRUE;
	}
	public function update_by_column($column, $value, $data) {
		$this->db->where($column, $value);
		return $this->db->update($this->table, $data);
	}
	public function update_by_id($id, $data) {
		$this->db->where("id", $id);
		return $this->db->update($this->table, $data);
	}
	public function update_batch($data, $id) {
		$this->db
			->update_batch($this->table, $data, $id);
		return TRUE;
	}
	public function delete($value, $column = "") {
		$column = (empty($column)) ? $this->primaryKey : $column;
		$this->db->where($column, $value);
		$query = $this->db->delete($this->table);
		return $query;
	}

	public function delete_soft($id_kewajiban) {
		$data = array(
			'status_kewajiban' => 'OPEN',
			'nilai_pemenuhan' => 0,
			'tgl_pemenuhan' => null,
			'id_metode_pembayaran' => null,
			'update_by' => $this->session->userdata("user")->user_id,
			"updated_date" => date("Y-m-d H:i:s")
		);

		$this->db->where('id_kewajiban', $id_kewajiban);
		$query = $this->db->update($this->table, $data);
		return $query;
	}

}
