<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_warga_model extends CI_Model {

	protected $table = "ref_unit";
	protected $primaryKey = "id";

	public function __construct() {
		parent::__construct();
	}

	public function get_all_cs() {
		$sql = "
			select
			x.*,
			case
			when((
				select
				count(*)
				from ref_user
				where kode_unit =x.kode_unit
			) > 0)
			then 'Y'
			else 'N'
			end as sts_disabled
			from ref_unit x
		";
		return $this->db->query($sql)->result();
	}

	public function get_all_custom_by_kodeunit($id_unit) {
		$sql = "
			select a.id_unit,a.kode_unit, a.nama_pemilik, a.luas_unit,
			       a.nomor_va_narobil, a.nomor_va_bca,
						 a.msisdn, a.email,
						 (select x.desc_sts_unit from ref_status_unit x
						  where x.sts_active = 1
						  and x.id_sts_unit = a.id_sts_unit limit 1) sts_huni,
						 (select (x.tarif_permeter * a.luas_unit)
						  from ref_tarif x
							where x.sts_active = 1) tarif,
						 (select x.jml_pembayaran
						 from t_pembayaran x
						 where x.id_pembayaran = (select max(z.id_pembayaran)
						                          from t_pembayaran z
																			where (z.kode_billing = a.nomor_va_narobil)
																			or z.kode_billing = a.nomor_va_bca)) last_transaksi_amount,
						 (select x.created_date
						 from t_pembayaran x
						 where x.id_pembayaran = (select max(z.id_pembayaran)
						                          from t_pembayaran z
																			where (z.kode_billing = a.nomor_va_narobil)
																			or z.kode_billing = a.nomor_va_bca)) last_transaksi_date,
																			a.notes, func_get_tagihan(a.kode_unit) nilai_tunggakan
			from ref_unit a
			where a.sts_active = 1
			and a.id_unit = '{$id_unit}'
			order by a.kode_unit
		";
		return $this->db->query($sql)->row();
	}

	public function get_vanonrutin($kode_unit) {
		// $kode_unit = $this->session->userdata("user")->kode_unit;

		$sql = "SELECT
					payment_code va_sementara,name as kode_unit,
					description,amount as nilai_tunggakan
				FROM narobil_pay_collect_billing_list x
				where payment_code not in (select nomor_va_narobil from ref_unit where sts_active = 1)
				and name = '{$kode_unit}'
				order by x.name asc";

				return $this->db->query($sql)->row();
	}

public function get_jumlahtunggakan($kode_unit) {
		// $kode_unit = $this->session->userdata("user")->kode_unit;

		$sql = "select coalesce(count(1),0)AS count from t_kewajiban 
where status_kewajiban ='OPEN' and kode_unit='{$kode_unit}'";

				return $this->db->query($sql)->row();
	}


	public function get_datatables($like = null, $length = null, $start, $count = NULL) {
		$kode_unit = $this->session->userdata("user")->kode_unit;
		$sql = "
				select a.id_unit,a.kode_unit, a.nama_pemilik, a.luas_unit,
				       a.nomor_va_narobil, a.nomor_va_bca,
				       		 (select x.desc_sts_unit from ref_status_unit x
							  where x.sts_active = 1
							  and x.id_sts_unit = a.id_sts_unit ) sts_huni,
							 (select (x.tarif_permeter * a.luas_unit)
							  from ref_tarif x
								where x.sts_active = 1) tarif,
								func_get_jmltagihan(a.kode_unit) jmltagihan
				from ref_unit a
				where a.sts_active = 1
				and a.kode_unit = '$kode_unit'
				order by a.kode_unit asc
		";



		$sql_count = "select count(*) from({$sql}) x where 1=1";


        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
            	"x.rownum",
            	"x.kode_unit",
            	"x.nama_pemilik",
            	"x.luas_unit",
            	"x.nomor_va_narobil",
            	"x.sts_huni",
            	"x.tarif",
            	null
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
            	$sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";

        $sql = "
			SELECT
			x.*,
			row_number() over(ORDER BY x.kode_unit asc) as rownum
            FROM( $sql ) x WHERE 1 = 1
		";

		if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.kode_unit ||
                x.nama_pemilik ||
                x.luas_unit ||
                x.nomor_va_narobil ||
                x.sts_huni ||
                x.tarif
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
	}

	public function get_datatables_trans_kewajiban($like = null, $length = null, $start, $count = NULL) {
		$kode_unit = $this->session->userdata("user")->kode_unit;

		$sql = "
			select a.id_kewajiban,
			       a.tahun, a.bulan,
						 a.luas_unit, a.tarif_permeter,
						 a.nilai_kewajiban, a.nilai_pemenuhan,
					   case when a.status_kewajiban = 'CLOSE'
						      then 'LUNAS' else 'BELUM DIBAYAR'
									end status_kewajiban
			from t_kewajiban a
			where a.sts_active = 1
			and a.kode_unit = '{$kode_unit}'
			order by tahun desc, bulan desc
		";

		$sql_count = "select count(*) from({$sql}) x where 1=1";

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
            	"x.rownum",
            	"x.tahun",
            	"x.bulan",
            	"x.luas_unit",
            	"x.tarif_permeter",
            	"x.nilai_kewajiban",
            	"x.nilai_pemenuhan",
            	"x.status_kewajiban"
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
            	$sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";

        $sql = "
			SELECT
			x.*,
			row_number() over() as rownum
            FROM( $sql ) x WHERE 1 = 1
		";

		if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.tahun ||'-'||
                x.bulan ||'-'||
                x.luas_unit ||'-'||
                x.tarif_permeter ||'-'||
                x.nilai_kewajiban ||'-'||
                x.nilai_pemenuhan ||'-'||
                x.status_kewajiban
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
	}

	public function get_trans_kewajiban_tunggakan_by_kodeunit($kode_unit) {

		$sql = "
			select a.id_kewajiban,
			       a.tahun, a.bulan,
						 a.luas_unit, a.tarif_permeter,
						 a.nilai_kewajiban, a.nilai_pemenuhan,
					   case when a.status_kewajiban = 'CLOSE'
						      then 'LUNAS' else 'BELUM DIBAYAR'
									end status_kewajiban
			from t_kewajiban a
			where a.sts_active = 1
			and a.kode_unit = '{$kode_unit}'
			and a.status_kewajiban = 'OPEN'
			order by a.tgl_kewajiban desc
		";
		return $this->db->query($sql)->result();
	}

	public function get_all() {
		$sql = "
			select
			*
			from {$this->table}
		";
		return $this->db->query($sql)->result();
	}
	public function get_all_by_column_as_object($column, $value) {
		$where[$column] = $value;
		return $this->db->get_where($this->table, $where)->result();
	}
	public function get_one_all_by_column_as_object($column, $value) {
		$where[$column] = $value;
		return $this->db->get_where($this->table, $where)->row();
	}
	public function get_one_as_object_by_kodeunit($id_unit) {
		$sql = "
			select
			*
			from {$this->table}
			where id_unit = '{$id_unit}'
		";
		return $this->db->query($sql)->row();
	}

	public function get_hunian() {
		$sql = "
			select
			*
			from ref_status_unit
			where sts_active = 1
		";
		return $this->db->query($sql)->result();
	}
	public function get_one_as_object_by_id($id) {
		$sql = "
			select
			*
			from {$this->table}
			where id={$id}
			limit 1
		";
		return $this->db->query($sql)->row();
	}

}
