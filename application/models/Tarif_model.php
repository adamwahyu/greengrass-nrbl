<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarif_model extends CI_Model {

	protected $table = "ref_tarif";
	protected $primaryKey = "id_tarif";

	public function __construct() {
		parent::__construct();
	}

	public function get_tarif() {
		$sql = "
			select a.tarif_permeter
			from ref_tarif a
			where a.id_tarif = (select max(x.id_tarif)
			                    from ref_tarif x
								where x.sts_active = 1)
		";

		return $this->db->query($sql)->row();
	}
	public function get_datatables($like = null, $length = null, $start, $count = NULL) {
		$sql = "
			select x.id_tarif,
			x.tarif_permeter,
			x.sts_active,
			x.created_date,
			x.updated_date
			from ref_tarif x where x.sts_active =1
		";

		$sql_count = "select count(*) from({$sql}) x where 1=1";
		
        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
            	"x.rownum",
            	"x.tarif_permeter",

            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
            	$sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";
        $sql = "
			SELECT
			x.*,
			row_number() over(ORDER BY x.created_date DESC) as rownum
            FROM( $sql ) x WHERE 1 = 1
		";
		
		if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.tarif_permeter::varchar(100)
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
	}

	public function get_all_status_active() {
		$sql = "
			select
			*
			from {$this->table}
			where sts_active = 1
		";
		return $this->db->query($sql)->result();
	}

	public function get_all() {
		$sql = "
			select
			*
			from {$this->table}
		";
		return $this->db->query($sql)->result();
	}
	public function get_all_by_column_as_object($column, $value) {
		$where[$column] = $value;
		return $this->db->get_where($this->table, $where)->result();
	}
	public function get_one_all_by_column_as_object($column, $value) {
		$where[$column] = $value;
		return $this->db->get_where($this->table, $where)->row();
	}
	public function get_one_as_object_by_id($id) {
		$sql = "
			select
			*
			from {$this->table}
			where ref_id_menu={$id}
			limit 1
		";
		return $this->db->query($sql)->row();
	}

	public function insert($data) {
		$this->db
			->insert($this->table, $data);
		return $this->db->insert_id();
	}
	public function insert_batch($data) {
		$this->db
			->insert_batch($this->table, $data);
		return TRUE;
	}
	public function update_disabled() {
		$user_id = $this->session->userdata("user")->user_id;
		$id_tarif = $this->db->query("select max(id_tarif) id from ref_tarif")->row()->id;
		$sql = "
	      	update ref_tarif set sts_active = 0
	    ";
	    $this->db->query($sql);

	    $sql = "
	      	update ref_tarif set update_by = ".$user_id." where id_tarif = ".$id_tarif."
	    ";
	    return $this->db->query($sql);
  }
}
