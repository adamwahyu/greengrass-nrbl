<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History_pembayaran_model extends CI_Model {

	public $table = "t_pembayaran";
	public $primaryKey = "id_pembayaran";

	public function __construct() {
		parent::__construct();
	}
	public function get_one_as_object_by_id($id) { 
		$sql = "
			SELECT
                a.id_pembayaran, 
                a.kode_pembayaran, a.kode_unit,
                a.nomor_va_narobil, a.nomor_va_bca, 
                to_char(a.tgl_pembayaran,'DD-MM-YYYY hh24:mi:ss') tgl_pembayaran, a.jml_pembayaran,
                a.status_pembayaran,a.notes,
                coalesce((select b.desc_metode_pembayaran from ref_metode_pembayaran b where a.id_metode_pembayaran = b.id_metode_pembayaran),'-') desc_metode_pembayaran
            FROM t_pembayaran a
            WHERE a.sts_active = 1
            and a.id_pembayaran = '{$id}'
		";
		return $this->db->query($sql)->row();
	}
	public function get_datatables($like = null, $length = null, $start, $stardate, $enddate, $count = NULL) {
        $where = '';
        if ($stardate == $enddate){
            $where = "to_char(tgl_pembayaran,'MM/DD/YYYY') = '{$stardate}'";
        }else{
            $where = "to_char(tgl_pembayaran,'MM/DD/YYYY') >= '{$stardate}' and to_char(tgl_pembayaran,'MM/DD/YYYY') <= '{$enddate}'";
        };
		$sql = "
    			SELECT
                    a.id_pembayaran, 
                    a.kode_pembayaran, coalesce(a.kode_unit,'-') kode_unit, 
                    to_char(a.tgl_pembayaran,'DD-MM-YYYY HH24:MI:SS') tgl_pembayaran, a.jml_pembayaran,
                    coalesce((select b.desc_metode_pembayaran from ref_metode_pembayaran b where a.id_metode_pembayaran = b.id_metode_pembayaran),'-') desc_metode_pembayaran
                FROM t_pembayaran a
                WHERE a.sts_active = 1
                and $where
                order by a.tgl_pembayaran desc
		";

		$sql_count = "select count(*) from({$sql}) x where 1=1";

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
            	null,
            	null,
            	null,
            	null,
            	null,
            	null
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
            	$sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";

        $sql = "
            SELECT
            x.*,
            row_number() over() as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";

		if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.kode_pembayaran ||'-'||
                x.kode_unit ||'-'||
                x.tgl_pembayaran ||'-'||
                x.jml_pembayaran ||'-'||
                x.desc_metode_pembayaran
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
	}


    public function get_history_excel($stardate, $enddate){
        $where = '';
        if ($stardate == $enddate){
            $where = "to_char(a.tgl_pembayaran,'MM/DD/YYYY') = '{$stardate}'";
        }else{
            $where = "to_char(a.tgl_pembayaran,'MM/DD/YYYY') >= '{$stardate}' and to_char(a.tgl_pembayaran,'MM/DD/YYYY') <= '{$enddate}'";
        };
        $sql = "
                SELECT
                    a.id_pembayaran, 
                    a.kode_pembayaran, coalesce(a.kode_unit,'-') kode_unit, 
                    a.nomor_va_narobil,a.nomor_va_bca,
                    to_char(a.tgl_pembayaran,'DD-MM-YYYY HH24:MI:SS') tgl_pembayaran, a.jml_pembayaran,
                    a.status_pembayaran,
                    coalesce((select b.desc_metode_pembayaran from ref_metode_pembayaran b where a.id_metode_pembayaran = b.id_metode_pembayaran),'-') desc_metode_pembayaran,a.notes
                FROM t_pembayaran a
                WHERE a.sts_active = 1
                and $where
                order by a.tgl_pembayaran desc
        ";
        $sql = "
            SELECT
            x.*,
            row_number() over() as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";
        
        return $this->db->query($sql)->result();
    }

}
