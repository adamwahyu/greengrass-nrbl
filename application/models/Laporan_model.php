<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get_mst_tahun_kewajiban($tahun_multiple= NULL) {
		
		if(!is_null($tahun_multiple)) {
			if(is_array($tahun_multiple)) {
				$sql = "
					select
					distinct tahun
					from t_kewajiban
					where tahun::integer in(".implode(",", $tahun_multiple).")
					order by tahun asc
				";
			} else {
				$tahun_multiple = explode(",", $tahun_multiple);
				$sql = "
					select
					distinct tahun
					from t_kewajiban
					where tahun::integer in(".implode(",", $tahun_multiple).")
					order by tahun asc
				";
			}
		} else {
			$sql = "
				select
				distinct tahun
				from t_kewajiban
				order by tahun asc
			";
		}
		
		return $this->db->query($sql)->result();
	}

	public function get_mst_bulan_kewajiban() {
		$sql = "
			select
			*
			from ref_bulan
			order by id_bulan asc
		";
		return $this->db->query($sql)->result();
	}
	public function get_mst_cluster_kewajiban() {
		$sql = "
			select
			*
			from ref_cluster
		";

		return $this->db->query($sql)->result();
	}

	public function get_kewajiban($cluster) {
		if($cluster == "all") {
			$sql = "
				select
				x.kode_unit,
				(
					select
					desc_sts_unit
					from ref_status_unit
					where id_sts_unit = x.id_sts_unit
				) desc_sts_unit
				from ref_unit x
				order by x.kode_unit asc
			";
		} else {
			$sql = "
				select
				x.kode_unit,
				(
					select
					desc_sts_unit
					from ref_status_unit
					where id_sts_unit = x.id_sts_unit
				) desc_sts_unit
				from ref_unit x
				where x.id_cluster = ".$cluster."
				order by x.kode_unit asc
			";
		}
		$sql = "
			SELECT
			x.*,
			row_number() over(ORDER BY x.kode_unit asc) as rownum
            FROM( $sql ) x WHERE 1 = 1
		";
		return $this->db->query($sql)->result();
	}

	public function get_datatables_kewajiban_detail($kode_unit, $bulan, $tahun) {

		$sql = "
			select
			kode_unit,
			sum(func_get_unitamount(kode_unit, '{$tahun}', '{$bulan}')) nilai_pemenuhan
			from t_kewajiban
			where tahun = '{$tahun}'
			and bulan = '{$bulan}'
			and kode_unit = '{$kode_unit}'
			group by kode_unit
		";
		return $this->db->query($sql)->row();
	}

	public function get_datatables_kewajiban($like = null, $length = null, $start, $count = NULL) {

		if($this->input->get("cluster") == "all") {
			$sql = "
				select
				x.kode_unit,
				(
					select
					desc_sts_unit
					from ref_status_unit
					where id_sts_unit = x.id_sts_unit
				) desc_sts_unit
				from ref_unit x
				order by x.kode_unit asc
			";
		} else {
			$sql = "
				select
				x.kode_unit,
				(
					select
					desc_sts_unit
					from ref_status_unit
					where id_sts_unit = x.id_sts_unit
				) desc_sts_unit
				from ref_unit x
				where x.id_cluster = ".$this->input->get("cluster")."
				order by x.kode_unit asc
			";
		}

		$sql_count = "select count(*) from({$sql}) x where 1=1";
		

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
            	// "x.rownum",
            	// "x.ref_name_menu",
            	// "x.ref_parent_name_menu",
            	// "x.ref_url_menu",
            	// "x.ref_icon_menu",
            	// "x.ref_position_menu",
            	// null
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
            	$sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";
        $sql = "
			SELECT
			x.*,
			row_number() over(ORDER BY x.kode_unit asc) as rownum
            FROM( $sql ) x WHERE 1 = 1
		";
		if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.kode_unit
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
	}

}
