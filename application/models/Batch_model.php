<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Batch_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
    
    public function get_datatables_header($like = null, $length = null, $start, $count = NULL) {
        $sql = "
            select
            x.*,
            (
                select
                desc_metode_pembayaran
                from ref_metode_pembayaran
                where id_metode_pembayaran = x.id_metode_pembayaran
            )desc_metode_pembayaran,
            (
                select
                count(*)
                from t_batch_detil where batch_code = x.batch_code
                and batch_detil_status = 'BERHASIL'
            ) jml_berhasil,
            (
                select
                count(*)
                from t_batch_detil where batch_code = x.batch_code
                and batch_detil_status = 'GAGAL' or batch_code = x.batch_code and batch_detil_status is null
            ) jml_gagal
            from t_batch_header x
        ";

        $sql_count = "select count(*) from({$sql}) x where 1=1";
        

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
                "x.rownum",
                "x.batch_code"
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
                $sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];    
            }
        }
        $sql = "select * from({$sql})x where 1=1";
        $sql = "
            SELECT
            x.*,
            row_number() over(ORDER BY x.batch_code DESC) as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";
        if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.batch_code
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
    }

    public function get_datatables_detil($like = null, $length = null, $start, $count = NULL) {
        $sql = "
            select
            *
            from t_batch_detil
            where batch_code = '".$this->input->get("batch_code")."'
        ";

        $sql_count = "select count(*) from({$sql}) x where 1=1";
        

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
                "x.rownum",
                "x.kode_unit",
                "x.transfer_code",
                "x.tgl_transfer",
                "x.amount"
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
                $sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];    
            }
        }
        $sql = "select * from({$sql})x where 1=1";
        $sql = "
            SELECT
            x.*,
            row_number() over(ORDER BY x.batch_code DESC) as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";
        if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.kode_unit ||
                x.transfer_code ||
                x.tgl_transfer ||
                x.amount
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
    }

    public function get_pembayaran_by_batch_code($batch_code, $kode_unit) {
        $sql = "
            select
            *
            from t_pembayaran
            where batch_code = '{$batch_code}'
            and kode_unit = '{$kode_unit}'
        ";
        return $this->db->query($sql)->result();
    }
    public function get_one_batch_detil_by_batch_code($batch_code) {
        $sql = "
            select
            *
            from t_batch_detil
            where batch_code = '{$batch_code}'
            and batch_detil_status != 'GAGAL'
        ";
        return $this->db->query($sql)->result();
    }

    public function get_one_batch_header_by_batch_code($batch_code) {
        $sql = "
            select
            x.*,
            (
                select
                desc_metode_pembayaran
                from ref_metode_pembayaran
                where id_metode_pembayaran = x.id_metode_pembayaran
            )desc_metode_pembayaran
            from t_batch_header x
            where batch_code = '{$batch_code}'
        ";
        return $this->db->query($sql)->row();
    }

    public function get_generate_code() {
        $sql = "select func_generate_batchcode() as code";
        return $this->db->query($sql)->row()->code;
    }

    public function insert_header($data) {
        $this->db
            ->insert("t_batch_header", $data);
        return $this->db->insert_id();
    }
    public function insert_batch_header($data) {
        $this->db
            ->insert_batch("t_batch_header", $data);
        return TRUE;
    }
    public function insert_detail($data) {
        $this->db
            ->insert("t_batch_detil", $data);
        return $this->db->insert_id();
    }
    public function insert_batch_detail($data) {
        $this->db
            ->insert_batch("t_batch_detil", $data);
        return TRUE;
    }
}
