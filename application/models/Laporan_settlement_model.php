<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_settlement_model extends CI_Model {

	public $table = "narobil_pay_collect_trans_hist";
	public $primaryKey = "trace_number";

	public function __construct() {
		parent::__construct();
	}
    
	public function get_datatables($like = null, $length = null, $start, $stardate, $enddate, $count = NULL) {
        $where = '';
        if ($stardate == $enddate){
            $where = "to_char(formatted_transaction_date,'MM/DD/YYYY') = '{$stardate}'";
        }else{
            $where = "to_char(formatted_transaction_date,'MM/DD/YYYY') >= '{$stardate}' and to_char(formatted_transaction_date,'MM/DD/YYYY') <= '{$enddate}'";
        };
		$sql = "
    			SELECT
                    a.trace_number, 
                    a.transaction_number, 
                    to_char(a.formatted_transaction_date,'DD-MM-YYYY HH24:MI:SS') tanggal_transaksi, 
                    a.transaction_state,
                    a.description,
                    abs(a.amount) amount
                FROM narobil_pay_collect_trans_hist a
                WHERE a.amount < 0
                and a.transaction_type_name = 'Bank Transfer Partner' 
                and $where
                order by formatted_transaction_date asc
		";

		$sql_count = "select count(*) from({$sql}) x where 1=1";

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
            	null,
            	null,
            	null,
            	null,
            	null,
            	null
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
            	$sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";

        $sql = "
            SELECT
            x.*,
            row_number() over() as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";

		if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.trace_number ||'-'||
                x.transaction_number ||'-'||
                x.tanggal_transaksi ||'-'||
                x.transaction_state ||'-'||
                x.description ||'-'||
                x.amount
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
	}

    public function get_settlement_excel($stardate, $enddate){
        $where = '';
        if ($stardate == $enddate){
            $where = "to_char(formatted_transaction_date,'MM/DD/YYYY') = '{$stardate}'";
        }else{
            $where = "to_char(formatted_transaction_date,'MM/DD/YYYY') >= '{$stardate}' and to_char(formatted_transaction_date,'MM/DD/YYYY') <= '{$enddate}'";
        };
        $sql = "
                SELECT
                    a.trace_number, 
                    a.transaction_number, 
                    to_char(a.formatted_transaction_date,'DD-MM-YYYY HH24:MI:SS') tanggal_transaksi, 
                    a.transaction_state,
                    a.description,
                    abs(a.amount) amount
                FROM narobil_pay_collect_trans_hist a
                WHERE a.amount < 0
                and a.transaction_type_name = 'Bank Transfer Partner' 
                and $where
                order by formatted_transaction_date asc
        ";
        $sql = "
            SELECT
            x.*,
            row_number() over() as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";
        
        return $this->db->query($sql)->result();
    }


}
