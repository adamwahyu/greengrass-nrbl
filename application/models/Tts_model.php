<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tts_model extends CI_Model {

	public $table = "t_spp";
	public $primaryKey = "id_spp";

	public function __construct() {
		parent::__construct();
	}

    public function get_datatables($like = null, $length = null, $start, $count = NULL) {
        $sql = "
            select
            x.*,
            (
                select
                desc_metode_pembayaran
                from ref_metode_pembayaran
                where id_metode_pembayaran = x.id_metode_pembayaran
            )desc_metode_pembayaran
            from t_tandaterima_header x
        ";

        $sql_count = "select count(*) from({$sql}) x where 1=1";

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
                "x.rownum",
                "x.kode_unit",
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
                $sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";
        $sql = "
            SELECT
            x.*,
            row_number() over(ORDER BY x.tth_id DESC) as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";

        if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.kode_unit
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
    }

    public function get_metode_pembayaran() {
        $sql = "
            select
            *
            from ref_metode_pembayaran
        ";
        return $this->db->query($sql)->result();
    }
    public function get_t_kewajiban_by_batch_id($id) {
        $sql = "
            select
            *
            from t_kewajiban where id_kewajiban in({$id})
        ";
        return $this->db->query($sql)->result();
    }

    public function get_list_t_kewajiban_open_by_kodeunit($kodeunit) {
        $sql = "
            select
            id_kewajiban,
            concat('IVC.',kode_unit,'.', bulan::int, '.', tahun) as invoice,
            bulan as kd_bulan,
            tahun,
            'Standard' as jenis_iuran,
            tarif_permeter,
            0 as denda,
            nilai_kewajiban
            from t_kewajiban
            where 1=1
            and kode_unit = '{$kodeunit}'
            order by tgl_kewajiban desc
        ";
        return $this->db->query($sql)->result();
    }

     public function get_one_header_by_tth_code($tth_code) {
        $sql = "
            select
            *,
            (
                select
                desc_metode_pembayaran
                from ref_metode_pembayaran
                where id_metode_pembayaran = x.id_metode_pembayaran
            )desc_metode_pembayaran
            from t_tandaterima_header x
            where tth_code = '{$tth_code}'
        ";
        return $this->db->query($sql)->row();
    }
    public function get_detail_by_tth_code($tth_code) {
        $sql = "
            select
            *
            from t_tandaterima_detail
            where tth_code = '{$tth_code}'
						order by tahun desc,bulan desc
        ";
        return $this->db->query($sql)->result();
    }

    public function get_generate_code() {
        $sql = "
            select func_generate_tterima_head_code() as code
        ";
        return $this->db->query($sql)->row()->code;
    }
     public function insert_header($data) {
        $this->db
            ->insert("t_tandaterima_header", $data);
        return $this->db->insert_id();
    }
    public function insert_batch_header($data) {
        $this->db
            ->insert_batch("t_tandaterima_header", $data);
        return TRUE;
    }
    public function insert_detail($data) {
        $this->db
            ->insert("t_tandaterima_detail", $data);
        return $this->db->insert_id();
    }
    public function insert_batch_detail($data) {
        $this->db
            ->insert_batch("t_tandaterima_detail", $data);
        return TRUE;
    }
}
