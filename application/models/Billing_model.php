<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Billing_model extends CI_Model {

	public $table = "t_billing";
	public $primaryKey = "id_billing";

	public function __construct() {
		parent::__construct();
	}
	public function get_one_as_object_by_id($id) {
		$sql = "
			select
			*
			from {$this->table}
			where ".$this->primaryKey."={$id}
			limit 1
		";
		return $this->db->query($sql)->row();
	}
	public function get_datatables($like = null, $length = null, $start, $count = NULL) {
		$sql = "
			select
			*
			from t_billing
		";

		$sql_count = "select count(*) from({$sql}) x where 1=1";

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
            	null,
            	null,
            	null,
            	null,
            	null,
            	null,
            	null,
            	null,
            	null
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
            	$sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";
		$sql = "
			SELECT
			x.*,
			row_number() over(ORDER BY x.created_date DESC) as rownum
            FROM( $sql ) x WHERE 1 = 1
		";

		if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.kode_billing ||
                x.kode_unit ||
                x.nama_pemilik ||
                x.nilai_billing
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
	}

}
