<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

	protected $table = "ref_menu";
	protected $primaryKey = "ref_id_menu";

	public function __construct() {
		parent::__construct();
	}

	public function get_datatables($like = null, $length = null, $start, $count = NULL) {
		$sql = "
			select
			x.ref_id_menu,
			coalesce(x.ref_parent_id_menu,-0) ref_parent_id_menu,
			coalesce((
				select
				ref_name_menu
				from ref_menu where ref_id_menu = x.ref_parent_id_menu
			),'-')ref_parent_name_menu,
			coalesce(x.ref_name_menu,'-') ref_name_menu,
			coalesce(x.ref_desc_menu,'-') ref_desc_menu,
			coalesce(x.ref_url_menu,'-') ref_url_menu,
			coalesce(x.ref_icon_menu,'-') ref_icon_menu,
			coalesce(x.ref_position_menu,'-') ref_position_menu,
			x.sts_active,
			x.create_date,
			x.update_date
			from ref_menu x
		";

		$sql_count = "select count(*) from({$sql}) x where 1=1";
		

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
            	"x.rownum",
            	"x.ref_name_menu",
            	"x.ref_parent_name_menu",
            	"x.ref_url_menu",
            	"x.ref_icon_menu",
            	"x.ref_position_menu",
            	null
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
            	$sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }
        $sql = "select * from({$sql})x where 1=1";
        $sql = "
			SELECT
			x.*,
			row_number() over(ORDER BY x.create_date DESC) as rownum
            FROM( $sql ) x WHERE 1 = 1
		";
		if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.ref_name_menu ||
                x.ref_parent_name_menu ||
                x.ref_url_menu ||
                x.ref_icon_menu ||
                x.ref_position_menu
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
	}

	public function sync_menu() {
		$sql = "
			select
			*
			from (
				select
				a.group_id,
				a.group_name,
				b.ref_id_menu,
				b.ref_name_menu,
				(
					select
					ref_id_gaccess
					from ref_group_access
					where ref_id_group_gaccess = a.group_id
					and ref_id_menu_gaccess = b.ref_id_menu limit 1
				),
				case
				when exists(select
					ref_id_gaccess
					from ref_group_access
					where ref_id_group_gaccess = a.group_id
					and ref_id_menu_gaccess = b.ref_id_menu)
				then 'Y'
				else 'N'
				end as status
				from ref_group a, (
					select
					*
					from ref_menu
				)b
			)x
			where x.status = 'N'
		";
		$get_sync_menu = $this->db->query($sql)->result();
		$user_id = $this->session->userdata("user")->user_id;

		$data_insert_group_access = array_map(function($v) use ($user_id) {
			return [
				"ref_id_group_gaccess" => $v->group_id,
				"ref_id_menu_gaccess" => $v->ref_id_menu,
				"sts_active" => 0,
				"create_by" => $user_id
			];
		}, $get_sync_menu);

		if(count($data_insert_group_access) > 0) {
			$this->db->insert_batch("ref_group_access", $data_insert_group_access);
		}
	}

	public function get_all_status_active() {
		$sql = "
			select
			*
			from {$this->table}
			where sts_active = 1
		";
		return $this->db->query($sql)->result();
	}

	public function get_all() {
		$sql = "
			select
			*
			from {$this->table}
		";
		return $this->db->query($sql)->result();
	}
	public function get_all_by_column_as_object($column, $value) {
		$where[$column] = $value;
		return $this->db->get_where($this->table, $where)->result();
	}
	public function get_one_all_by_column_as_object($column, $value) {
		$where[$column] = $value;
		return $this->db->get_where($this->table, $where)->row();
	}
	public function get_one_as_object_by_id($id) {
		$sql = "
			select
			*
			from {$this->table}
			where ref_id_menu={$id}
			limit 1
		";
		return $this->db->query($sql)->row();
	}

	public function insert($data) {
		$this->db
			->insert($this->table, $data);
		return $this->db->insert_id();
	}
	public function insert_batch($data) {
		$this->db
			->insert_batch($this->table, $data);
		return TRUE;
	}
	public function update_by_column($column, $value, $data) {
		$this->db->where($column, $value);
		return $this->db->update($this->table, $data);
	}
	public function update_by_id($id, $data) {
		$this->db->where($this->primaryKey, $id);
		return $this->db->update($this->table, $data);
	}
	public function update_batch($data, $id) {
		$this->db
			->update_batch($this->table, $data, $id);
		return TRUE;
	}
	public function delete($value, $column = "") {
		$column = (empty($column)) ? $this->primaryKey : $column;
		$this->db->where($column, $value);
		$query = $this->db->delete($this->table);
		return $query;
	}

}
