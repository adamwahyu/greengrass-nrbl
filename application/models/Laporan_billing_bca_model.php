<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_billing_bca_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

    public function dynamic_month()
    {
        $sql = "select 1 id_bulan,TO_CHAR(NOW(),'MON') bulan
                UNION
                select 2 id_bulan,TO_CHAR(NOW()-interval '1 month','MON') bulan
                UNION
                select 3 id_bulan,TO_CHAR(NOW()-interval '2 month','MON') bulan
                UNION
                select 4 id_bulan,TO_CHAR(NOW()-interval '3 month','MON') bulan
                ORDER BY id_bulan desc";

        return $this->db->query($sql)->result();
    }

	public function get_datatables($like = null, $length = null, $start, $count = NULL) {
		$sql = "
                select * from v_generate_billing_bca
		";

		$sql_count = "select count(*) from({$sql}) x where 1=1";

        if($this->input->get("order")) {
            $columns = [
            	null,
            	null,
            	null,
            	null,
            	null,
            	null
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
            	$sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];
            }
        }

        $sql = "
            SELECT
            x.*,
            row_number() over(ORDER BY x.kode_unit asc) as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";

		if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.nomor_va_bca ||'-'||
                x.kode_unit ||'-'||
                x.tunggakan ||'-'||
                x.b4 ||'-'||
                x.b3 ||'-'||
                x.b2 ||'-'||
                x.b1 
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT $length OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
	}

    public function get_billing_bca_excel(){
        $sql = '
            select * from v_generate_billing_bca
        ';
        $sql = "
            SELECT
            x.*,
            row_number() over() as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";
        return $this->db->query($sql)->result();
    }

}
