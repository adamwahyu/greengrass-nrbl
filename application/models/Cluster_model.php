<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cluster_model extends CI_Model {

    protected $table = "ref_cluster";
    protected $primaryKey = "id_cluster";

	public function __construct() {
		parent::__construct();
	}

    public function get_one_all_by_column_as_object($column, $value) {
        $where[$column] = $value;
        return $this->db->get_where($this->table, $where)->row();
    }
    
    public function insert($data) {
        $this->db
            ->insert($this->table, $data);
        return $this->db->insert_id();
    }
    public function insert_batch($data) {
        $this->db
            ->insert_batch($this->table, $data);
        return TRUE;
    }
    public function update_by_column($column, $value, $data) {
        $this->db->where($column, $value);
        return $this->db->update($this->table, $data);
    }
    public function update_by_id($id, $data) {
        $this->db->where($this->primaryKey, $id);
        return $this->db->update($this->table, $data);
    }
    public function update_batch($data, $id) {
        $this->db
            ->update_batch($this->table, $data, $id);
        return TRUE;
    }
    public function delete($value, $column = "") {
        $column = (empty($column)) ? $this->primaryKey : $column;
        $this->db->where($column, $value);
        $query = $this->db->delete($this->table);
        return $query;
    }
    public function get_perumahan_all() {
        $sql = "
            select
            *
            from ref_perumahan
        ";
        return $this->db->query($sql)->result();
    }

    public function get_datatables($like = null, $length = null, $start, $count = NULL) {
        $sql = "
            select
            *,
            (
                select
                nama_perumahan
                from ref_perumahan
                where id_perumahan = x.id_perumahan
            ) nama_perumahan
            from ref_cluster x
        ";

        $sql_count = "select count(*) from({$sql}) x where 1=1";
        

        $sql = "select * from({$sql})x";

        if($this->input->get("order")) {
            $columns = [
                "x.rownum",
                "x.nama_cluster"
            ];

            if(isset($columns[$this->input->get("order")['0']['column']]) && !is_null($columns[$this->input->get("order")['0']['column']])) {
                $sql .= " ORDER BY ".$columns[$this->input->get("order")['0']['column']]." ".$this->input->get("order")['0']['dir'];    
            }
        }
        $sql = "select * from({$sql})x where 1=1";
        $sql = "
            SELECT
            x.*,
            row_number() over(ORDER BY x.nama_cluster DESC) as rownum
            FROM( $sql ) x WHERE 1 = 1
        ";
        if(!is_null($count)) {
            $sql = $sql_count;
        }
        if(!empty($like)) {
            $sql .= "AND upper(
                x.nama_cluster 
            ) LIKE UPPER('%".$this->db->escape_like_str($like)."%')";
        }
        if(!empty($length) && is_null($count)) {
            $sql .= " LIMIT abs($length) OFFSET $start";
        }

        $this->db->trans_begin();
        $result = $this->db->query($sql);
        $this->db->trans_commit();

        if(!is_null($count)) {
            $rows = $result->row();
            $val = 0;
            if($rows){
                $val = $rows->count;
            }
            return $val;
        } else {
            return ($result->num_rows() > 0) ? $result->result() : array();
        }
    }

}
