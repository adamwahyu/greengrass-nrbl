<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// echo(exec("getmac"));
		// exit;
		if(!$this->session->userdata("is_logged_in")) {
			redirect("auth/login");
		}

		$this->install();

		$this->load->model([
			"GroupAccess_model"
		]);

		$menus = $this->GroupAccess_model->get_menu_active_as_object_by_group_id($this->session->userdata("user")->group_id);
		$menus_links = array_map(function($v) {
			$alinks = explode("/", $v["link"]);
			return $alinks[0];
		}, $menus);

		$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$path = parse_url($actual_link)["path"];
		$dir = preg_replace('@/+$@', '', dirname($_SERVER['SCRIPT_NAME']));
		$dir = str_replace("\\", '', $dir);
		if(!empty($dir)) {
			$dir .= "/";
		}
		$path = str_replace($dir, "", $path);
		$path = explode("/", $path);
		if(is_array($path)) {
			if(empty($path[0])) {
				$path = $path[1];
			} else {
				$path = $path[0];
			}
		}
		if(!in_array($path, $menus_links)) {
			show_404();
		}
		$menus_widgets = [];
		$mm = [];
		foreach($menus as $m) {
			if($m["link"] == "profile/change_password" || $m["link"] == "profile/edit" || $m["link"] == "auth/logout") {
				$menus_widgets[] = $m;
			} else {
				$mm[] = $m;
			}
		}
		$menus = $mm;
		$this->data["v_menu"] = str_replace("<ul class='nav nav-treeview'></ul>", "", build_menu($menus));
		$this->data["v_menu_widget"] = $menus_widgets;
		$this->data["v_user"] = $this->session->userdata("user");
	}

	private function install() {
		if(!file_exists(FCPATH."/upload")) {
			mkdir(FCPATH."/upload");
		}
		if(!file_exists(FCPATH."/upload/file_pembayaran")) {
			mkdir(FCPATH."/upload/file_pembayaran");
		}
		if(!file_exists(FCPATH."/upload/pembayaran")) {
			mkdir(FCPATH."/upload/pembayaran");
		}
	}

}
